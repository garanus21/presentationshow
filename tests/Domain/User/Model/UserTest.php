<?php

declare(strict_types=1);

namespace App\Tests\Domain\User\Model;

use App\Domain\User\Model\User;
use App\Domain\User\ValueObject\UserId;
use App\Infrastructure\Security\ValueObject\EncodedPassword;
use PHPUnit\Framework\TestCase;

/**
 * Class UserTest
 *
 * @package App\Tests\Domain\User\Model
 */
class UserTest extends TestCase
{
    private $user;

    protected function setUp()
    {
        $this->user = new User(
            new UserId('c2567932-d131-4921-b411-6fdaa6e4e697'),
            'john.doe@example.com',
            'John',
            'Doe',
            new EncodedPassword('test123')
        );
    }

    public function testConstructor()
    {
        $this->assertEquals('c2567932-d131-4921-b411-6fdaa6e4e697', $this->user->uuid()->__toString());
        $this->assertEquals('john.doe@example.com', $this->user->getEmail());
        $this->assertEquals('John', $this->user->getFirstName());
        $this->assertEquals('Doe', $this->user->getLastName());
        $this->assertEquals('john.doe@example.com', $this->user->getUsername());
        $this->assertEquals(null, $this->user->getSalt());
        $this->assertEqualsCanonicalizing(['ROLE_USER'], $this->user->getRoles());
        $this->assertNotEmpty($this->user->getPassword());
    }

    public function testRemind()
    {
        $this->user->remind();

        $this->assertNotNull($this->user->getReminder());
        $this->assertNotNull($this->user->getReminder()->getCode());
        $this->assertNotNull($this->user->getReminder()->getExpiresAt());
    }

    public function testResetPassword()
    {
        $this->user->remind();
        $this->user->resetPassword(new EncodedPassword('test1234'));

        $this->assertNull($this->user->getReminder()->getCode());
        $this->assertNull($this->user->getReminder()->getExpiresAt());
    }

    public function testUpdate()
    {
        $this->user->update('Nelson', 'Morrison');

        $this->assertEquals('Nelson', $this->user->getFirstName());
        $this->assertEquals('Morrison', $this->user->getLastName());
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->user = null;
    }
}
