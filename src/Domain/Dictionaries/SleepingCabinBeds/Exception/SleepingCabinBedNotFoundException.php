<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\SleepingCabinBeds\Exception;

use App\Domain\Common\Exception\NotFoundException;

/**
 * Class SleepingCabinBedNotFoundException
 * @package App\Domain\Dictionaries\SleepingCabinBeds\Exception
 */
class SleepingCabinBedNotFoundException extends NotFoundException
{
    /**
     * SleepingCabinBedNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('sleeping_cabin_bed.exception.not_found', 404);
    }
}