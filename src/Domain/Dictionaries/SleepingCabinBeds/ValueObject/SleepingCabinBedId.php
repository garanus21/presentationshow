<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\SleepingCabinBeds\ValueObject;

use App\Domain\Common\ValueObject\AggregateRootId;

/**
 * Class SleepingCabinBedId
 * @package App\Domain\Dictionaries\SleepingCabinBeds\ValueObject
 */
class SleepingCabinBedId extends AggregateRootId
{
}