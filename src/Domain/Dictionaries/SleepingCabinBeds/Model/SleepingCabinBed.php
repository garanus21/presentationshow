<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\SleepingCabinBeds\Model;

use App\Domain\Common\ValueObject\AggregateRoot;
use App\Domain\Dictionaries\Cabins\Model\Cabin;
use App\Domain\Dictionaries\SleepingCabinBeds\ValueObject\SleepingCabinBedId;

/**
 * Class SleepingCabinBed
 * @package App\Domain\Dictionaries\SleepingCabinBeds\Model
 */
class SleepingCabinBed extends AggregateRoot
{
    /**
     * @var SleepingCabinBedId
     */
    protected $uuid;

    /**
     * Sleeping cabin bed name.
     *
     * @var string
     */
    private $name;

    /**
     * Sleeping cabin bed created date.
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * Sleeping cabin bed deleted date.
     *
     * @var \DateTime|null
     */
    private $deletedAt;

    /**
     * Cabin.
     *
     * @var Cabin|null
     */
    private $cabin;

    /**
     * SleepingCabinBed constructor.
     * @param SleepingCabinBedId $uuid
     * @param string $name
     * @param \DateTime $createdAt
     * @param \DateTime|null $deletedAt
     * @param Cabin|null $cabin
     */
    public function __construct(
        SleepingCabinBedId $uuid,
        string $name,
        \DateTime $createdAt,
        ?\DateTime $deletedAt,
        ?Cabin $cabin
    ) {
        parent::__construct($uuid);

        $this->name = $name;
        $this->createdAt = $createdAt;
        $this->deletedAt = $deletedAt;
        $this->cabin = $cabin;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime|null $deletedAt
     */
    public function setDeletedAt(?\DateTime $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return Cabin|null
     */
    public function getCabin(): ?Cabin
    {
        return $this->cabin;
    }

    /**
     * @param Cabin|null $cabin
     */
    public function setCabin(?Cabin $cabin): void
    {
        $this->cabin = $cabin;
    }
}