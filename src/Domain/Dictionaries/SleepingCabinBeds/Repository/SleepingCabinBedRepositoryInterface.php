<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\SleepingCabinBeds\Repository;

use App\Domain\Dictionaries\SleepingCabinBeds\Model\SleepingCabinBed;

/**
 * Interface SleepingCabinBedRepositoryInterface
 * @package App\Domain\Dictionaries\SleepingCabinBeds\Repository
 */
interface SleepingCabinBedRepositoryInterface
{
    /**
     * Delete.
     *
     * @param SleepingCabinBed $sleepingCabinBed
     */
    public function delete(SleepingCabinBed $sleepingCabinBed): void;

    /**
     * Find all.
     *
     * @return SleepingCabinBed[]
     */
    public function findAll();

    /**
     * Find cabin sleeping cabin bed.
     *
     * @param $cabinId
     * @return array
     */
    public function findByCabinId($cabinId): array;

    /**
     * Save.
     *
     * @param SleepingCabinBed $sleepingCabinBed
     */
    public function save(SleepingCabinBed $sleepingCabinBed): void;

    /**
     * Find one by Id.
     *
     * @param $sleepingCabinBedId
     * @return SleepingCabinBed|null
     */
    public function findOneById($sleepingCabinBedId): ?SleepingCabinBed;

    /**
     * Get one by id.
     *
     * @param $sleepingCabinBedId
     * @return SleepingCabinBed|null
     */
    public function getOneById($sleepingCabinBedId): ?SleepingCabinBed;
}