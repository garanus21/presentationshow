<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\SleepingCabinTypes\ValueObject;

use App\Domain\Common\ValueObject\AggregateRootId;

/**
 * Class SleepingCabinTypeId
 * @package App\Domain\Dictionaries\SleepingCabinTypes\ValueObject
 */
class SleepingCabinTypeId extends AggregateRootId
{
}