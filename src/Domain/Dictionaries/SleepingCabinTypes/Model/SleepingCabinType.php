<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\SleepingCabinTypes\Model;

use App\Domain\Common\ValueObject\AggregateRoot;
use App\Domain\Dictionaries\Cabins\Model\Cabin;
use App\Domain\Dictionaries\SleepingCabinTypes\ValueObject\SleepingCabinTypeId;

/**
 * Class SleepingCabinType
 * @package App\Domain\Dictionaries\SleepingCabinTypes\Model
 */
class SleepingCabinType extends AggregateRoot
{
    /**
     * @var SleepingCabinTypeId
     */
    protected $uuid;

    /**
     * Sleeping cabin type name.
     *
     * @var string
     */
    private $name;

    /**
     * Sleeping cabin type created date.
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * Sleeping cabin type deleted date.
     *
     * @var \DateTime|null
     */
    private $deletedAt;

    /**
     * Sleeping cabin type cabin object.
     *
     * @var Cabin|null
     */
    private $cabin;

    /**
     * SleepingCabinType constructor.
     * @param SleepingCabinTypeId $uuid
     * @param string $name
     * @param \DateTime $createdAt
     * @param \DateTime|null $deletedAt
     * @param Cabin|null $cabin
     */
    public function __construct(
        SleepingCabinTypeId $uuid,
        string $name,
        \DateTime $createdAt,
        ?\DateTime $deletedAt,
        ?Cabin $cabin
    ) {
        parent::__construct($uuid);

        $this->name = $name;
        $this->createdAt = $createdAt;
        $this->deletedAt = $deletedAt;
        $this->cabin = $cabin;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime|null $deletedAt
     */
    public function setDeletedAt(?\DateTime $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return Cabin|null
     */
    public function getCabin(): ?Cabin
    {
        return $this->cabin;
    }

    /**
     * @param Cabin|null $cabin
     */
    public function setCabin(?Cabin $cabin): void
    {
        $this->cabin = $cabin;
    }
}