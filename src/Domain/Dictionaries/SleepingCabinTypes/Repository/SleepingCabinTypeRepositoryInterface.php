<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\SleepingCabinTypes\Repository;

use App\Domain\Dictionaries\SleepingCabinTypes\Model\SleepingCabinType;

/**
 * Interface SleepingCabinTypeRepositoryInterface
 * @package App\Domain\Dictionaries\SleepingCabinTypes\Repository
 */
interface SleepingCabinTypeRepositoryInterface
{
    /**
     * Delete.
     *
     * @param SleepingCabinType $sleepingCabinType
     */
    public function delete(SleepingCabinType $sleepingCabinType): void;

    /**
     * Find all colors.
     *
     * @return SleepingCabinType[]
     */
    public function findAll();

    /**
     * Find cabin sleeping cabin type.
     *
     * @param $cabinId
     * @return array
     */
    public function findByCabinId($cabinId): array;

    /**
     * Save.
     *
     * @param SleepingCabinType $sleepingCabinType
     */
    public function save(SleepingCabinType $sleepingCabinType): void;

    /**
     * Find one by Id.
     *
     * @param $sleepingCabinTypeId
     * @return SleepingCabinType|null
     */
    public function findOneById($sleepingCabinTypeId): ?SleepingCabinType;

    /**
     * Get one by id.
     *
     * @param $sleepingCabinTypeId
     * @return SleepingCabinType|null
     */
    public function getOneById($sleepingCabinTypeId): ?SleepingCabinType;
}