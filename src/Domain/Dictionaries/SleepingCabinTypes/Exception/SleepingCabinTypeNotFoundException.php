<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\SleepingCabinTypes\Exception;

use App\Domain\Common\Exception\NotFoundException;

/**
 * Class SleepingCabinTypeNotFoundException
 * @package App\Domain\Dictionaries\SleepingCabinTypes\Exception
 */
class SleepingCabinTypeNotFoundException extends NotFoundException
{
    /**
     * SleepingCabinTypeNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('sleeping_cabin_type.exception.not_found', 404);
    }
}