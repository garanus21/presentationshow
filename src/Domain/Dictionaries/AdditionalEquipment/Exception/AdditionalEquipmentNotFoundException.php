<?php

declare(strict_types=1);


namespace App\Domain\Dictionaries\AdditionalEquipment\Exception;

use App\Domain\Common\Exception\NotFoundException;

/**
 * Class AdditionalEquipmentNotFoundException
 * @package App\Domain\Dictionaries\AdditionalEquipment\Exception
 */
class AdditionalEquipmentNotFoundException extends NotFoundException
{
    /**
     * AdditionalEquipmentNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('additional_equipment.exception.not_found', 404);
    }
}