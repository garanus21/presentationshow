<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\AdditionalEquipment\Repository;

use App\Domain\Dictionaries\AdditionalEquipment\Model\AdditionalEquipment;

/**
 * Interface AdditionalEquipmentRepositoryInterface
 * @package App\Domain\Dictionaries\AdditionalEquipment\Repository
 */
interface AdditionalEquipmentRepositoryInterface
{
    /**
     * Delete.
     *
     * @param AdditionalEquipment $additionalEquipment
     */
    public function delete(AdditionalEquipment $additionalEquipment): void;

    /**
     * Find all.
     *
     * @return AdditionalEquipment[]
     */
    public function findAll();

    /**
     * Find cabin additional equipment.
     *
     * @param $cabinId
     * @return array
     */
    public function findByCabinId($cabinId): array;

    /**
     * Save.
     *
     * @param AdditionalEquipment $additionalEquipment
     */
    public function save(AdditionalEquipment $additionalEquipment): void;

    /**
     * Find one by Id.
     *
     * @param $additionalEquipmentId
     * @return AdditionalEquipment|null
     */
    public function findOneById($additionalEquipmentId): ?AdditionalEquipment;

    /**
     * Get one by id.
     *
     * @param $additionalEquipmentId
     * @return AdditionalEquipment|null
     */
    public function getOneById($additionalEquipmentId): ?AdditionalEquipment;
}