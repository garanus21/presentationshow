<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\AdditionalEquipment\ValueObject;

use App\Domain\Common\ValueObject\AggregateRootId;

/**
 * Class AdditionalEquipmentId
 * @package App\Domain\Dictionaries\AdditionalEquipment\ValueObject
 */
class AdditionalEquipmentId extends AggregateRootId
{
}