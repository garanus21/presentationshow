<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\AdditionalEquipment\Model;

use App\Domain\Common\ValueObject\AggregateRoot;
use App\Domain\Dictionaries\AdditionalEquipment\ValueObject\AdditionalEquipmentId;
use App\Domain\Dictionaries\Cabins\Model\Cabin;

/**
 * Class AdditionalEquipment
 * @package App\Domain\Dictionaries\AdditionalEquipment\Model
 */
class AdditionalEquipment extends AggregateRoot
{
    /**
     * @var AdditionalEquipmentId
     */
    protected $uuid;

    /**
     * Additional equipment name.
     *
     * @var string
     */
    private $name;

    /**
     * Additional equipment comment.
     *
     * @var string|null
     */
    private $comment;

    /**
     * Additional equipment created date.
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * Additional equipment deleted date.
     *
     * @var \DateTime|null
     */
    private $deletedAt;

    /**
     * Additional equipment price.
     *
     * @var int|null
     */
    private $price;

    /**
     * Additional equipment cabin.
     *
     * @var Cabin|null
     */
    private $cabin;

    /**
     * AdditionalEquipment constructor.
     * @param AdditionalEquipmentId $uuid
     * @param string $name
     * @param string|null $comment
     * @param \DateTime $createdAt
     * @param \DateTime|null $deletedAt
     * @param int|null $price
     * @param Cabin|null $cabin
     */
    public function __construct(
        AdditionalEquipmentId $uuid,
        string $name,
        ?string $comment,
        \DateTime $createdAt,
        ?\DateTime $deletedAt,
        ?int $price,
        ?Cabin $cabin
    ) {
        parent::__construct($uuid);

        $this->name = $name;
        $this->comment = $comment;
        $this->createdAt = $createdAt;
        $this->deletedAt = $deletedAt;
        $this->price = $price;
        $this->cabin = $cabin;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     */
    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime|null $deletedAt
     */
    public function setDeletedAt(?\DateTime $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return Cabin|null
     */
    public function getCabin(): ?Cabin
    {
        return $this->cabin;
    }

    /**
     * @param Cabin|null $cabin
     */
    public function setCabin(?Cabin $cabin): void
    {
        $this->cabin = $cabin;
    }

    /**
     * @return int|null
     */
    public function getPrice(): ?int
    {
        return $this->price;
    }

    /**
     * @param int|null $price
     */
    public function setPrice(?int $price): void
    {
        $this->price = $price;
    }
}