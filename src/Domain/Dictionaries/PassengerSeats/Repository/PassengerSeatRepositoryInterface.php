<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\PassengerSeats\Repository;

use App\Domain\Dictionaries\PassengerSeats\Model\PassengerSeat;

/**
 * Interface PassengerSeatRepositoryInterface
 * @package App\Domain\Dictionaries\PassengerSeats\Repository
 */
interface PassengerSeatRepositoryInterface
{
    /**
     * Delete.
     *
     * @param PassengerSeat $passengerSeat
     */
    public function delete(PassengerSeat $passengerSeat): void;

    /**
     * Find all colors.
     *
     * @return PassengerSeat[]
     */
    public function findAll();

    /**
     * Find cabin passenger seats.
     *
     * @param $cabinId
     * @return array
     */
    public function findByCabinId($cabinId): array;

    /**
     * Save.
     *
     * @param PassengerSeat $passengerSeat
     */
    public function save(PassengerSeat $passengerSeat): void;

    /**
     * Find one by Id.
     *
     * @param $passengerSeatId
     * @return PassengerSeat|null
     */
    public function findOneById($passengerSeatId): ?PassengerSeat;

    /**
     * Get one by id.
     *
     * @param $passengerSeatId
     * @return PassengerSeat|null
     */
    public function getOneById($passengerSeatId): ?PassengerSeat;
}