<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\PassengerSeats\ValueObject;

use App\Domain\Common\ValueObject\AggregateRootId;

/**
 * Class PassengerSeatId
 * @package App\Domain\Dictionaries\PassengerSeats\ValueObject
 */
class PassengerSeatId extends AggregateRootId
{
}