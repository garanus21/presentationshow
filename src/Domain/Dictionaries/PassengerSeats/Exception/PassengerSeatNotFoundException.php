<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\PassengerSeats\Exception;

use App\Domain\Common\Exception\NotFoundException;

/**
 * Class PassengerSeatNotFoundException
 * @package App\Domain\Dictionaries\PassengerSeats\Exception
 */
class PassengerSeatNotFoundException extends NotFoundException
{
    /**
     * PassengerSeatNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('passenger_seat.exception.not_found', 404);
    }
}