<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\PassengerSeats\Model;

use App\Domain\Common\ValueObject\AggregateRoot;
use App\Domain\Dictionaries\Cabins\Model\Cabin;
use App\Domain\Dictionaries\PassengerSeats\ValueObject\PassengerSeatId;

/**
 * Class PassengerSeat
 * @package App\Domain\Dictionaries\PassengerSeats\Model
 */
class PassengerSeat extends AggregateRoot
{
    /**
     * @var PassengerSeatId
     */
    protected $uuid;

    /**
     * Passenger seat name.
     *
     * @var string
     */
    private $name;

    /**
     * Passenger seat created date.
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * Passenger seat deleted date.
     *
     * @var \DateTime|null
     */
    private $deletedAt;

    /**
     * Object cabin.
     *
     * @var Cabin|null
     */
    private $cabin;

    /**
     * PassengerSeat constructor.
     * @param PassengerSeatId $uuid
     * @param string $name
     * @param \DateTime $createdAt
     * @param \DateTime|null $deletedAt
     * @param Cabin|null $cabin
     */
    public function __construct(
        PassengerSeatId $uuid,
        string $name,
        \DateTime $createdAt,
        ?\DateTime $deletedAt,
        ?Cabin $cabin
    ) {
        parent::__construct($uuid);

        $this->name = $name;
        $this->createdAt = $createdAt;
        $this->deletedAt = $deletedAt;
        $this->cabin = $cabin;
    }

    /**
     * @return PassengerSeatId
     */
    public function getUuid(): PassengerSeatId
    {
        return $this->uuid;
    }

    /**
     * @param PassengerSeatId $uuid
     */
    public function setUuid(PassengerSeatId $uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime|null $deletedAt
     */
    public function setDeletedAt(?\DateTime $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return Cabin|null
     */
    public function getCabin(): ?Cabin
    {
        return $this->cabin;
    }

    /**
     * @param Cabin|null $cabin
     */
    public function setCabin(?Cabin $cabin): void
    {
        $this->cabin = $cabin;
    }
}