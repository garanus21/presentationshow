<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\Brands\Repository;

use App\Domain\Dictionaries\Brands\Model\Brand;

/**
 * Interface BrandRepositoryInterface
 * @package App\Domain\Dictionaries\Brands\Repository
 */
interface BrandRepositoryInterface
{
    /**
     * Delete.
     *
     * @param Brand $brand
     */
    public function delete(Brand $brand): void;

    /**
     * Find all cabins.
     *
     * @return Brand[]
     */
    public function findAll();

    /**
     * Save.
     *
     * @param Brand $brand
     */
    public function save(Brand $brand): void;

    /**
     * Find one by Id.
     *
     * @param $brandId
     * @return Brand|null
     */
    public function findOneById($brandId): ?Brand;

    /**
     * Get one by id.
     *
     * @param $brandId
     * @return Brand|null
     */
    public function getOneById($brandId): ?Brand;
}