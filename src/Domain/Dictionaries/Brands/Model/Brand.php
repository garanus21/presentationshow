<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\Brands\Model;

use App\Domain\Common\ValueObject\AggregateRoot;
use App\Domain\Dictionaries\Brands\ValueObject\BrandId;

/**
 * Class Brand
 * @package App\Domain\Dictionaries\Brands\Model
 */
class Brand extends AggregateRoot
{
    /**
     * @var BrandId
     */
    protected $uuid;

    /**
     * Brand name.
     *
     * @var string
     */
    private $name;

    /**
     * Brand created date.
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * Brand deleted date.
     *
     * @var \DateTime|null
     */
    private $deletedAt;

    /**
     * Brand constructor.
     * @param BrandId $uuid
     * @param string $name
     * @param \DateTime $createdAt
     * @param \DateTime|null $deletedAt
     */
    public function __construct(
        BrandId $uuid,
        string $name,
        \DateTime $createdAt,
        ?\DateTime $deletedAt
    ) {
        parent::__construct($uuid);

        $this->name = $name;
        $this->createdAt = $createdAt;
        $this->deletedAt = $deletedAt;
    }

    /**
     * Delete function.
     *
     * @return \DateTime
     * @throws \Exception
     */
    public function delete()
    {
        return $this->deletedAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime|null $deletedAt
     */
    public function setDeletedAt(?\DateTime $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }
}