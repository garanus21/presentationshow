<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\Brands\ValueObject;

use App\Domain\Common\ValueObject\AggregateRootId;

/**
 * Class BrandId
 * @package App\Domain\Dictionaries\Brands\ValueObject
 */
class BrandId extends AggregateRootId
{
}