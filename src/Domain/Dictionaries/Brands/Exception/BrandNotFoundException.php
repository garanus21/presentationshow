<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\Brands\Exception;

use App\Domain\Common\Exception\NotFoundException;

/**
 * Class BrandNotFoundException
 * @package App\Domain\Dictionaries\Brands\Exception
 */
class BrandNotFoundException extends NotFoundException
{
    /**
     * BrandNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('brand.exception.not_found', 404);
    }
}