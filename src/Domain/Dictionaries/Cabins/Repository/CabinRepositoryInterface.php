<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\Cabins\Repository;

use App\Domain\Dictionaries\Cabins\Model\Cabin;

/**
 * Interface CabinRepositoryInterface
 * @package App\Domain\Dictionaries\Cabins\Repository
 */
interface CabinRepositoryInterface
{
    /**
     * Delete.
     *
     * @param Cabin $cabin
     */
    public function delete(Cabin $cabin): void;

    /**
     * Find all cabins.
     *
     * @return Cabin[]
     */
    public function findAll();

    /**
     * Save.
     *
     * @param Cabin $cabin
     */
    public function save(Cabin $cabin): void;

    /**
     * Find one by Id.
     *
     * @param $cabinId
     * @return Cabin|null
     */
    public function findOneById($cabinId): ?Cabin;

    /**
     * Get one by id.
     *
     * @param $cabinId
     * @return Cabin|null
     */
    public function getOneById($cabinId): ?Cabin;
}