<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\Cabins\Exception;

use App\Domain\Common\Exception\NotFoundException;

/**
 * Class CabinNotFoundException
 * @package App\Domain\Dictionaries\Cabins\Exception
 */
class CabinNotFoundException extends NotFoundException
{
    /**
     * CabinNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('cabin.exception.not_found', 404);
    }
}