<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\Cabins\Model;

use App\Domain\Common\ValueObject\AggregateRoot;
use App\Domain\Dictionaries\Cabins\ValueObject\CabinId;

/**
 * Class Cabin
 * @package App\Domain\Dictionaries\Cabins\Model
 */
class Cabin extends AggregateRoot
{
    /**
     * @var CabinId
     */
    protected $uuid;

    /**
     * Cabin name.
     *
     * @var string
     */
    private $name;

    /**
     * Cabin created date.
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * Cabin deleted date.
     *
     * @var \DateTime|null
     */
    private $deletedAt;

    /**
     * Cabin constructor.
     * @param CabinId $uuid
     * @param string $name
     * @param \DateTime $createdAt
     * @param \DateTime|null $deletedAt
     */
    public function __construct(
        CabinId $uuid,
        string $name,
        \DateTime $createdAt,
        ?\DateTime $deletedAt
    ) {
        parent::__construct($uuid);

        $this->name = $name;
        $this->createdAt = $createdAt;
        $this->deletedAt = $deletedAt;
    }

    /**
     * Delete function.
     *
     * @return \DateTime
     * @throws \Exception
     */
    public function delete()
    {
        return $this->deletedAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime|null $deletedAt
     */
    public function setDeletedAt(?\DateTime $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }
}