<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\Cabins\ValueObject;

use App\Domain\Common\ValueObject\AggregateRootId;

/**
 * Class CabinId
 * @package App\Domain\Dictionaries\Cabins\ValueObject
 */
class CabinId extends AggregateRootId
{
}