<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\Colors\ValueObject;

use App\Domain\Common\ValueObject\AggregateRootId;

/**
 * Class ColorId
 * @package App\Domain\Dictionaries\Colors\ValueObject
 */
class ColorId extends AggregateRootId
{
}