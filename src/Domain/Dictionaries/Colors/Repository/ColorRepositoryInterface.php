<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\Colors\Repository;

use App\Domain\Dictionaries\Colors\Model\Color;

/**
 * Interface ColorRepositoryInterface
 * @package App\Domain\Dictionaries\Colors\Repository
 */
interface ColorRepositoryInterface
{
    /**
     * Delete.
     *
     * @param Color $color
     */
    public function delete(Color $color): void;

    /**
     * Find all colors.
     *
     * @return Color[]
     */
    public function findAll();

    /**
     * Save.
     *
     * @param Color $color
     */
    public function save(Color $color): void;

    /**
     * Find one by Id.
     *
     * @param $colorId
     * @return Color|null
     */
    public function findOneById($colorId): ?Color;

    /**
     * Get one by id.
     *
     * @param $colorId
     * @return Color|null
     */
    public function getOneById($colorId): ?Color;
}