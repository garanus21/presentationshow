<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\Colors\Model;

use App\Domain\Common\ValueObject\AggregateRoot;
use App\Domain\Dictionaries\Colors\ValueObject\ColorId;

/**
 * Class Color
 * @package App\Domain\Dictionaries\Colors\Model
 */
class Color extends AggregateRoot
{
    /**
     * @var ColorId
     */
    protected $uuid;

    /**
     * Color name.
     *
     * @var string
     */
    private $name;

    /**
     * Color created date.
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * Color deleted date.
     *
     * @var \DateTime|null
     */
    private $deletedAt;

    /**
     * Color constructor.
     * @param ColorId $uuid
     * @param string $name
     * @param \DateTime $createdAt
     * @param \DateTime|null $deletedAt
     */
    public function __construct(
        ColorId $uuid,
        string $name,
        \DateTime $createdAt,
        ?\DateTime $deletedAt
    ) {
        parent::__construct($uuid);

        $this->name = $name;
        $this->createdAt = $createdAt;
        $this->deletedAt = $deletedAt;
    }

    /**
     * Delete function.
     *
     * @return \DateTime
     * @throws \Exception
     */
    public function delete()
    {
        return $this->deletedAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime|null $deletedAt
     */
    public function setDeletedAt(?\DateTime $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }
}