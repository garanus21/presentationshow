<?php

declare(strict_types=1);

namespace App\Domain\Dictionaries\Colors\Exception;

use App\Domain\Common\Exception\NotFoundException;

/**
 * Class ColorNotFoundException
 * @package App\Domain\Dictionaries\Colors\Exception
 */
class ColorNotFoundException extends NotFoundException
{
    /**
     * ColorNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('color.exception.not_found', 404);
    }
}