<?php

declare(strict_types=1);

namespace App\Domain\Orders\Exception;

use App\Domain\Common\Exception\NotFoundException;

/**
 * Class OrderNotFoundException
 * @package App\Domain\Orders\Exception
 *
 */
class OrderNotFoundException extends NotFoundException
{
    /**
     * OrderNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('order.exception.not_found', 404);
    }
}