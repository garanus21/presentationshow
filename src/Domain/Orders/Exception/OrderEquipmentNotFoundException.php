<?php

declare(strict_types=1);

namespace App\Domain\Orders\Exception;

use App\Domain\Common\Exception\NotFoundException;

/**
 * Class OrderEquipmentNotFoundException
 * @package App\Domain\Orders\Exception
 */
class OrderEquipmentNotFoundException extends NotFoundException
{
    /**
     * OrderEquipmentNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('order_equipment.exception.not_found', 404);
    }
}