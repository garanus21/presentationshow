<?php

declare(strict_types=1);

namespace App\Domain\Orders\ValueObject;

use App\Domain\Common\ValueObject\AggregateRootId;

/**
 * Class OrderId
 * @package App\Domain\Orders\ValueObject
 */
class OrderId extends AggregateRootId
{
}