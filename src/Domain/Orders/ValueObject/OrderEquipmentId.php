<?php

declare(strict_types=1);

namespace App\Domain\Orders\ValueObject;

use App\Domain\Common\ValueObject\AggregateRootId;

/**
 * Class OrderEquipmentId
 * @package App\Domain\Orders\ValueObject
 */
class OrderEquipmentId extends AggregateRootId
{
}