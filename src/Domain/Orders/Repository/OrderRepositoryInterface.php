<?php

declare(strict_types=1);

namespace App\Domain\Orders\Repository;

use App\Domain\Orders\Model\Order;
use Doctrine\ORM\QueryBuilder;

/**
 * Interface OrderRepositoryInterface
 * @package App\Domain\Orders\Repository
 */
interface OrderRepositoryInterface
{
    /**
     * Delete.
     *
     * @param Order $order
     */
    public function delete(Order $order): void;

    /**
     * Find all orders.
     *
     * @return Order[]
     */
    public function findAll();

    /**
     * Save.
     *
     * @param Order $order
     */
    public function save(Order $order): void;

    /**
     * Find one by Id.
     *
     * @param $orderId
     * @return Order|null
     */
    public function findOneById($orderId): ?Order;

    /**
     * Get one by id.
     *
     * @param $orderId
     * @return Order|null
     */
    public function getOneById($orderId): ?Order;

    /**
     * Find paginated orders.
     *
     * @param array $criteria
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function findPaginatedOrders(array $criteria, int $limit, int $offset): array;

    /**
     * Find paginated orders count.
     *
     * @param array $criteria
     * @return int
     */
    public function findPaginatedCountBy(array $criteria): int;

    /**
     * @param string $lastName
     * @param string $orderDate
     * @param string $equipmentName
     * @param string $productType
     * @param string $orderStatus
     * @param string $nip
     * @param string $orderNumber
     * @param string $orderCompletionTime
     * @param int $orderByDate
     * @param int $orderByClientName
     * @return int
     */
    public function searchPaginatedCountByQuery(
        string $lastName,
        string $orderDate,
        string $equipmentName,
        string $productType,
        string $orderStatus,
        string $nip,
        string $orderNumber,
        string $orderCompletionTime,
        int $orderByDate,
        int $orderByClientName
    ): int;

    /**
     * @param string $lastName
     * @param string $orderDate
     * @param string $equipmentName
     * @param string $productType
     * @param string $orderStatus
     * @param string $nip
     * @param string $orderNumber
     * @param string $orderCompletionTime
     * @param int $orderByDate
     * @param int $orderByClientName
     * @return QueryBuilder
     */
    public function queryBuilderSearchByQueries(
        string $lastName,
        string $orderDate,
        string $equipmentName,
        string $productType,
        string $orderStatus,
        string $nip,
        string $orderNumber,
        string $orderCompletionTime,
        int $orderByDate,
        int $orderByClientName
    ): QueryBuilder;

    /**
     * @param string $lastName
     * @param string $orderDate
     * @param string $equipmentName
     * @param string $productType
     * @param string $orderStatus
     * @param string $nip
     * @param string $orderNumber
     * @param string $orderCompletionTime
     * @param int $orderByDate
     * @param int $orderByClientName
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function searchPaginatedByQueries(
        string $lastName,
        string $orderDate,
        string $equipmentName,
        string $productType,
        string $orderStatus,
        string $nip,
        string $orderNumber,
        string $orderCompletionTime,
        int $orderByDate,
        int $orderByClientName,
        int $limit,
        int $offset
    ): array;
}