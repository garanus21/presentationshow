<?php

declare(strict_types=1);

namespace App\Domain\Orders\Repository;

use App\Domain\Orders\Model\OrderEquipment;

/**
 * Interface OrderEquipmentRepositoryInterface
 * @package App\Domain\Orders\Repository
 */
interface OrderEquipmentRepositoryInterface
{
    /**
     * Delete.
     *
     * @param OrderEquipment $orderEquipment
     */
    public function delete(OrderEquipment $orderEquipment): void;

    /**
     * Find all.
     *
     * @return OrderEquipment[]
     */
    public function findAll();

    /**
     * Save.
     *
     * @param OrderEquipment $orderEquipment
     */
    public function save(OrderEquipment $orderEquipment): void;

    /**
     * Find one by Id.
     *
     * @param $orderEquipmentId
     * @return OrderEquipment|null
     */
    public function findOneById($orderEquipmentId): ?OrderEquipment;

    /**
     * Get one by id.
     *
     * @param $orderEquipmentId
     * @return OrderEquipment|null
     */
    public function getOneById($orderEquipmentId): ?OrderEquipment;

    /**
     * Find by order id.
     *
     * @param $orderId
     * @return array
     */
    public function findByOrder($orderId): array;

    /**
     * Find by order and equipment.
     *
     * @param $orderId
     * @param $additionalEquipmentId
     * @return array
     */
    public function findByOrderAndEquipment($orderId, $additionalEquipmentId): array;
}