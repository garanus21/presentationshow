<?php

declare(strict_types=1);

namespace App\Domain\Orders\Factory;

use App\Domain\Orders\Model\Order;

/**
 * Interface OrderFactoryInterface
 * @package App\Domain\Orders\Factory
 */
interface OrderFactoryInterface
{
    /**
     * Create order.
     *
     * @param array $data
     * @return Order
     */
    public function create(array $data): Order;

    /**
     * Update order.
     *
     * @param array $data
     * @param $object
     * @return Order
     */
    public function update(array $data, $object): Order;
}