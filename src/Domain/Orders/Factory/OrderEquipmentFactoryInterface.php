<?php

declare(strict_types=1);

namespace App\Domain\Orders\Factory;

use App\Domain\Orders\Model\OrderEquipment;

/**
 * Interface OrderEquipmentFactoryInterface
 * @package App\Domain\Orders\Factory
 */
interface OrderEquipmentFactoryInterface
{
    /**
     * Create order equipment.
     *
     * @param array $data
     * @return OrderEquipment
     */
    public function create(array $data): OrderEquipment;
}