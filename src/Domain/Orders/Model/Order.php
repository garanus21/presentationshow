<?php

declare(strict_types=1);

namespace App\Domain\Orders\Model;

use App\Domain\Common\ValueObject\AggregateRoot;
use App\Domain\Dictionaries\Brands\Model\Brand;
use App\Domain\Dictionaries\Cabins\Model\Cabin;
use App\Domain\Dictionaries\Colors\Model\Color;
use App\Domain\Dictionaries\PassengerSeats\Model\PassengerSeat;
use App\Domain\Dictionaries\SleepingCabinBeds\Model\SleepingCabinBed;
use App\Domain\Dictionaries\SleepingCabinTypes\Model\SleepingCabinType;
use App\Domain\Orders\ValueObject\OrderId;

/**
 * Class Order
 * @package App\Domain\Orders\Model
 */
class Order extends AggregateRoot
{
    /**
     * @var OrderId
     */
    protected $uuid;

    /**
     * @var string
     */
    private $clientName;

    /**
     * @var string|null
     */
    private $clientAddress;

    /**
     * @var string|null
     */
    private $clientNip;

    /**
     * @var string|null
     */
    private $clientRegon;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     */
    private $deletedAt;

    /**
     * Status (eg. 0 - new, 1 - in progress, 2 - done, 3 - canceled)
     *
     * @var int
     */
    private $status;

    /**
     * @var string|null
     */
    private $orderNumber;

    /**
     * @var Cabin|null
     */
    private $cabin;

    /**
     * @var Brand|null
     */
    private $brand;

    /**
     * @var string|null
     */
    private $productionYear;

    /**
     * @var Color|null
     */
    private $color;

    /**
     * @var string|null
     */
    private $colorNumber;

    /**
     * @var string|null
     */
    private $vin;

    /**
     * @var string|null
     */
    private $placeAssembly;

    /**
     * @var \DateTime|null
     */
    private $orderCompletionTime;

    /**
     * @var \DateTime|null
     */
    private $laminated;

    /**
     * @var \DateTime|null
     */
    private $outcropped;

    /**
     * @var \DateTime|null
     */
    private $upholstered;

    /**
     * @var \DateTime|null
     */
    private $varnishing;

    /**
     * @var \DateTime|null
     */
    private $readyToAssembly;

    /**
     * @var \DateTime|null
     */
    private $assembly;

    /**
     * @var string|null
     */
    private $totalPrice;

    /**
     * @var string|null
     */
    private $comment;

    private $passengerSeat;
    private $sleepCabinBed;
    private $sleepCabinType;
    private $manualOrderNumber;

    /**
     * Order constructor.
     * @param OrderId $uuid
     * @param string $clientName
     * @param string|null $clientAddress
     * @param string|null $clientNip
     * @param string|null $clientRegon
     * @param \DateTime $createdAt
     * @param \DateTime|null $deletedAt
     * @param int $status
     * @param string|null $orderNumber
     * @param Cabin|null $cabin
     * @param Brand|null $brand
     * @param string|null $productionYear
     * @param Color|null $color
     * @param string|null $colorNumber
     * @param string|null $vin
     * @param string|null $placeAssembly
     * @param \DateTime|null $orderCompletionTime
     * @param \DateTime|null $laminated
     * @param \DateTime|null $outcropped
     * @param \DateTime|null $upholstered
     * @param \DateTime|null $varnishing
     * @param \DateTime|null $readyToAssembly
     * @param \DateTime|null $assembly
     * @param string|null $totalPrice
     * @param string|null $comment
     * @param PassengerSeat|null $passengerSeat
     * @param SleepingCabinBed|null $sleepCabinBed
     * @param SleepingCabinType|null $sleepCabinType
     * @param string|null $manualOrderNumber
     */
    public function __construct(
        OrderId $uuid,
        string $clientName,
        ?string $clientAddress,
        ?string $clientNip,
        ?string $clientRegon,
        \DateTime $createdAt,
        ?\DateTime $deletedAt,
        int $status,
        ?string $orderNumber,
        ?Cabin $cabin,
        ?Brand $brand,
        ?string $productionYear,
        ?Color $color,
        ?string $colorNumber,
        ?string $vin,
        ?string $placeAssembly,
        ?\DateTime $orderCompletionTime,
        ?\DateTime $laminated,
        ?\DateTime $outcropped,
        ?\DateTime $upholstered,
        ?\DateTime $varnishing,
        ?\DateTime $readyToAssembly,
        ?\DateTime $assembly,
        ?string $totalPrice,
        ?string $comment,
        ?PassengerSeat $passengerSeat,
        ?SleepingCabinBed $sleepCabinBed,
        ?SleepingCabinType $sleepCabinType,
        ?string $manualOrderNumber
    ) {
        parent::__construct($uuid);

        $this->clientName = $clientName;
        $this->clientAddress = $clientAddress;
        $this->clientNip = $clientNip;
        $this->clientRegon = $clientRegon;
        $this->createdAt = $createdAt;
        $this->deletedAt = $deletedAt;
        $this->status = $status;
        $this->orderNumber = $orderNumber;
        $this->cabin = $cabin;
        $this->brand = $brand;
        $this->productionYear = $productionYear;
        $this->color = $color;
        $this->colorNumber = $colorNumber;
        $this->vin = $vin;
        $this->placeAssembly = $placeAssembly;
        $this->orderCompletionTime = $orderCompletionTime;
        $this->laminated = $laminated;
        $this->outcropped = $outcropped;
        $this->upholstered = $upholstered;
        $this->varnishing = $varnishing;
        $this->readyToAssembly = $readyToAssembly;
        $this->assembly = $assembly;
        $this->totalPrice = $totalPrice;
        $this->comment = $comment;
        $this->passengerSeat = $passengerSeat;
        $this->sleepCabinBed = $sleepCabinBed;
        $this->sleepCabinType = $sleepCabinType;
        $this->manualOrderNumber = $manualOrderNumber;
    }

    /**
     * Create.
     *
     * @param OrderId $uuid
     * @param string $clientName
     * @param string|null $clientAddress
     * @param string|null $clientNip
     * @param string|null $clientRegon
     * @param \DateTime $createdAt
     * @param \DateTime|null $deletedAt
     * @param int $status
     * @param string|null $orderNumber
     * @param Cabin|null $cabin
     * @param Brand|null $brand
     * @param string|null $productionYear
     * @param Color|null $color
     * @param string|null $colorNumber
     * @param string|null $vin
     * @param string|null $placeAssembly
     * @param \DateTime|null $orderCompletionTime
     * @param \DateTime|null $laminated
     * @param \DateTime|null $outcropped
     * @param \DateTime|null $upholstered
     * @param \DateTime|null $varnishing
     * @param \DateTime|null $readyToAssembly
     * @param \DateTime|null $assembly
     * @param string|null $totalPrice
     * @param string|null $comment
     * @param PassengerSeat|null $passengerSeat
     * @param SleepingCabinBed|null $sleepCabinBed
     * @param SleepingCabinType|null $sleepCabinType
     * @param string|null $manualOrderNumber
     * @return static
     */
    public static function create(
        OrderId $uuid,
        string $clientName,
        ?string $clientAddress,
        ?string $clientNip,
        ?string $clientRegon,
        \DateTime $createdAt,
        ?\DateTime $deletedAt,
        int $status,
        ?string $orderNumber,
        ?Cabin $cabin,
        ?Brand $brand,
        ?string $productionYear,
        ?Color $color,
        ?string $colorNumber,
        ?string $vin,
        ?string $placeAssembly,
        ?\DateTime $orderCompletionTime,
        ?\DateTime $laminated,
        ?\DateTime $outcropped,
        ?\DateTime $upholstered,
        ?\DateTime $varnishing,
        ?\DateTime $readyToAssembly,
        ?\DateTime $assembly,
        ?string $totalPrice,
        ?string $comment,
        ?PassengerSeat $passengerSeat,
        ?SleepingCabinBed $sleepCabinBed,
        ?SleepingCabinType $sleepCabinType,
        ?string $manualOrderNumber
    ): self {
        return new self(
            $uuid,
            $clientName,
            $clientAddress,
            $clientNip,
            $clientRegon,
            $createdAt,
            $deletedAt,
            $status,
            $orderNumber,
            $cabin,
            $brand,
            $productionYear,
            $color,
            $colorNumber,
            $vin,
            $placeAssembly,
            $orderCompletionTime,
            $laminated,
            $outcropped,
            $upholstered,
            $varnishing,
            $readyToAssembly,
            $assembly,
            $totalPrice,
            $comment,
            $passengerSeat,
            $sleepCabinBed,
            $sleepCabinType,
            $manualOrderNumber
        );
    }

    /**
     * Update.
     *
     * @param string $clientName
     * @param string|null $clientAddress
     * @param string|null $clientNip
     * @param string|null $clientRegon
     * @param int $status
     * @param string|null $productionYear
     * @param string|null $colorNumber
     * @param string|null $vin
     * @param string|null $placeAssembly
     * @param \DateTime|null $orderCompletionTime
     * @param \DateTime|null $laminated
     * @param \DateTime|null $outcropped
     * @param \DateTime|null $upholstered
     * @param \DateTime|null $varnishing
     * @param \DateTime|null $readyToAssembly
     * @param \DateTime|null $assembly
     * @param string|null $totalPrice
     * @param string|null $comment
     * @param PassengerSeat|null $passengerSeat
     * @param SleepingCabinBed|null $sleepCabinBed
     * @param SleepingCabinType|null $sleepCabinType
     * @param string|null $manualOrderNumber
     * @return $this
     */
    public function update(
        string $clientName,
        ?string $clientAddress,
        ?string $clientNip,
        ?string $clientRegon,
        int $status,
        ?string $productionYear,
        ?string $colorNumber,
        ?string $vin,
        ?string $placeAssembly,
        ?\DateTime $orderCompletionTime,
        ?\DateTime $laminated,
        ?\DateTime $outcropped,
        ?\DateTime $upholstered,
        ?\DateTime $varnishing,
        ?\DateTime $readyToAssembly,
        ?\DateTime $assembly,
        ?string $totalPrice,
        ?string $comment,
        ?PassengerSeat $passengerSeat,
        ?SleepingCabinBed $sleepCabinBed,
        ?SleepingCabinType $sleepCabinType,
        ?string $manualOrderNumber
    ): self {
        $this->clientName = $clientName;
        $this->clientAddress = $clientAddress;
        $this->clientNip = $clientNip;
        $this->clientRegon = $clientRegon;
        $this->status = $status;
        $this->productionYear = $productionYear;
        $this->colorNumber = $colorNumber;
        $this->vin = $vin;
        $this->placeAssembly = $placeAssembly;
        $this->orderCompletionTime = $orderCompletionTime;
        $this->laminated = $laminated;
        $this->outcropped = $outcropped;
        $this->upholstered = $upholstered;
        $this->varnishing = $varnishing;
        $this->readyToAssembly = $readyToAssembly;
        $this->assembly = $assembly;
        $this->totalPrice = $totalPrice;
        $this->comment = $comment;
        $this->passengerSeat = $passengerSeat;
        $this->sleepCabinBed = $sleepCabinBed;
        $this->sleepCabinType = $sleepCabinType;
        $this->manualOrderNumber = $manualOrderNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getClientName(): string
    {
        return $this->clientName;
    }

    /**
     * @param string $clientName
     */
    public function setClientName(string $clientName): void
    {
        $this->clientName = $clientName;
    }

    /**
     * @return string|null
     */
    public function getClientAddress(): ?string
    {
        return $this->clientAddress;
    }

    /**
     * @param string|null $clientAddress
     */
    public function setClientAddress(?string $clientAddress): void
    {
        $this->clientAddress = $clientAddress;
    }

    /**
     * @return string|null
     */
    public function getClientNip(): ?string
    {
        return $this->clientNip;
    }

    /**
     * @param string|null $clientNip
     */
    public function setClientNip(?string $clientNip): void
    {
        $this->clientNip = $clientNip;
    }

    /**
     * @return string|null
     */
    public function getClientRegon(): ?string
    {
        return $this->clientRegon;
    }

    /**
     * @param string|null $clientRegon
     */
    public function setClientRegon(?string $clientRegon): void
    {
        $this->clientRegon = $clientRegon;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime|null $deletedAt
     */
    public function setDeletedAt(?\DateTime $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getOrderNumber(): ?string
    {
        return $this->orderNumber;
    }

    /**
     * @param string|null $orderNumber
     */
    public function setOrderNumber(?string $orderNumber): void
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return Cabin|null
     */
    public function getCabin(): ?Cabin
    {
        return $this->cabin;
    }

    /**
     * @param Cabin|null $cabin
     */
    public function setCabin(?Cabin $cabin): void
    {
        $this->cabin = $cabin;
    }

    /**
     * @return Brand|null
     */
    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    /**
     * @param Brand|null $brand
     */
    public function setBrand(?Brand $brand): void
    {
        $this->brand = $brand;
    }

    /**
     * @return string|null
     */
    public function getProductionYear(): ?string
    {
        return $this->productionYear;
    }

    /**
     * @param string|null $productionYear
     */
    public function setProductionYear(?string $productionYear): void
    {
        $this->productionYear = $productionYear;
    }

    /**
     * @return Color|null
     */
    public function getColor(): ?Color
    {
        return $this->color;
    }

    /**
     * @param Color|null $color
     */
    public function setColor(?Color $color): void
    {
        $this->color = $color;
    }

    /**
     * @return string|null
     */
    public function getColorNumber(): ?string
    {
        return $this->colorNumber;
    }

    /**
     * @param string|null $colorNumber
     */
    public function setColorNumber(?string $colorNumber): void
    {
        $this->colorNumber = $colorNumber;
    }

    /**
     * @return string|null
     */
    public function getVin(): ?string
    {
        return $this->vin;
    }

    /**
     * @param string|null $vin
     */
    public function setVin(?string $vin): void
    {
        $this->vin = $vin;
    }

    /**
     * @return string|null
     */
    public function getPlaceAssembly(): ?string
    {
        return $this->placeAssembly;
    }

    /**
     * @param string|null $placeAssembly
     */
    public function setPlaceAssembly(?string $placeAssembly): void
    {
        $this->placeAssembly = $placeAssembly;
    }

    /**
     * @return \DateTime|null
     */
    public function getOrderCompletionTime(): ?\DateTime
    {
        return $this->orderCompletionTime;
    }

    /**
     * @param \DateTime|null $orderCompletionTime
     */
    public function setOrderCompletionTime(?\DateTime $orderCompletionTime): void
    {
        $this->orderCompletionTime = $orderCompletionTime;
    }

    /**
     * @return \DateTime|null
     */
    public function getLaminated(): ?\DateTime
    {
        return $this->laminated;
    }

    /**
     * @param \DateTime|null $laminated
     */
    public function setLaminated(?\DateTime $laminated): void
    {
        $this->laminated = $laminated;
    }

    /**
     * @return \DateTime|null
     */
    public function getOutcropped(): ?\DateTime
    {
        return $this->outcropped;
    }

    /**
     * @param \DateTime|null $outcropped
     */
    public function setOutcropped(?\DateTime $outcropped): void
    {
        $this->outcropped = $outcropped;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpholstered(): ?\DateTime
    {
        return $this->upholstered;
    }

    /**
     * @param \DateTime|null $upholstered
     */
    public function setUpholstered(?\DateTime $upholstered): void
    {
        $this->upholstered = $upholstered;
    }

    /**
     * @return \DateTime|null
     */
    public function getVarnishing(): ?\DateTime
    {
        return $this->varnishing;
    }

    /**
     * @param \DateTime|null $varnishing
     */
    public function setVarnishing(?\DateTime $varnishing): void
    {
        $this->varnishing = $varnishing;
    }

    /**
     * @return \DateTime|null
     */
    public function getReadyToAssembly(): ?\DateTime
    {
        return $this->readyToAssembly;
    }

    /**
     * @param \DateTime|null $readyToAssembly
     */
    public function setReadyToAssembly(?\DateTime $readyToAssembly): void
    {
        $this->readyToAssembly = $readyToAssembly;
    }

    /**
     * @return \DateTime|null
     */
    public function getAssembly(): ?\DateTime
    {
        return $this->assembly;
    }

    /**
     * @param \DateTime|null $assembly
     */
    public function setAssembly(?\DateTime $assembly): void
    {
        $this->assembly = $assembly;
    }

    /**
     * @return string|null
     */
    public function getTotalPrice(): ?string
    {
        return $this->totalPrice;
    }

    /**
     * @param string|null $totalPrice
     */
    public function setTotalPrice(?string $totalPrice): void
    {
        $this->totalPrice = $totalPrice;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     */
    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return PassengerSeat|null
     */
    public function getPassengerSeat(): ?PassengerSeat
    {
        return $this->passengerSeat;
    }

    /**
     * @param PassengerSeat|null $passengerSeat
     */
    public function setPassengerSeat(?PassengerSeat $passengerSeat): void
    {
        $this->passengerSeat = $passengerSeat;
    }

    /**
     * @return SleepingCabinBed|null
     */
    public function getSleepCabinBed(): ?SleepingCabinBed
    {
        return $this->sleepCabinBed;
    }

    /**
     * @param SleepingCabinBed|null $sleepCabinBed
     */
    public function setSleepCabinBed(?SleepingCabinBed $sleepCabinBed): void
    {
        $this->sleepCabinBed = $sleepCabinBed;
    }

    /**
     * @return SleepingCabinType|null
     */
    public function getSleepCabinType(): ?SleepingCabinType
    {
        return $this->sleepCabinType;
    }

    /**
     * @param SleepingCabinType|null $sleepCabinType
     */
    public function setSleepCabinType(?SleepingCabinType $sleepCabinType): void
    {
        $this->sleepCabinType = $sleepCabinType;
    }

    /**
     * @return string|null
     */
    public function getManualOrderNumber(): ?string
    {
        return $this->manualOrderNumber;
    }

    /**
     * @param string|null $manualOrderNumber
     */
    public function setManualOrderNumber(?string $manualOrderNumber): void
    {
        $this->manualOrderNumber = $manualOrderNumber;
    }
}