<?php

declare(strict_types=1);

namespace App\Domain\Orders\Model;

use App\Domain\Common\ValueObject\AggregateRoot;
use App\Domain\Dictionaries\AdditionalEquipment\Model\AdditionalEquipment;
use App\Domain\Orders\ValueObject\OrderEquipmentId;

/**
 * Class OrderEquipment
 * @package App\Domain\Orders\Model
 */
class OrderEquipment extends AggregateRoot
{
    /**
     * @var OrderEquipmentId
     */
    protected $uuid;

    /**
     * @var Order|null
     */
    private $order;

    /**
     * @var AdditionalEquipment|null
     */
    private $additionalEquipment;

    /**
     * Comment.
     *
     * @var string|null
     */
    private $comment;

    /**
     * Price.
     *
     * @var string|null
     */
    private $price;

    /**
     * Created date.
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * OrderEquipment constructor.
     * @param OrderEquipmentId $uuid
     * @param Order|null $order
     * @param AdditionalEquipment|null $additionalEquipment
     * @param string|null $comment
     * @param string|null $price
     * @param \DateTime $createdAt
     */
    public function __construct(
        OrderEquipmentId $uuid,
        ?Order $order,
        ?AdditionalEquipment $additionalEquipment,
        ?string $comment,
        ?string $price,
        \DateTime $createdAt
    ) {
        parent::__construct($uuid);

        $this->order = $order;
        $this->additionalEquipment = $additionalEquipment;
        $this->comment = $comment;
        $this->price = $price;
        $this->createdAt = $createdAt;
    }

    /**
     * Create function.
     *
     * @param OrderEquipmentId $orderEquipmentId
     * @param Order|null $order
     * @param AdditionalEquipment|null $additionalEquipment
     * @param string|null $comment
     * @param string|null $price
     * @param \DateTime $createdAt
     * @return static
     */
    public static function create(
        OrderEquipmentId $orderEquipmentId,
        ?Order $order,
        ?AdditionalEquipment $additionalEquipment,
        ?string $comment,
        ?string $price,
        \DateTime $createdAt
    ): self {
        return new self(
            $orderEquipmentId,
            $order,
            $additionalEquipment,
            $comment,
            $price,
            $createdAt
        );
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return Order|null
     */
    public function getOrder(): ?Order
    {
        return $this->order;
    }

    /**
     * @param Order|null $order
     */
    public function setOrder(?Order $order): void
    {
        $this->order = $order;
    }

    /**
     * @return AdditionalEquipment|null
     */
    public function getAdditionalEquipment(): ?AdditionalEquipment
    {
        return $this->additionalEquipment;
    }

    /**
     * @param AdditionalEquipment|null $additionalEquipment
     */
    public function setAdditionalEquipment(?AdditionalEquipment $additionalEquipment): void
    {
        $this->additionalEquipment = $additionalEquipment;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     */
    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return string|null
     */
    public function getPrice(): ?string
    {
        return $this->price;
    }

    /**
     * @param string|null $price
     */
    public function setPrice(?string $price): void
    {
        $this->price = $price;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}