<?php

declare(strict_types=1);

namespace App\Domain\Security\Model;

use FOS\OAuthServerBundle\Entity\Client as BaseClient;

/**
 * Class Client
 *
 * @package App\Domain\Security\Model
 */
class Client extends BaseClient
{
    /**
     * @var int
     */
    protected $id;
}
