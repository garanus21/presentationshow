<?php

declare(strict_types=1);

namespace App\Domain\Security\Repository;

/**
 * Interface SecurityRepositoryInterface
 *
 * @package App\Domain\Security\Repository
 */
interface AccessTokenRepositoryInterface
{
    /**
     * Delete AccessToken by token.
     *
     * @param string $token
     */
    public function removeByToken(string $token): void;
}
