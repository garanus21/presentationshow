<?php

declare(strict_types=1);

namespace App\Domain\Security\Exception;

use Exception;

/**
 * Class AuthenticationException
 *
 * @package App\Domain\Security\Exception
 */
class AuthenticationException extends Exception
{
    /**
     * AuthenticationException constructor.
     */
    public function __construct()
    {
        parent::__construct('security.exception.authentication_exception');
    }
}
