<?php

declare(strict_types=1);

namespace App\Domain\Security\Exception;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class AccessDeniedException
 *
 * @package App\Domain\Security\Exception
 */
class AccessDeniedException extends AccessDeniedHttpException
{
    /**
     * AccessDeniedException constructor.
     */
    public function __construct()
    {
        parent::__construct('ACCESS_DENIED');
    }
}
