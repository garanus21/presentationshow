<?php

declare(strict_types=1);

namespace App\Domain\Security\Exception;

use InvalidArgumentException;

/**
 * Class InvalidPasswordException
 *
 * @package App\Domain\Security\Exception
 */
class InvalidPasswordException extends InvalidArgumentException
{
    /**
     * InvalidPasswordException constructor.
     */
    public function __construct()
    {
        parent::__construct("security.exception.invalid_password", 400);
    }
}
