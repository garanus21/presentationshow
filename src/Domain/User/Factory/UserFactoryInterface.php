<?php

declare(strict_types=1);

namespace App\Domain\User\Factory;

use App\Domain\User\Model\User;

/**
 * Interface UserFactoryInterface
 *
 * @package App\Domain\User\Factory
 */
interface UserFactoryInterface
{
    /**
     * Register user.
     *
     * @param array $data
     * @return User
     */
    public function register(array $data): User;

    /**
     * Reset password.
     *
     * @param array $data
     * @param $object
     * @return User
     */
    public function resetPassword(array $data, $object): User;

    /**
     * Update.
     *
     * @param array $data
     * @param $object
     * @return User
     */
    public function update(array $data, $object): User;
}
