<?php

declare(strict_types=1);

namespace App\Domain\User\Helper;

use App\Domain\User\Model\User;

/**
 * Interface UserMailerInterface
 *
 * @package App\Domain\User\Helper
 */
interface UserMailerInterface
{
    /**
     * Send forgot password email.
     *
     * @param User $user
     * @return int
     */
    public function sendForgotPasswordEmail(User $user): int;

    /**
     * Send user activation account e-mail.
     *
     * @param User $user
     * @param $hash
     * @return mixed
     */
    public function sendActivationEmail(User $user, $hash): int;
}
