<?php

declare(strict_types=1);

namespace App\Domain\User\Repository;

use App\Domain\User\Exception\UserNotFoundException;
use App\Domain\User\Model\User;
use App\Domain\User\ValueObject\UserId;
use App\Domain\Workspace\ValueObject\WorkspaceId;

/**
 * Interface UserRepositoryInterface
 *
 * @package App\Domain\User\Repository
 */
interface UserRepositoryInterface
{
    /**
     * Delete.
     *
     * @param User $user
     */
    public function delete(User $user): void;

    /**
     * Find all users.
     *
     * @return User[]
     */
    public function findAll();

    /**
     * Find one by username.
     *
     * @param string $username
     * @return User|null
     */
    public function findOneByUsername(string $username): ?User;

    /**
     * Find one by uuid.
     *
     * @param int $userId
     * @return User|null
     */
    public function findOneByUuid(int $userId): ?User;

    /**
     * Get one by code and not expired.
     *
     * @param string $code
     * @return User|null
     * @throws UserNotFoundException
     */
    public function getOneByCodeAndNotExpired(string $code): User;

    /**
     * Get one by email.
     *
     * @param string $email
     * @return User|null
     * @throws UserNotFoundException
     */
    public function getOneByEmail(string $email): User;

    /**
     * Get one by uuid.
     *
     * @param UserId $userId
     * @return User
     * @throws UserNotFoundException
     */
    public function getOneByUuid(int $userId): User;

    /**
     * Save.
     *
     * @param User $user
     */
    public function save(User $user): void;

    /**
     * Find by email.
     *
     * @param string $email
     * @return User|null
     */
    public function findOneByEmail(string $email): ?User;
}
