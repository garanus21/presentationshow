<?php

declare(strict_types=1);

namespace App\Domain\User\Exception;

use InvalidArgumentException;

/**
 * Class UserPasswordAreNotEquals
 *
 * @package App\Domain\User\Exception
 */
class UserPasswordAreNotEquals extends InvalidArgumentException
{
    /**
     * UserPasswordAreNotEquals constructor.
     */
    public function __construct()
    {
        parent::__construct('worker.password.not.equals');
    }
}
