<?php

declare(strict_types=1);

namespace App\Domain\User\ValueObject;

use DateTimeInterface;

/**
 * Class Reminder
 *
 * @package App\Domain\User\ValueObject
 */
final class Reminder
{
    /**
     * @var string|null
     */
    private $code;

    /**
     * @var DateTimeInterface|null
     */
    private $expiresAt;

    /**
     * Reminder constructor.
     *
     * @param string $code
     * @param DateTimeInterface $expiresAt
     */
    public function __construct(
        string $code,
        DateTimeInterface $expiresAt
    ) {
        $this->code = $code;
        $this->expiresAt = $expiresAt;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getExpiresAt(): ?DateTimeInterface
    {
        return $this->expiresAt;
    }

    /**
     * {@inheritDoc}
     */
    public function clear()
    {
        $this->code = null;
        $this->expiresAt = null;
    }
}
