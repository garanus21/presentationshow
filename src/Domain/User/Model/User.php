<?php

declare(strict_types=1);

namespace App\Domain\User\Model;

use App\Domain\Common\ValueObject\AggregateRoot;
use App\Domain\Security\ValueObject\AuthUser;
use App\Domain\Security\ValueObject\EncodedPasswordInterface;
use App\Domain\User\ValueObject\Reminder;
use App\Domain\User\ValueObject\UserId;
use App\Infrastructure\Common\Helper\TokenGenerator;
use DateTime;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\Encoder\EncoderAwareInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class User
 *
 * @package App\Domain\User\Model
 */
class User extends AggregateRoot implements UserInterface, EncoderAwareInterface
{
    /**
     * @var UserId
     */
    protected $uuid;
    
    /**
     * @var string
     */
    private $email;
    
    /**
     * @var string
     */
    private $firstName;
    
    /**
     * @var string
     */
    private $lastName;

    /**
     * @var AuthUser
     */
    private $auth;

    /**
     * @var Collection
     */
    private $accessTokens;

    /**
     * @var Collection
     */
    private $refreshTokens;

    /**
     * @var Reminder|null
     */
    private $reminder;

    /**
     * @var DateTime
     */
    private $createdAt;

    /**
     * @var null
     */
    private $deletedAt;

    /**
     * is super admin.
     *
     * @var bool
     */
    private $isSuperAdmin;

    /**
     * Check is activated account.
     *
     * @var bool
     */
    private $isActive;

    /**
     * Account is blocked.
     *
     * @var bool|null
     */
    private $isBlocked;

    /**
     * Activation hash.
     *
     * @var string|null
     */
    private $activationHash;

    /**
     * @var string|null
     */
    private $phone;

    // -------------------------------------------------------------------------

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return AuthUser
     */
    public function getAuth(): AuthUser
    {
        return $this->auth;
    }

    /**
     * @return Reminder|null
     */
    public function getReminder(): ?Reminder
    {
        return $this->reminder;
    }

    /**
     * @return bool
     */
    public function isSuperAdmin(): bool
    {
        return $this->isSuperAdmin;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    public function getEncoderName()
    {
        return 'hash';
    }

    /**
     * {@inheritDoc}
     */
    public function getRoles()
    {
        return $this->auth->roles();
    }

    /**
     * {@inheritDoc}
     */
    public function getPassword()
    {
        return $this->auth->password();
    }

    /**
     * {@inheritDoc}
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function getUsername()
    {
        return $this->auth->username();
    }

    /**
     *{@inheritDoc}
     */
    public function eraseCredentials()
    {
    }

    // -------------------------------------------------------------------------

    /**
     * User constructor.
     *
     * @param UserId $userId
     * @param string $email
     * @param string $firstName
     * @param string $lastName
     * @param EncodedPasswordInterface $encodedPassword
     * @param DateTime $createdAt
     * @param DateTime|null $deletedAt
     * @param bool $isSuperAdmin
     * @param bool $isActive
     * @param bool|null $isBlocked
     * @param string|null $activationHash
     * @param string|null $phone
     * @throws \Exception
     */
    public function __construct(
        UserId $userId,
        string $email,
        string $firstName,
        string $lastName,
        EncodedPasswordInterface $encodedPassword,
        DateTime $createdAt,
        ?DateTime $deletedAt,
        bool $isSuperAdmin,
        bool $isActive,
        ?bool $isBlocked,
        ?string $activationHash,
        ?string $phone
    ) {
        parent::__construct($userId);

        $this->auth = new AuthUser($email, $encodedPassword);
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->createdAt = new DateTime();
        $this->deletedAt = null;
        $this->isSuperAdmin = $isSuperAdmin;
        $this->isActive = $isActive;
        $this->isBlocked = $isBlocked;
        $this->activationHash = $activationHash;
        $this->phone = $phone;
    }

    /**
     * Register.
     *
     * @param UserId $userId
     * @param string $email
     * @param string $firstName
     * @param string $lastName
     * @param EncodedPasswordInterface $encodedPassword
     * @param DateTime $createdAt
     * @param DateTime|null $deletedAt
     * @param bool $isSuperAdmin
     * @param bool $isActive
     * @param bool|null $isBlocked
     * @param string|null $activationHash
     * @param string|null $phone
     * @return User
     * @throws \Exception
     */
    public static function register(
        UserId $userId,
        string $email,
        string $firstName,
        string $lastName,
        EncodedPasswordInterface $encodedPassword,
        DateTime $createdAt,
        ?DateTime $deletedAt,
        bool $isSuperAdmin,
        bool $isActive,
        ?bool $isBlocked,
        ?string $activationHash,
        ?string $phone
    ): self {
        return new self(
            $userId,
            $email,
            $firstName,
            $lastName,
            $encodedPassword,
            $createdAt,
            $deletedAt,
            $isSuperAdmin,
            $isActive,
            $isBlocked,
            $activationHash,
            $phone
        );
    }

    // -------------------------------------------------------------------------

    /**
     * Create remind password token and set date expires on.
     */
    public function remind()
    {
        $this->reminder = new Reminder(
            (new TokenGenerator())->generate(),
            (new DateTime())->modify('+1 hour')
        );
    }

    /**
     * Reset password.
     *
     * @param EncodedPasswordInterface $password
     */
    public function resetPassword(EncodedPasswordInterface $password): void
    {
        $this->auth->resetPassword($password);

        if (! is_null($this->reminder)) {
            $this->reminder->clear();
        }
    }

    /**
     * Update data.
     *
     * @param string $firstName
     * @param string $lastName
     * @param string|null $phone
     * @return User
     */
    public function update(
        string $firstName,
        string $lastName,
        string $phone
    ): self {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->phone = $phone;

        return $this;
    }

    /**
     * Update admin data.
     *
     * @param string $firstName
     * @param string $lastName
     * @param string $phone
     * @return User
     */
    public function updateAdmin(
        string $firstName,
        string $lastName,
        string $phone
    ): self {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->phone = $phone;

        return $this;
    }

    /**
     * Function delete.first
     *
     * @return DateTime
     * @throws \Exception
     */
    public function delete()
    {
        return $this->deletedAt = new DateTime();
    }

    /**
     * @return null
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set super admin.
     *
     * @return bool
     */
    public function setSuperAdmin()
    {
        return $this->isSuperAdmin = true;
    }

    /**
     * @return bool|null
     */
    public function getIsBlocked(): ?bool
    {
        return $this->isBlocked;
    }

    /**
     * Block account.
     *
     * @return bool
     */
    public function setBlocked()
    {
        return $this->isBlocked = true;
    }

    /**
     * Unlock account.
     *
     * @return bool
     */
    public function setUnlock()
    {
        return $this->isBlocked = false;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string|null
     */
    public function getActivationHash(): ?string
    {
        return $this->activationHash;
    }

    /**
     * @param string|null $activationHash
     */
    public function setActivationHash(?string $activationHash): void
    {
        $this->activationHash = $activationHash;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }
}
