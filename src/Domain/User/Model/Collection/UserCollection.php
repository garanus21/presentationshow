<?php

declare(strict_types=1);

namespace App\Domain\User\Model\Collection;

use App\Domain\Common\Model\Collection\AbstractCollection;

/**
 * Class UserCollection
 *
 * @package App\Domain\User\Model\Collection
 */
class UserCollection extends AbstractCollection
{
}
