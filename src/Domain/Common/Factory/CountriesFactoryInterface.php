<?php

declare(strict_types=1);

namespace App\Domain\Common\Factory;

use App\Domain\Common\Model\Countries;

/**
 * Class ListOfCountriesFactoryInterface
 *
 * @package App\Domain\Common\Factory
 */
interface CountriesFactoryInterface
{
    /**
     * Create new ListOfCountries.
     *
     * @param array $data
     * @return Countries
     */
    public function createCountry(array $data): Countries;
}
