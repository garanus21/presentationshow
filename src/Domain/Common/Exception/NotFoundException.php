<?php

declare(strict_types=1);

namespace App\Domain\Common\Exception;

use Exception;

/**
 * Class NotFoundException
 *
 * @package App\Domain\Common\Exception
 */
abstract class NotFoundException extends Exception
{
}
