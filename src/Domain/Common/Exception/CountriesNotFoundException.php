<?php

declare(strict_types=1);

namespace App\Domain\Common\Exception;

/**
 * Class ListOfCountriesNotFoundException
 *
 * @package App\Domain\Common\Exception
 */
class CountriesNotFoundException extends NotFoundException
{
    public function __construct()
    {
        /**
         * CountriesNotFoundException constructor.
         */
        parent::__construct('countries.exception.not_found', 2004);
    }
}
