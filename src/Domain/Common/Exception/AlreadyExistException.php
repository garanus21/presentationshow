<?php

declare(strict_types=1);

namespace App\Domain\Common\Exception;

use Exception;

/**
 * Class AlreadyExistException
 *
 * @package App\Domain\Common\Exception
 */
class AlreadyExistException extends Exception
{
    /**
     * AlreadyExistException constructor.
     */
    public function __construct()
    {
        parent::__construct('already_exist.exception', 409);
    }
}
