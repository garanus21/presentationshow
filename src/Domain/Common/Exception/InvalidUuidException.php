<?php

declare(strict_types=1);

namespace App\Domain\Common\Exception;

use InvalidArgumentException;

/**
 * Class InvalidUuidException
 *
 * @package App\Domain\Common\Exception
 */
class InvalidUuidException extends InvalidArgumentException
{
    /**
     * InvalidUuidException constructor.
     */
    public function __construct()
    {
        parent::__construct('aggregator_root.exception.invalid_uuid', 400);
    }
}
