<?php

declare(strict_types=1);

namespace App\Domain\Common\Repository;

use App\Domain\Common\Model\Countries;
use App\Domain\Common\ValueObject\CountriesId;

/**
 * Interface CountriesRepositoryInterface
 *
 * @package App\Domain\Common\Repository
 */
interface CountriesRepositoryInterface
{
    /**
     * @param CountriesId $countriesId
     * @return Countries|null
     */
    public function findOneByUuid(CountriesId $countriesId): ?Countries;

    /**
     * @return Countries[]
     */
    public function findAll();

    /**
     * Save.
     *
     * @param Countries $countriesId
     */
    public function save(Countries $countriesId): void;
}
