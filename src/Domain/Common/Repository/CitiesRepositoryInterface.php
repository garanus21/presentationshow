<?php

declare(strict_types=1);

namespace App\Domain\Common\Repository;

use App\Domain\Common\Model\Cities;

/**
 * Interface CitiesRepositoryInterface
 *
 * @package App\Domain\Common\Repository
 */
interface CitiesRepositoryInterface
{
    /**
     * @return Cities[]
     */
    public function findAll();

    /**
     * @param string $query
     * @return array
     */
    public function searchCity(string $query): array;
}
