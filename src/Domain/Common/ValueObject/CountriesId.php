<?php

declare(strict_types=1);

namespace App\Domain\Common\ValueObject;

/**
 * Class CountriesId
 *
 * @package App\Domain\Common\ValueObject
 */
class CountriesId extends AggregateRootId
{
}
