<?php

declare(strict_types=1);

namespace App\Domain\Common\ValueObject;

/**
 * Class CitiesId
 *
 * @package App\Domain\Common\ValueObject
 */
class CitiesId extends AggregateRootId
{
}
