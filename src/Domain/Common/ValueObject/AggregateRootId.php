<?php

declare(strict_types=1);

namespace App\Domain\Common\ValueObject;

use App\Domain\Common\Exception\InvalidUuidException;
use Exception;
use InvalidArgumentException;
use Ramsey\Uuid\Uuid;

/**
 * Class AggregateRootId
 *
 * @package App\Domain\Common\ValueObject
 */
abstract class AggregateRootId
{
    /**
     * @var int
     */
    protected $uuid;

    /**
     * AggregateRootId constructor.
     *
     * @param int|null $id
     * @throws Exception
     */
    public function __construct(?int $id = null)
    {
        try {
            $this->uuid =$id;
        } catch (InvalidArgumentException $e) {
            throw new InvalidUuidException();
        }
    }

    /**
     * @param AggregateRootId $aggregateRootId
     * @return bool
     */
    public function equals(AggregateRootId $aggregateRootId): bool
    {
        return $this->uuid === $aggregateRootId->__toString();
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->uuid;
    }

    /**
     * @return int
     */
    public function __toInt()
    {
        return (int) $this->uuid;
    }
}
