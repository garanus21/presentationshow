<?php

declare(strict_types=1);

namespace App\Domain\Common\ValueObject;

/**
 * Class AggregateRoot
 *
 * @package App\Domain\Common\ValueObject
 */
abstract class AggregateRoot
{
    /**
     * @var int
     */
    protected $uuid;

    /**
     * AggregateRoot constructor.
     *
     * @param AggregateRootId $aggregateRootId
     */
    protected function __construct(AggregateRootId $aggregateRootId)
    {
        $this->uuid = $aggregateRootId;
    }
    
    /**
     * @return int
     */
    public function uuid(): int
    {
        return $this->uuid;
    }
    
    /**
     * @param AggregateRootId $aggregateRootId
     * @return bool
     */
    final public function equals(AggregateRootId $aggregateRootId): bool
    {
        return $this->uuid->equals($aggregateRootId);
    }
    
    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->uuid;
    }
}
