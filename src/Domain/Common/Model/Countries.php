<?php

declare(strict_types=1);

namespace App\Domain\Common\Model;

use App\Domain\Common\ValueObject\AggregateRoot;
use App\Domain\Common\ValueObject\CountriesId;

/**
 * Class ListOfCountries
 *
 * @package App\Domain\Common\Model
 */
class Countries extends AggregateRoot
{
    /**
     * @var CountriesId
     */
    protected $uuid;

    /**
     * @var string
     */
    private $name;

    // -------------------------------------------------------------------------

    /**
     * Countries constructor.
     *
     * @param CountriesId $countriesId
     * @param string $name
     */
    public function __construct(
        CountriesId $countriesId,
        string $name
    ) {
        parent::__construct($countriesId);

        $this->name = $name;
    }

    /**
     * Create.
     *
     * @param CountriesId $countriesId
     * @param string $name
     * @return static
     */
    public static function create(
        CountriesId $countriesId,
        string $name
    ): self {
        return new self(
            $countriesId,
            $name
        );
    }
}
