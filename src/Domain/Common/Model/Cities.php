<?php

declare(strict_types=1);

namespace App\Domain\Common\Model;

use App\Domain\Common\ValueObject\AggregateRoot;
use App\Domain\Common\ValueObject\CitiesId;

/**
 * Class Cities
 *
 * @package App\Domain\Common\Model
 */
class Cities extends AggregateRoot
{
    /**
     * City uuid.
     *
     * @var CitiesId
     */
    protected $uuid;

    /**
     * City name.
     *
     * @var string
     */
    private $name;

    /**
     * Cities constructor.
     *
     * @param CitiesId $uuid
     * @param string $name
     */
    public function __construct(
        CitiesId $uuid,
        string $name
    ) {
        parent::__construct($uuid);

        $this->name = $name;
    }
}
