<?php

declare(strict_types=1);

namespace App\Domain\Common\Model\Collection;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class AbstractCollection
 *
 * @package App\Domain\Common\Model\Collection
 */
abstract class AbstractCollection extends ArrayCollection
{
}
