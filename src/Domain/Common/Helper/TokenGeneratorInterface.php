<?php

declare(strict_types=1);

namespace App\Domain\Common\Helper;

/**
 * Interface TokenGeneratorInterface
 *
 * @package App\Domain\Common\Helper
 */
interface TokenGeneratorInterface
{
    /**
     * Generate token.
     *
     * @return string
     */
    public function generate(): string;
}
