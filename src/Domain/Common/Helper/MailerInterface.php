<?php

declare(strict_types=1);

namespace App\Domain\Common\Helper;

/**
 * Interface MailerInterface
 *
 * @package App\Domain\Common\Helper
 */
interface MailerInterface
{
    /**
     * Send email.
     *
     * @return int
     */
    public function send(): int;
}
