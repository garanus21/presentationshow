<?php

declare(strict_types=1);

namespace App\Infrastructure\Dictionaries\Brands\Doctrine\Types;

use Doctrine\DBAL\Types\IntegerType;

/**
 * Class BrandIdType
 * @package App\Infrastructure\Dictionaries\Brands\Doctrine\Types
 */
class BrandIdType extends IntegerType
{
    const BRAND_ID = 'brandId';

    /**
     * @return string
     */
    public function getName()
    {
        return self::BRAND_ID;
    }
}