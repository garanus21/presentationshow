<?php

declare(strict_types=1);

namespace App\Infrastructure\Dictionaries\Brands\Repository;

use App\Domain\Dictionaries\Brands\Exception\BrandNotFoundException;
use App\Domain\Dictionaries\Brands\Model\Brand;
use App\Domain\Dictionaries\Brands\Repository\BrandRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class BrandRepository
 * @package App\Infrastructure\Dictionaries\Brands\Repository
 */
class BrandRepository extends ServiceEntityRepository implements BrandRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Brand::class);
    }

    /**
     * Delete.
     *
     * @param Brand $brand
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Brand $brand): void
    {
        $this->_em->remove($brand);
        $this->_em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return parent::findBy(
            [
                'deletedAt' => null
            ]
        );
    }

    /**
     * Save.
     *
     * @param Brand $brand
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Brand $brand): void
    {
        $this->_em->persist($brand);
        $this->_em->flush();
    }

    /**
     * Find one by Id.
     *
     * @param $brandId
     * @return Brand|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById($brandId): ?Brand
    {
        return $this->createQueryBuilder('b')
            ->where('b.uuid = :brandId')
            ->andWhere('b.deletedAt IS NULL')
            ->setParameter('brandId' , $brandId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Get one by id.
     *
     * @param $brandId
     * @return Brand|null
     * @throws BrandNotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneById($brandId): ?Brand
    {
        $brand = $this->findOneById($brandId);
        if (!$brand) {
            throw new BrandNotFoundException();
        }
        return $brand;
    }
}