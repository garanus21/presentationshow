<?php

declare(strict_types=1);

namespace App\Infrastructure\Dictionaries\PassengerSeats\Doctrine\Types;

use Doctrine\DBAL\Types\IntegerType;

/**
 * Class PassengerSeatIdType
 * @package App\Infrastructure\Dictionaries\PassengerSeats\Doctrine\Types
 */
class PassengerSeatIdType extends IntegerType
{
    const PASSENGER_SEAT_ID = 'passengerSeatId';

    /**
     * @return string
     */
    public function getName()
    {
        return self::PASSENGER_SEAT_ID;
    }
}