<?php

declare(strict_types=1);

namespace App\Infrastructure\Dictionaries\PassengerSeats\Repository;

use App\Domain\Dictionaries\PassengerSeats\Exception\PassengerSeatNotFoundException;
use App\Domain\Dictionaries\PassengerSeats\Model\PassengerSeat;
use App\Domain\Dictionaries\PassengerSeats\Repository\PassengerSeatRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class PassengerSeatRepository
 * @package App\Infrastructure\Dictionaries\PassengerSeats\Repository
 */
class PassengerSeatRepository extends ServiceEntityRepository implements PassengerSeatRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PassengerSeat::class);
    }

    /**
     * Delete.
     *
     * @param PassengerSeat $passengerSeat
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(PassengerSeat $passengerSeat): void
    {
        $this->_em->remove($passengerSeat);
        $this->_em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return parent::findBy(
            [
                'deletedAt' => null
            ]
        );
    }

    /**
     * Find cabin passenger seats.
     *
     * @param $cabinId
     * @return array
     */
    public function findByCabinId($cabinId): array
    {
        return $this->createQueryBuilder('ps')
            ->leftJoin('ps.cabin', 'c')
            ->where('c.uuid = :cabinId')
            ->andWhere('ps.deletedAt IS NULL')
            ->setParameter('cabinId' , $cabinId)
            ->getQuery()
            ->getResult();
    }

    /**
     * Save.
     *
     * @param PassengerSeat $passengerSeat
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(PassengerSeat $passengerSeat): void
    {
        $this->_em->persist($passengerSeat);
        $this->_em->flush();
    }

    /**
     * Find one by Id.
     *
     * @param $passengerSeatId
     * @return PassengerSeat|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById($passengerSeatId): ?PassengerSeat
    {
        return $this->createQueryBuilder('ps')
            ->where('ps.uuid = :passengerSeatId')
            ->andWhere('ps.deletedAt IS NULL')
            ->setParameter('passengerSeatId' , $passengerSeatId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Get one by id.
     *
     * @param $passengerSeatId
     * @return PassengerSeat|null
     * @throws PassengerSeatNotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneById($passengerSeatId): ?PassengerSeat
    {
        $passengerSeat = $this->findOneById($passengerSeatId);
        if (!$passengerSeat) {
            throw new PassengerSeatNotFoundException();
        }
        return $passengerSeat;
    }
}