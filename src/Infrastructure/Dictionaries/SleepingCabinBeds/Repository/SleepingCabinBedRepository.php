<?php

declare(strict_types=1);

namespace App\Infrastructure\Dictionaries\SleepingCabinBeds\Repository;

use App\Domain\Dictionaries\SleepingCabinBeds\Exception\SleepingCabinBedNotFoundException;
use App\Domain\Dictionaries\SleepingCabinBeds\Model\SleepingCabinBed;
use App\Domain\Dictionaries\SleepingCabinBeds\Repository\SleepingCabinBedRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class SleepingCabinBedRepository
 * @package App\Infrastructure\Dictionaries\SleepingCabinBeds\Repository
 */
class SleepingCabinBedRepository extends ServiceEntityRepository implements SleepingCabinBedRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SleepingCabinBed::class);
    }

    /**
     * Delete.
     *
     * @param SleepingCabinBed $sleepingCabinBed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(SleepingCabinBed $sleepingCabinBed): void
    {
        $this->_em->remove($sleepingCabinBed);
        $this->_em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return parent::findBy(
            [
                'deletedAt' => null
            ]
        );
    }

    /**
     * Find cabin sleeping cabin bed.
     *
     * @param $cabinId
     * @return array
     */
    public function findByCabinId($cabinId): array
    {
        return $this->createQueryBuilder('scb')
            ->leftJoin('scb.cabin', 'c')
            ->where('c.uuid = :cabinId')
            ->andWhere('scb.deletedAt IS NULL')
            ->setParameter('cabinId' , $cabinId)
            ->getQuery()
            ->getResult();
    }

    /**
     * Save.
     *
     * @param SleepingCabinBed $sleepingCabinBed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(SleepingCabinBed $sleepingCabinBed): void
    {
        $this->_em->persist($sleepingCabinBed);
        $this->_em->flush();
    }

    /**
     * Find one by Id.
     *
     * @param $sleepingCabinBedId
     * @return SleepingCabinBed|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById($sleepingCabinBedId): ?SleepingCabinBed
    {
        return $this->createQueryBuilder('scb')
            ->where('scb.uuid = :sleepingCabinBedId')
            ->andWhere('scb.deletedAt IS NULL')
            ->setParameter('sleepingCabinBedId' , $sleepingCabinBedId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Get one by id.
     *
     * @param $sleepingCabinBedId
     * @return SleepingCabinBed|null
     * @throws SleepingCabinBedNotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneById($sleepingCabinBedId): ?SleepingCabinBed
    {
        $sleepingCabinBed = $this->findOneById($sleepingCabinBedId);
        if (!$sleepingCabinBed) {
            throw new SleepingCabinBedNotFoundException();
        }
        return $sleepingCabinBed;
    }
}