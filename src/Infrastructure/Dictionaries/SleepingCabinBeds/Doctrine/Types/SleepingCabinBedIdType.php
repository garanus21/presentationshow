<?php

declare(strict_types=1);

namespace App\Infrastructure\Dictionaries\SleepingCabinBeds\Doctrine\Types;

use Doctrine\DBAL\Types\IntegerType;

/**
 * Class SleepingCabinBedIdType
 * @package App\Infrastructure\Dictionaries\SleepingCabinBeds\Doctrine\Types
 */
class SleepingCabinBedIdType extends IntegerType
{
    const SLEEPING_CABIN_BED_ID = 'sleepingCabinBedId';

    /**
     * @return string
     */
    public function getName()
    {
        return self::SLEEPING_CABIN_BED_ID;
    }
}