<?php

declare(strict_types=1);

namespace App\Infrastructure\Dictionaries\SleepingCabinTypes\Doctrine\Types;

use Doctrine\DBAL\Types\IntegerType;

/**
 * Class SleepingCabinTypeIdType
 * @package App\Infrastructure\Dictionaries\SleepingCabinTypes\Doctrine\Types
 */
class SleepingCabinTypeIdType extends IntegerType
{
    const SLEEPING_CABIN_TYPE_ID = 'sleepingCabinTypeId';

    /**
     * @return string
     */
    public function getName()
    {
        return self::SLEEPING_CABIN_TYPE_ID;
    }
}