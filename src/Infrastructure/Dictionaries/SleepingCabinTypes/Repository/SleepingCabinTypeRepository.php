<?php

declare(strict_types=1);

namespace App\Infrastructure\Dictionaries\SleepingCabinTypes\Repository;

use App\Domain\Dictionaries\SleepingCabinTypes\Exception\SleepingCabinTypeNotFoundException;
use App\Domain\Dictionaries\SleepingCabinTypes\Model\SleepingCabinType;
use App\Domain\Dictionaries\SleepingCabinTypes\Repository\SleepingCabinTypeRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class SleepingCabinTypeRepository
 * @package App\Infrastructure\Dictionaries\SleepingCabinTypes\Repository
 */
class SleepingCabinTypeRepository extends ServiceEntityRepository implements SleepingCabinTypeRepositoryInterface
{
    /**
     * SleepingCabinTypeRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SleepingCabinType::class);
    }

    /**
     * Delete.
     *
     * @param SleepingCabinType $sleepingCabinType
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(SleepingCabinType $sleepingCabinType): void
    {
        $this->_em->remove($sleepingCabinType);
        $this->_em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return parent::findBy(
            [
                'deletedAt' => null
            ]
        );
    }

    /**
     * Find cabin sleeping cabin type.
     *
     * @param $cabinId
     * @return array
     */
    public function findByCabinId($cabinId): array
    {
        return $this->createQueryBuilder('sct')
            ->leftJoin('sct.cabin', 'c')
            ->where('c.uuid = :cabinId')
            ->andWhere('sct.deletedAt IS NULL')
            ->setParameter('cabinId' , $cabinId)
            ->getQuery()
            ->getResult();
    }

    /**
     * Save.
     *
     * @param SleepingCabinType $sleepingCabinType
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(SleepingCabinType $sleepingCabinType): void
    {
        $this->_em->persist($sleepingCabinType);
        $this->_em->flush();
    }

    /**
     * Find one by Id.
     *
     * @param $sleepingCabinTypeId
     * @return SleepingCabinType|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById($sleepingCabinTypeId): ?SleepingCabinType
    {
        return $this->createQueryBuilder('sct')
            ->where('sct.uuid = :sleepingCabinTypeId')
            ->andWhere('sct.deletedAt IS NULL')
            ->setParameter('sleepingCabinTypeId' , $sleepingCabinTypeId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Get one by id.
     *
     * @param $sleepingCabinTypeId
     * @return SleepingCabinType|null
     * @throws SleepingCabinTypeNotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneById($sleepingCabinTypeId): ?SleepingCabinType
    {
        $sleepingCabinType = $this->findOneById($sleepingCabinTypeId);
        if (!$sleepingCabinType) {
            throw new SleepingCabinTypeNotFoundException();
        }
        return $sleepingCabinType;
    }
}