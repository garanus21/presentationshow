<?php

declare(strict_types=1);

namespace App\Infrastructure\Dictionaries\Colors\Repository;

use App\Domain\Dictionaries\Colors\Exception\ColorNotFoundException;
use App\Domain\Dictionaries\Colors\Model\Color;
use App\Domain\Dictionaries\Colors\Repository\ColorRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class ColorRepository
 * @package App\Infrastructure\Dictionaries\Colors\Repository
 */
class ColorRepository extends ServiceEntityRepository implements ColorRepositoryInterface
{
    /**
     * ColorRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Color::class);
    }

    /**
     * Delete.
     *
     * @param Color $color
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Color $color): void
    {
        $this->_em->remove($color);
        $this->_em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return parent::findBy(
            [
                'deletedAt' => null
            ]
        );
    }

    /**
     * Save.
     *
     * @param Color $color
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Color $color): void
    {
        $this->_em->persist($color);
        $this->_em->flush();
    }

    /**
     * Find one by Id.
     *
     * @param $colorId
     * @return Color|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById($colorId): ?Color
    {
        return $this->createQueryBuilder('c')
            ->where('c.uuid = :colorId')
            ->andWhere('c.deletedAt IS NULL')
            ->setParameter('colorId' , $colorId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Get one by id.
     *
     * @param $colorId
     * @return Color|null
     * @throws ColorNotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneById($colorId): ?Color
    {
        $color = $this->findOneById($colorId);
        if (!$color) {
            throw new ColorNotFoundException();
        }
        return $color;
    }
}