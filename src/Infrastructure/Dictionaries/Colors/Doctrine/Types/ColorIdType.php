<?php

declare(strict_types=1);

namespace App\Infrastructure\Dictionaries\Colors\Doctrine\Types;

use Doctrine\DBAL\Types\IntegerType;

/**
 * Class ColorIdType
 * @package App\Infrastructure\Dictionaries\Colors\Doctrine\Types
 */
class ColorIdType extends IntegerType
{
    const COLOR_ID = 'colorId';

    /**
     * @return string
     */
    public function getName()
    {
        return self::COLOR_ID;
    }
}