<?php

declare(strict_types=1);

namespace App\Infrastructure\Dictionaries\AdditionalEquipment\Repository;

use App\Domain\Dictionaries\AdditionalEquipment\Exception\AdditionalEquipmentNotFoundException;
use App\Domain\Dictionaries\AdditionalEquipment\Model\AdditionalEquipment;
use App\Domain\Dictionaries\AdditionalEquipment\Repository\AdditionalEquipmentRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class AdditionalEquipmentRepository
 * @package App\Infrastructure\Dictionaries\AdditionalEquipment\Repository
 */
class AdditionalEquipmentRepository extends ServiceEntityRepository implements AdditionalEquipmentRepositoryInterface
{
    /**
     * AdditionalEquipmentRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdditionalEquipment::class);
    }

    /**
     * Delete.
     *
     * @param AdditionalEquipment $additionalEquipment
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(AdditionalEquipment $additionalEquipment): void
    {
        $this->_em->remove($additionalEquipment);
        $this->_em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return parent::findBy(
            [
                'deletedAt' => null
            ],
            [
                'name'=> 'ASC'
            ]
        );
    }

    /**
     * Find cabin additional equipment.
     *
     * @param $cabinId
     * @return array
     */
    public function findByCabinId($cabinId): array
    {
        return $this->createQueryBuilder('a')
            ->leftJoin('a.cabin', 'c')
            ->where('c.uuid = :cabinId')
            ->andWhere('a.deletedAt IS NULL')
            ->setParameter('cabinId' , $cabinId)
            ->addOrderBy('a.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Save.
     *
     * @param AdditionalEquipment $additionalEquipment
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(AdditionalEquipment $additionalEquipment): void
    {
        $this->_em->persist($additionalEquipment);
        $this->_em->flush();
    }

    /**
     * Find one by Id.
     *
     * @param $additionalEquipmentId
     * @return AdditionalEquipment|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById($additionalEquipmentId): ?AdditionalEquipment
    {
        return $this->createQueryBuilder('a')
            ->where('a.uuid = :additionalEquipmentId')
            ->andWhere('a.deletedAt IS NULL')
            ->setParameter('additionalEquipmentId' , $additionalEquipmentId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Get one by id.
     *
     * @param $additionalEquipmentId
     * @return AdditionalEquipment|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws AdditionalEquipmentNotFoundException
     */
    public function getOneById($additionalEquipmentId): ?AdditionalEquipment
    {
        $additionalEquipment = $this->findOneById($additionalEquipmentId);
        if (!$additionalEquipment) {
            throw new AdditionalEquipmentNotFoundException();
        }
        return $additionalEquipment;
    }
}