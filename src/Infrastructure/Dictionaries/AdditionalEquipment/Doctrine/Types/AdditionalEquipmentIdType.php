<?php

declare(strict_types=1);

namespace App\Infrastructure\Dictionaries\AdditionalEquipment\Doctrine\Types;

use Doctrine\DBAL\Types\IntegerType;

/**
 * Class AdditionalEquipmentIdType
 * @package App\Infrastructure\Dictionaries\AdditionalEquipment\Doctrine\Types
 */
class AdditionalEquipmentIdType extends IntegerType
{
    const ADDITIONAL_EQUIPMENT_ID = 'additionalEquipmentId';

    /**
     * @return string
     */
    public function getName()
    {
        return self::ADDITIONAL_EQUIPMENT_ID;
    }
}