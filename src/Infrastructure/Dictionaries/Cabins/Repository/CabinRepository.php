<?php

declare(strict_types=1);

namespace App\Infrastructure\Dictionaries\Cabins\Repository;

use App\Domain\Dictionaries\Cabins\Exception\CabinNotFoundException;
use App\Domain\Dictionaries\Cabins\Model\Cabin;
use App\Domain\Dictionaries\Cabins\Repository\CabinRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class CabinRepository
 * @package App\Infrastructure\Dictionaries\Cabins\Repository
 */
class CabinRepository extends ServiceEntityRepository implements CabinRepositoryInterface
{
    /**
     * CabinRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cabin::class);
    }

    /**
     * Delete.
     *
     * @param Cabin $cabin
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Cabin $cabin): void
    {
        $this->_em->remove($cabin);
        $this->_em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return parent::findBy(
            [
                'deletedAt' => null
            ]
        );
    }

    /**
     * Save.
     *
     * @param Cabin $cabin
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Cabin $cabin): void
    {
        $this->_em->persist($cabin);
        $this->_em->flush();
    }

    /**
     * Find one by Id.
     *
     * @param $cabinId
     * @return Cabin|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById($cabinId): ?Cabin
    {
        return $this->createQueryBuilder('c')
            ->where('c.uuid = :cabinId')
            ->andWhere('c.deletedAt IS NULL')
            ->setParameter('cabinId' , $cabinId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Get one by id.
     *
     * @param $cabinId
     * @return Cabin|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws CabinNotFoundException
     */
    public function getOneById($cabinId): ?Cabin
    {
        $cabin = $this->findOneById($cabinId);
        if (!$cabin) {
            throw new CabinNotFoundException();
        }
        return $cabin;
    }
}