<?php

declare(strict_types=1);

namespace App\Infrastructure\Dictionaries\Cabins\Doctrine\Types;

use Doctrine\DBAL\Types\IntegerType;

/**
 * Class CabinIdType
 * @package App\Infrastructure\Dictionaries\Cabins\Doctrine\Types
 */
class CabinIdType extends IntegerType
{
    const CABIN_ID = 'cabinId';

    /**
     * @return string
     */
    public function getName()
    {
        return self::CABIN_ID;
    }
}