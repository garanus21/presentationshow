<?php

declare(strict_types=1);

namespace App\Infrastructure\Orders\Factory;

use App\Domain\Orders\Factory\OrderFactoryInterface;
use App\Domain\Orders\Model\Order;
use App\Infrastructure\Common\Factory\AbstractFactory;
use App\Infrastructure\Orders\Factory\Form\CreateOrderType;
use App\Infrastructure\Orders\Factory\Form\UpdateOrderType;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Class OrderFactory
 * @package App\Infrastructure\Orders\Factory
 */
class OrderFactory extends AbstractFactory implements OrderFactoryInterface
{
    /**
     * OrderFactory constructor.
     * @param FormFactoryInterface $factory
     */
    public function __construct(FormFactoryInterface $factory)
    {
        parent::__construct($factory);
    }

    /**
     * Create order.
     *
     * @param array $data
     * @return Order
     * @throws \App\Infrastructure\Common\Exception\Form\FormException
     */
    public function create(array $data): Order
    {
        $this->formClass = CreateOrderType::class;

        return $this->execute(self::CREATE, $data);
    }

    /**
     * Update order.
     *
     * @param array $data
     * @param $object
     * @return Order
     * @throws \App\Infrastructure\Common\Exception\Form\FormException
     */
    public function update(array $data, $object): Order
    {
        $this->formClass = UpdateOrderType::class;

        return $this->execute(self::UPDATE, $data, $object);
    }
}