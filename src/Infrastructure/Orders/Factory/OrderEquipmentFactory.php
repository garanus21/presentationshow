<?php

declare(strict_types=1);

namespace App\Infrastructure\Orders\Factory;

use App\Domain\Orders\Factory\OrderEquipmentFactoryInterface;
use App\Domain\Orders\Model\OrderEquipment;
use App\Infrastructure\Common\Factory\AbstractFactory;
use App\Infrastructure\Orders\Factory\Form\CreateOrderEquipmentType;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Class OrderEquipmentFactory
 * @package App\Infrastructure\Orders\Factory
 */
class OrderEquipmentFactory extends AbstractFactory implements OrderEquipmentFactoryInterface
{
    /**
     * OrderEquipmentFactory constructor.
     * @param FormFactoryInterface $factory
     */
    public function __construct(FormFactoryInterface $factory)
    {
        parent::__construct($factory);
    }

    /**
     * Create order equipment.
     *
     * @param array $data
     * @return OrderEquipment
     * @throws \App\Infrastructure\Common\Exception\Form\FormException
     */
    public function create(array $data): OrderEquipment
    {
        $this->formClass = CreateOrderEquipmentType::class;

        return $this->execute(self::CREATE, $data);
    }
}