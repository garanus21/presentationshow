<?php

declare(strict_types=1);

namespace App\Infrastructure\Orders\Factory\Form;

use App\Domain\Dictionaries\AdditionalEquipment\Exception\AdditionalEquipmentNotFoundException;
use App\Domain\Dictionaries\AdditionalEquipment\Repository\AdditionalEquipmentRepositoryInterface;
use App\Domain\Orders\Exception\OrderNotFoundException;
use App\Domain\Orders\Model\OrderEquipment;
use App\Domain\Orders\Repository\OrderRepositoryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class CreateOrderEquipmentType
 * @package App\Infrastructure\Orders\Factory\Form
 */
class CreateOrderEquipmentType extends AbstractType
{
    /**
     * Order repository.
     *
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * Additional equipment repository.
     *
     * @var AdditionalEquipmentRepositoryInterface
     */
    private $additionalEquipmentRepository;

    /**
     * CreateOrderEquipmentType constructor.
     * @param OrderRepositoryInterface $orderRepository
     * @param AdditionalEquipmentRepositoryInterface $additionalEquipmentRepository
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        AdditionalEquipmentRepositoryInterface $additionalEquipmentRepository
    ) {
        $this->orderRepository = $orderRepository;
        $this->additionalEquipmentRepository = $additionalEquipmentRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('uuid', null, [
                'mapped' => false,
            ])
            ->add(
                $builder
                    ->create('order_id', TextType::class, [
                        'mapped' => false,
                    ])
                    ->addModelTransformer(new CallbackTransformer(
                        function ($data) {
                            return $data;
                        },
                        function ($data) {
                            if (! $data){
                                throw new OrderNotFoundException();
                            }
                            else{
                                return $this->orderRepository->getOneById((int)$data);
                            }
                        }
                    ))
            )
            ->add(
                $builder
                    ->create('additional_equipment_id', TextType::class, [
                        'mapped' => false,
                    ])
                    ->addModelTransformer(new CallbackTransformer(
                        function ($data) {
                            return $data;
                        },
                        function ($data) {
                            if (! $data){
                                throw new AdditionalEquipmentNotFoundException();
                            }
                            else{
                                return $this->additionalEquipmentRepository->getOneById((int)$data);
                            }
                        }
                    ))
            )
            ->add('comment', TextType::class, [
                'mapped' => false,
            ])
            ->add('price', TextType::class, [
                'mapped' => false,
            ])
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => OrderEquipment::class,
            'empty_data' => function (FormInterface $form) {
                return OrderEquipment::create(
                    $form->get('uuid')->getData(),
                    $form->get('order_id')->getData(),
                    $form->get('additional_equipment_id')->getData(),
                    $form->get('comment')->getData(),
                    $form->get('price')->getData(),
                    new \DateTime()
                );
            },
        ]);
    }
}