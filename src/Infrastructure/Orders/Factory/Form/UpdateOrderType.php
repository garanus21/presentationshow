<?php

declare(strict_types=1);

namespace App\Infrastructure\Orders\Factory\Form;

use App\Domain\Dictionaries\PassengerSeats\Exception\PassengerSeatNotFoundException;
use App\Domain\Dictionaries\PassengerSeats\Repository\PassengerSeatRepositoryInterface;
use App\Domain\Dictionaries\SleepingCabinBeds\Exception\SleepingCabinBedNotFoundException;
use App\Domain\Dictionaries\SleepingCabinBeds\Repository\SleepingCabinBedRepositoryInterface;
use App\Domain\Dictionaries\SleepingCabinTypes\Exception\SleepingCabinTypeNotFoundException;
use App\Domain\Dictionaries\SleepingCabinTypes\Repository\SleepingCabinTypeRepositoryInterface;
use App\Domain\Orders\Model\Order;
use App\Infrastructure\Common\Factory\AbstractFactory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class UpdateOrderType
 * @package App\Infrastructure\Orders\Factory\Form
 */
class UpdateOrderType extends AbstractType
{
    private $passengersSeatRepository;
    private $sleepingCabinBedRepository;
    private $sleepingCabinTypeRepository;

    /**
     * UpdateOrderType constructor.
     * @param PassengerSeatRepositoryInterface $passengersSeatRepository
     * @param SleepingCabinBedRepositoryInterface $sleepingCabinBedRepository
     * @param SleepingCabinTypeRepositoryInterface $sleepingCabinTypeRepository
     */
    public function __construct(
        PassengerSeatRepositoryInterface $passengersSeatRepository,
        SleepingCabinBedRepositoryInterface $sleepingCabinBedRepository,
        SleepingCabinTypeRepositoryInterface $sleepingCabinTypeRepository
    ) {
        $this->passengersSeatRepository = $passengersSeatRepository;
        $this->sleepingCabinBedRepository = $sleepingCabinBedRepository;
        $this->sleepingCabinTypeRepository = $sleepingCabinTypeRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('uuid', null, [
                'mapped' => false,
            ])
            ->add('client_name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'order.exception.client_name.not_blank',
                    ]),
                ],
                'mapped' => false,
            ])
            ->add('client_address', TextType::class, [
                'mapped' => false,
            ])
            ->add('client_nip', TextType::class, [
                'mapped' => false,
            ])
            ->add('client_regon', TextType::class, [
                'mapped' => false,
            ])
            ->add('status', NumberType::class, [
                'mapped' => false,
            ])
            ->add('production_year', TextType::class, [
                'mapped' => false,
            ])
            ->add('color_number', TextType::class, [
                'mapped' => false,
            ])
            ->add('vin', TextType::class, [
                'mapped' => false,
            ])
            ->add('place_assembly', TextType::class, [
                'mapped' => false,
            ])
            ->add('order_completion_time', DateType::class, [
                'mapped' => false,
                'widget' => 'single_text',
            ])
            ->add('laminated', DateType::class, [
                'mapped' => false,
                'widget' => 'single_text',
            ])
            ->add('outcropped', DateType::class, [
                'mapped' => false,
                'widget' => 'single_text',
            ])
            ->add('upholstered', DateType::class, [
                'mapped' => false,
                'widget' => 'single_text',
            ])
            ->add('varnishing', DateType::class, [
                'mapped' => false,
                'widget' => 'single_text',
            ])
            ->add('ready_to_assembly', DateType::class, [
                'mapped' => false,
                'widget' => 'single_text',
            ])
            ->add('assembly', DateType::class, [
                'mapped' => false,
                'widget' => 'single_text',
            ])
            ->add('total_price', TextType::class, [
                'mapped' => false,
            ])
            ->add('comment', TextType::class, [
                'mapped' => false,
            ])
            ->add(
                $builder
                    ->create('passenger_seats_id', TextType::class, [
                        'mapped' => false,
                    ])
                    ->addModelTransformer(new CallbackTransformer(
                        function ($data) {
                            return $data;
                        },
                        function ($data) {
                            if (! $data){
                                null;
                            }
                            else{
                                return $this->passengersSeatRepository->getOneById((int)$data);
                            }
                        }
                    ))
            )
            ->add(
                $builder
                    ->create('sleep_cabin_beds_id', TextType::class, [
                        'mapped' => false,
                    ])
                    ->addModelTransformer(new CallbackTransformer(
                        function ($data) {
                            return $data;
                        },
                        function ($data) {
                            if (! $data){
                                null;
                            }
                            else{
                                return $this->sleepingCabinBedRepository->getOneById((int)$data);
                            }
                        }
                    ))
            )
            ->add(
                $builder
                    ->create('sleep_cabin_types_id', TextType::class, [
                        'mapped' => false,
                    ])
                    ->addModelTransformer(new CallbackTransformer(
                        function ($data) {
                            return $data;
                        },
                        function ($data) {
                            if (! $data){
                                null;
                            }
                            else{
                                return $this->sleepingCabinTypeRepository->getOneById((int)$data);
                            }
                        }
                    ))
            )
            ->add('manual_order_number', TextType::class, [
                'mapped' => false,
            ])
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => Order::class,
            'method' => AbstractFactory::UPDATE
        ]);
    }
}