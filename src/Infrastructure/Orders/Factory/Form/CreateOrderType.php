<?php

declare(strict_types=1);

namespace App\Infrastructure\Orders\Factory\Form;

use App\Domain\Dictionaries\Brands\Exception\BrandNotFoundException;
use App\Domain\Dictionaries\Brands\Repository\BrandRepositoryInterface;
use App\Domain\Dictionaries\Cabins\Exception\CabinNotFoundException;
use App\Domain\Dictionaries\Cabins\Repository\CabinRepositoryInterface;
use App\Domain\Dictionaries\Colors\Exception\ColorNotFoundException;
use App\Domain\Dictionaries\Colors\Repository\ColorRepositoryInterface;
use App\Domain\Dictionaries\PassengerSeats\Exception\PassengerSeatNotFoundException;
use App\Domain\Dictionaries\PassengerSeats\Repository\PassengerSeatRepositoryInterface;
use App\Domain\Dictionaries\SleepingCabinBeds\Exception\SleepingCabinBedNotFoundException;
use App\Domain\Dictionaries\SleepingCabinBeds\Repository\SleepingCabinBedRepositoryInterface;
use App\Domain\Dictionaries\SleepingCabinTypes\Exception\SleepingCabinTypeNotFoundException;
use App\Domain\Dictionaries\SleepingCabinTypes\Repository\SleepingCabinTypeRepositoryInterface;
use App\Domain\Orders\Model\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class CreateOrderType
 * @package App\Infrastructure\Orders\Factory\Form
 */
class CreateOrderType extends AbstractType
{
    /**
     * Cabin repository.
     *
     * @var CabinRepositoryInterface
     */
    private $cabinRepository;

    /**
     * Brand repository.
     *
     * @var BrandRepositoryInterface
     */
    private $brandRepository;

    /**
     * Color repository.
     *
     * @var ColorRepositoryInterface
     */
    private $colorRepository;

    private $passengersSeatRepository;
    private $sleepingCabinBedRepository;
    private $sleepingCabinTypeRepository;

    /**
     * CreateOrderType constructor.
     * @param CabinRepositoryInterface $cabinRepository
     * @param BrandRepositoryInterface $brandRepository
     * @param ColorRepositoryInterface $colorRepository
     * @param PassengerSeatRepositoryInterface $passengersSeatRepository
     * @param SleepingCabinBedRepositoryInterface $sleepingCabinBedRepository
     * @param SleepingCabinTypeRepositoryInterface $sleepingCabinTypeRepository
     */
    public function __construct(
        CabinRepositoryInterface $cabinRepository,
        BrandRepositoryInterface $brandRepository,
        ColorRepositoryInterface $colorRepository,
        PassengerSeatRepositoryInterface $passengersSeatRepository,
        SleepingCabinBedRepositoryInterface $sleepingCabinBedRepository,
        SleepingCabinTypeRepositoryInterface $sleepingCabinTypeRepository
    ) {
        $this->cabinRepository = $cabinRepository;
        $this->brandRepository = $brandRepository;
        $this->colorRepository = $colorRepository;
        $this->passengersSeatRepository = $passengersSeatRepository;
        $this->sleepingCabinBedRepository = $sleepingCabinBedRepository;
        $this->sleepingCabinTypeRepository = $sleepingCabinTypeRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('uuid', null, [
                'mapped' => false,
            ])
            ->add('client_name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'order.exception.client_name.not_blank',
                    ]),
                ],
                'mapped' => false,
            ])
            ->add('client_address', TextType::class, [
                'mapped' => false,
            ])
            ->add('client_nip', TextType::class, [
                'mapped' => false,
            ])
            ->add('client_regon', TextType::class, [
                'mapped' => false,
            ])
            ->add(
                $builder
                    ->create('cabin_id', TextType::class, [
                        'mapped' => false,
                    ])
                    ->addModelTransformer(new CallbackTransformer(
                        function ($data) {
                            return $data;
                        },
                        function ($data) {
                            if (! $data){
                                throw new CabinNotFoundException();
                            }
                            else{
                                return $this->cabinRepository->getOneById((int)$data);
                            }
                        }
                    ))
            )
            ->add(
                $builder
                    ->create('brand_id', TextType::class, [
                        'mapped' => false,
                    ])
                    ->addModelTransformer(new CallbackTransformer(
                        function ($data) {
                            return $data;
                        },
                        function ($data) {
                            if (! $data){
                                throw new BrandNotFoundException();
                            }
                            else{
                                return $this->brandRepository->getOneById((int)$data);
                            }
                        }
                    ))
            )
            ->add(
                $builder
                    ->create('color_id', TextType::class, [
                        'mapped' => false,
                    ])
                    ->addModelTransformer(new CallbackTransformer(
                        function ($data) {
                            return $data;
                        },
                        function ($data) {
                            if (! $data){
                                null;
                            }
                            else{
                                return $this->colorRepository->getOneById((int)$data);
                            }
                        }
                    ))
            )
            ->add('production_year', TextType::class, [
                'mapped' => false,
            ])
            ->add('color_number', TextType::class, [
                'mapped' => false,
            ])
            ->add('vin', TextType::class, [
                'mapped' => false,
            ])
            ->add('place_assembly', TextType::class, [
                'mapped' => false,
            ])
            ->add('order_completion_time', DateType::class, [
                'mapped' => false,
                'widget' => 'single_text',
            ])
            ->add('laminated', DateType::class, [
                'mapped' => false,
                'widget' => 'single_text',
            ])
            ->add('outcropped', DateType::class, [
                'mapped' => false,
                'widget' => 'single_text',
            ])
            ->add('upholstered', DateType::class, [
                'mapped' => false,
                'widget' => 'single_text',
            ])
            ->add('varnishing', DateType::class, [
                'mapped' => false,
                'widget' => 'single_text',
            ])
            ->add('ready_to_assembly', DateType::class, [
                'mapped' => false,
                'widget' => 'single_text',
            ])
            ->add('assembly', DateType::class, [
                'mapped' => false,
                'widget' => 'single_text',
            ])
            ->add('total_price', TextType::class, [
                'mapped' => false,
            ])
            ->add('comment', TextType::class, [
                'mapped' => false,
            ])
            ->add(
                $builder
                    ->create('passenger_seats_id', TextType::class, [
                        'mapped' => false,
                    ])
                    ->addModelTransformer(new CallbackTransformer(
                        function ($data) {
                            return $data;
                        },
                        function ($data) {
                            if (! $data){
                                null;
                            }
                            else{
                                return $this->passengersSeatRepository->getOneById((int)$data);
                            }
                        }
                    ))
            )
            ->add(
                $builder
                    ->create('sleep_cabin_beds_id', TextType::class, [
                        'mapped' => false,
                    ])
                    ->addModelTransformer(new CallbackTransformer(
                        function ($data) {
                            return $data;
                        },
                        function ($data) {
                            if (! $data){
                                null;
                            }
                            else{
                                return $this->sleepingCabinBedRepository->getOneById((int)$data);
                            }
                        }
                    ))
            )
            ->add(
                $builder
                    ->create('sleep_cabin_types_id', TextType::class, [
                        'mapped' => false,
                    ])
                    ->addModelTransformer(new CallbackTransformer(
                        function ($data) {
                            return $data;
                        },
                        function ($data) {
                            if (! $data){
                                null;
                            }
                            else{
                                return $this->sleepingCabinTypeRepository->getOneById((int)$data);
                            }
                        }
                    ))
            )
            ->add('manual_order_number', TextType::class, [
                'mapped' => false,
            ])
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => Order::class,
            'empty_data' => function (FormInterface $form) {
                return Order::create(
                    $form->get('uuid')->getData(),
                    $form->get('client_name')->getData(),
                    $form->get('client_address')->getData(),
                    $form->get('client_nip')->getData(),
                    $form->get('client_regon')->getData(),
                    new \DateTime(),
                    null,
                    0,
                    null,
                    $form->get('cabin_id')->getData(),
                    $form->get('brand_id')->getData(),
                    $form->get('production_year')->getData(),
                    $form->get('color_id')->getData(),
                    $form->get('color_number')->getData(),
                    $form->get('vin')->getData(),
                    $form->get('place_assembly')->getData(),
                    $form->get('order_completion_time')->getData(),
                    $form->get('laminated')->getData(),
                    $form->get('outcropped')->getData(),
                    $form->get('upholstered')->getData(),
                    $form->get('varnishing')->getData(),
                    $form->get('ready_to_assembly')->getData(),
                    $form->get('assembly')->getData(),
                    $form->get('total_price')->getData(),
                    $form->get('comment')->getData(),
                    $form->get('passenger_seats_id')->getData(),
                    $form->get('sleep_cabin_beds_id')->getData(),
                    $form->get('sleep_cabin_types_id')->getData(),
                    $form->get('manual_order_number')->getData()
                );
            },
        ]);
    }
}