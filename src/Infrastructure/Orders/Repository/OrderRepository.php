<?php

declare(strict_types=1);

namespace App\Infrastructure\Orders\Repository;

use App\Domain\Orders\Exception\OrderNotFoundException;
use App\Domain\Orders\Model\Order;
use App\Domain\Orders\Repository\OrderRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * Class OrderRepository
 * @package App\Infrastructure\Orders\Repository
 */
class OrderRepository extends ServiceEntityRepository implements OrderRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    /**
     * Delete.
     *
     * @param Order $order
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Order $order): void
    {
        $this->_em->remove($order);
        $this->_em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return parent::findBy(
            [
                'deletedAt' => null
            ]
        );
    }

    /**
     * Save.
     *
     * @param Order $order
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Order $order): void
    {
        $this->_em->persist($order);
        $this->_em->flush();
    }

    /**
     * Find one by Id.
     *
     * @param $orderId
     * @return Order|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById($orderId): ?Order
    {
        return $this->createQueryBuilder('o')
            ->where('o.uuid = :orderId')
            ->andWhere('o.deletedAt IS NULL')
            ->setParameter('orderId' , $orderId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Get one by id.
     *
     * @param $orderId
     * @return Order|null
     * @throws OrderNotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneById($orderId): ?Order
    {
        $order = $this->findOneById($orderId);
        if (!$order) {
            throw new OrderNotFoundException();
        }
        return $order;
    }


    /**
     * @param array $criteria
     * @return QueryBuilder
     */
    private function queryBuilderPaginatedBy(array $criteria): QueryBuilder
    {
        $qb = $this->createQueryBuilder('o');

        if (! empty($criteria)) {
            $i = 1;

            foreach ($criteria as $key => $value) {
                $value = ! is_array($value) ? [$value] : $value;
                $qb
                    ->andWhere("$key IN (:param_$i)")
                    ->setParameter("param_$i", $value);
                $i++;
            }
        }
        return $qb;
    }

    /**
     * {@inheritDoc}
     */
    public function findPaginatedOrders(array $criteria, int $limit, int $offset): array
    {
        return $this->queryBuilderPaginatedBy($criteria)
            ->andWhere('o.deletedAt IS NULL')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('o.createdAt', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * {@inheritDoc}
     */
    public function findPaginatedCountBy(array $criteria): int
    {
        return (int) $this->queryBuilderPaginatedBy($criteria)
            ->select($this->_em->getExpressionBuilder()->count('o.uuid'))
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param string $lastName
     * @param string $orderDate
     * @param string $equipmentName
     * @param string $productType
     * @param string $orderStatus
     * @param string $nip
     * @param string $orderNumber
     * @param string $orderCompletionTime
     * @param int $orderByDate
     * @param int $orderByClientName
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function searchPaginatedCountByQuery(
        string $lastName,
        string $orderDate,
        string $equipmentName,
        string $productType,
        string $orderStatus,
        string $nip,
        string $orderNumber,
        string $orderCompletionTime,
        int $orderByDate,
        int $orderByClientName
    ): int
    {
        return (int) $this->queryBuilderSearchByQueries(
            $lastName,
            $orderDate,
            $equipmentName,
            $productType,
            $orderStatus,
            $nip,
            $orderNumber,
            $orderCompletionTime,
            $orderByDate,
            $orderByClientName
        )
            ->select($this->_em->getExpressionBuilder()->count('o.uuid'))
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param string $lastName
     * @param string $orderDate
     * @param string $equipmentName
     * @param string $productType
     * @param string $orderStatus
     * @param string $nip
     * @param string $orderNumber
     * @param string $orderCompletionTime
     * @param int $orderByDate
     * @param int $orderByClientName
     * @return QueryBuilder
     */
    public function queryBuilderSearchByQueries(
        string $lastName,
        string $orderDate,
        string $equipmentName,
        string $productType,
        string $orderStatus,
        string $nip,
        string $orderNumber,
        string $orderCompletionTime,
        int $orderByDate,
        int $orderByClientName
    ): QueryBuilder
    {
        $qb = $this->createQueryBuilder('o')
            ->leftJoin('o.cabin', 'c')
            ->andWhere('o.deletedAt IS NULL');
        if (! empty($lastName)) {
            $qb
                ->andWhere("o.clientName LIKE :lastName")
                ->setParameter('lastName', '%'.$lastName.'%');
        }
        if (! empty($orderDate)) {
            $qb
                ->andWhere('o.createdAt LIKE :orderDate')
                ->setParameter('orderDate', $orderDate.'%');
        }
        if (! empty($orderCompletionTime)) {
            $qb
                ->andWhere('o.orderCompletionTime LIKE :orderCompletionTime')
                ->setParameter('orderCompletionTime', $orderCompletionTime.'%');
        }
        if (! empty($equipmentName)) {
            $qb
                ->leftJoin('App\Domain\Orders\Model\OrderEquipment', 'oe', 'WITH', 'oe.order = o.uuid')
                ->leftJoin('App\Domain\Dictionaries\AdditionalEquipment\Model\AdditionalEquipment', 'ae', 'WITH', 'ae.uuid = oe.additionalEquipment')
                ->andWhere("ae.name LIKE :equipmentName")
                ->setParameter('equipmentName', '%'.$equipmentName.'%');
        }
        if (! empty($productType)) {
            $qb
                ->andWhere("c.name LIKE :productType")
                ->setParameter('productType', '%'.$productType.'%');
        }
        if (! empty($orderStatus) || $orderStatus == '0') {
            $qb
                ->andWhere("o.status = :orderStatus")
                ->setParameter('orderStatus', $orderStatus);
        }
        if (! empty($nip)) {
            $qb
                ->andWhere("o.clientNip LIKE :nip")
                ->setParameter('nip', '%'.$nip.'%');
        }
        if ($orderByDate == 0) {
            null;
        } elseif ($orderByDate == 1) {
            $qb
                ->addOrderBy('o.createdAt', 'ASC');
        } elseif ($orderByDate == 2) {
            $qb
                ->addOrderBy('o.createdAt', 'DESC');
        } else {
            null;
        }
        if ($orderByClientName == 0) {
            null;
        } elseif ($orderByClientName == 1) {
            $qb
                ->addOrderBy('o.clientName', 'ASC');
        } elseif ($orderByClientName == 2) {
            $qb
                ->addOrderBy('o.clientName', 'DESC');
        } else {
            null;
        }
        if (! empty($orderNumber)) {
            $qb
                ->andWhere('o.manualOrderNumber LIKE :orderNumber')
                ->setParameter('orderNumber', $orderNumber.'%');
        }
        return $qb;
    }

    /**
     * @param string $lastName
     * @param string $orderDate
     * @param string $equipmentName
     * @param string $productType
     * @param string $orderStatus
     * @param string $nip
     * @param string $orderNumber
     * @param string $orderCompletionTime
     * @param int $orderByDate
     * @param int $orderByClientName
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function searchPaginatedByQueries(
        string $lastName,
        string $orderDate,
        string $equipmentName,
        string $productType,
        string $orderStatus,
        string $nip,
        string $orderNumber,
        string $orderCompletionTime,
        int $orderByDate,
        int $orderByClientName,
        int $limit,
        int $offset): array
    {
        return $this->queryBuilderSearchByQueries(
            $lastName,
            $orderDate,
            $equipmentName,
            $productType,
            $orderStatus,
            $nip,
            $orderNumber,
            $orderCompletionTime,
            $orderByDate,
            $orderByClientName
        )
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}