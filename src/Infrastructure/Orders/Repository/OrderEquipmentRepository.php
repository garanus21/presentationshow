<?php

declare(strict_types=1);

namespace App\Infrastructure\Orders\Repository;

use App\Domain\Orders\Exception\OrderEquipmentNotFoundException;
use App\Domain\Orders\Model\OrderEquipment;
use App\Domain\Orders\Repository\OrderEquipmentRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * Class OrderEquipmentRepository
 * @package App\Infrastructure\Orders\Repository
 */
class OrderEquipmentRepository extends ServiceEntityRepository implements OrderEquipmentRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderEquipment::class);
    }

    /**
     * Delete.
     *
     * @param OrderEquipment $orderEquipment
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(OrderEquipment $orderEquipment): void
    {
        $this->_em->remove($orderEquipment);
        $this->_em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return parent::findAll();
    }

    /**
     * Save.
     *
     * @param OrderEquipment $orderEquipment
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(OrderEquipment $orderEquipment): void
    {
        $this->_em->persist($orderEquipment);
        $this->_em->flush();
    }

    /**
     * Find one by Id.
     *
     * @param $orderEquipmentId
     * @return OrderEquipment|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById($orderEquipmentId): ?OrderEquipment
    {
        return $this->createQueryBuilder('oe')
            ->where('oe.uuid = :orderEquipmentId')
            ->setParameter('orderEquipmentId' , $orderEquipmentId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Get one by id.
     *
     * @param $orderEquipmentId
     * @return OrderEquipment|null
     * @throws OrderEquipmentNotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneById($orderEquipmentId): ?OrderEquipment
    {
        $orderEquipment = $this->findOneById($orderEquipmentId);
        if (!$orderEquipment) {
            throw new OrderEquipmentNotFoundException();
        }
        return $orderEquipment;
    }

    /**
     * Find by order id.
     *
     * @param $orderId
     * @return array
     */
    public function findByOrder($orderId): array
    {
        return $this->createQueryBuilder('oe')
            ->leftJoin('oe.order', 'o')
            ->where('o.uuid = :orderId')
            ->setParameter('orderId' , $orderId)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find by order and equipment.
     *
     * @param $orderId
     * @param $additionalEquipmentId
     * @return array
     */
    public function findByOrderAndEquipment($orderId, $additionalEquipmentId): array
    {
        return $this->createQueryBuilder('oe')
            ->leftJoin('oe.order', 'o')
            ->leftJoin('oe.additionalEquipment', 'ae')
            ->where('o.uuid = :orderId')
            ->andWhere('ae.uuid = :additionalEquipment')
            ->setParameters(
                [
                    'orderId' => $orderId,
                    'additionalEquipment' => $additionalEquipmentId
                ]
            )
            ->getQuery()
            ->getResult();
    }
}