<?php

declare(strict_types=1);

namespace App\Infrastructure\Orders\Doctrine\Types;

use Doctrine\DBAL\Types\IntegerType;

/**
 * Class OrderIdType
 * @package App\Infrastructure\Orders\Doctrine\Types
 */
class OrderIdType extends IntegerType
{
    const ORDER_ID = 'orderId';

    /**
     * @return string
     */
    public function getName()
    {
        return self::ORDER_ID;
    }
}