<?php

declare(strict_types=1);

namespace App\Infrastructure\Orders\Doctrine\Types;

use Doctrine\DBAL\Types\IntegerType;

/**
 * Class OrderEquipmentIdType
 * @package App\Infrastructure\Orders\Doctrine\Types
 */
class OrderEquipmentIdType extends IntegerType
{
    const ORDER_EQUIPMENT_ID = 'orderEquipmentId';

    /**
     * @return string
     */
    public function getName()
    {
        return self::ORDER_EQUIPMENT_ID;
    }
}