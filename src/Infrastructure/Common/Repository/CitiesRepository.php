<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Repository;

use App\Domain\Common\Model\Cities;
use App\Domain\Common\Repository\CitiesRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class CitiesRepository
 *
 * @package App\Infrastructure\Common\Repository
 */
class CitiesRepository extends ServiceEntityRepository implements CitiesRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cities::class);
    }

    /**
     * {@inheritDoc}
     */
    public function searchCity(string $query): array
    {
        return $this->createQueryBuilder('c')
            ->where('c.name LIKE :query')
            ->setParameter('query', '%'.$query.'%')
            ->orderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
