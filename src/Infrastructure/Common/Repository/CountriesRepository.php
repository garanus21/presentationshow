<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Repository;

use App\Domain\Common\Model\Countries;
use App\Domain\Common\Repository\CountriesRepositoryInterface;
use App\Domain\Common\ValueObject\CountriesId;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class CountriesRepository
 *
 * @package App\Infrastructure\Common\Repository
 */
class CountriesRepository extends ServiceEntityRepository implements CountriesRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Countries::class);
    }

    /**
     * @param CountriesId $countriesId
     * @return Countries|null
     * @throws NonUniqueResultException
     */
    public function findOneByUuid(CountriesId $countriesId): ?Countries
    {
        return $this->createQueryBuilder('c')
            ->where('c.uuid = :countriesId')
            ->setParameter('countriesId', $countriesId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Save.
     *
     * @param Countries $countriesId
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Countries $countriesId): void
    {
        $this->_em->persist($countriesId);
        $this->_em->flush();
    }

    /**
     * @return Countries[]
     */
    public function findAll(): array
    {
        return $this->createQueryBuilder('c')
            ->orderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
