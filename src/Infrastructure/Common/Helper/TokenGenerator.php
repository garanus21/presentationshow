<?php 

declare(strict_types=1);

namespace App\Infrastructure\Common\Helper;

use App\Domain\Common\Helper\TokenGeneratorInterface;

/**
 * Class TokenGenerator
 */
class TokenGenerator implements TokenGeneratorInterface
{
    /**
     * @var int
     */
    private $entropy;

    /**
     * TokenGenerator constructor.
     *
     * @param int $length
     */
    public function __construct(int $length = 32)
    {
        $this->entropy = (intval(($length - 8) * 8));
    }

    /**
     * {@inheritDoc}
     */
	public function generate(): string
	{
		$bytes = random_bytes($this->entropy / 8);

        return rtrim(strtr(base64_encode($bytes), '+/', '-_'), '=');
	}
}
