<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Helper;

use App\Domain\Common\Helper\MailerInterface;
use Swift_Mailer;
use Twig\Environment;

/**
 * Class Mailer
 *
 * @package App\Infrastructure\Common\Helper
 */
class Mailer implements MailerInterface
{
    /**
     * @var Swift_Mailer
     */
    protected $mailer;

    /**
     * @var Environment
     */
    protected $twig;

    /**
     * @var \Swift_Message
     */
    protected $message;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $name;

    /**
     * Mailer constructor.
     *
     * @param Swift_Mailer $mailer
     * @param Environment $twig
     * @param string $mailerFromEmail
     * @param string $mailerFromName
     */
    public function __construct(
        Swift_Mailer $mailer,
        Environment $twig,
        string $mailerFromEmail,
        string $mailerFromName
    ) {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->email = $mailerFromEmail;
        $this->name = $mailerFromName;

        $this->initMessage();
    }

    /**
     * {@inheritDoc}
     */
    public function send(): int
    {
        return $this->mailer->send($this->message);
    }

    /**
     * Init message.
     */
    private function initMessage()
    {
        $from = $this->email;

        if (! empty($this->name)) {
            $from = [$this->email => $this->name];
        }

        $this->message = (new \Swift_Message())->setFrom($from);
    }

    /**
     * Render message template
     *
     * @param string $view
     * @param array $params
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function render(string $view, array $params = []): string
    {
        return $this->twig->render($view, array_merge($params, [
            'message' => $this->message,
            'subject' => $this->message->getSubject(),
        ]));
    }
}
