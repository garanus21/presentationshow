<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Pagination;

/**
 * Trait PagerTrait
 *
 * @package App\Infrastructure\Common\Pagination
 */
trait PagerTrait
{
    /**
     * @param int $limit
     * @return int
     */
    private function limit(int $limit = 100): int
    {
        if ($limit < 1 || $limit > 100) {
            $limit = 100;
        }

        return $limit;
    }

    /**
     * @param int $page
     * @param int $limit
     * @return int
     */
    private function offset(int $page, int $limit): int
    {
        $offset = 0;

        if ($page > 1) {
            $offset = ($page - 1) * $limit;
        }

        return $offset;
    }

    /**
     * @param int $page
     * @return int
     */
    private function page(int $page = 1): int
    {
        if ($page < 1) {
            $page = 1;
        }

        return $page;
    }
}
