<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Symfony\Validator\Constraints;

use DateTime;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class PeselValidator
 *
 * @package App\Infrastructure\Common\Symfony\Validator\Constraints
 */
class PeselValidator extends ConstraintValidator
{
    /**
     * Checks if the passed value is valid.
     *
     * @param $value
     * @param Pesel|Constraint $constraint The constraint for the validation
     * @return void
     */
    public function validate($value, Constraint $constraint)
    {
        $valid = true;

        if ($value) {
            if (strlen($value) == 11 && ctype_digit($value)) {
                $weights = [9, 7, 3, 1, 9, 7, 3, 1, 9, 7];
                $sum = 0;

                for ($i = 0; $i < 10; $i++) {
                    $sum += $weights[$i] * $value[$i];
                }

                if ($sum % 10 == $value[10]) {
                    $year = (int) substr($value,0, 2);
                    $month = (int) substr($value,2, 2);
                    $day = (int) substr($value, 4, 2);

                    if ($month >= 1 && $month <= 12) {
                        $year += 1900;
                    } elseif ($month >= 21 && $month <= 32) {
                        $year += 2000;
                        $month -= 20;
                    } elseif ($month >= 41 && $month <= 52){
                        $year += 2100;
                        $month -= 40;
                    } elseif ($month >= 61 && $month <= 72){
                        $year += 2200;
                        $month -= 60;
                    } elseif ($month >= 81 && $month <= 92){
                        $year += 1800;
                        $month -= 80;
                    } else {
                        $valid = false;
                    }

                    if ($valid) {
                        $dateOfBirth = new DateTime($year.'-'.$month);

                        if ($day <= $dateOfBirth->format('t')) {
                            $dateOfBirth->setDate($year, $month, $day);

                            if ($dateOfBirth > new DateTime()) {
                                $valid = false;
                            }
                        } else {
                            $valid = false;
                        }
                    }
                } else {
                    $valid = false;
                }
            } else {
                $valid = false;
            }
        }

        if (! $valid) {
            $this->context->addViolation($constraint->message);
        }
    }
}
