<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Symfony\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class PeselConstraint
 *
 * @package App\Infrastructure\Common\Symfony\Validator\Constraints
 */
class Pesel extends Constraint
{
    /**
     * @var string
     */
    public $message = 'This value is not a valid PESEL number.';
}
