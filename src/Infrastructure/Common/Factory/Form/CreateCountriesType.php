<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Factory\Form;

use App\Domain\Common\Model\Countries;
use App\Infrastructure\Common\Factory\AbstractFactory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CreateCountriesType
 *
 * @package App\Infrastructure\Common\Factory\Form
 */
class CreateCountriesType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('uuid', null, [
                'mapped' => false,
            ])
            ->add('name', TextType::class, [
                'mapped' => false,
            ]);
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => Countries::class,
            'method' => AbstractFactory::CREATE,
            'empty_data' => function (FormInterface $form) {
                return Countries::create(
                    $form->get('uuid')->getData(),
                    (string) $form->get('name')->getData()
                );
            }
        ]);
    }
}
