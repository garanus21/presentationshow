<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Factory;

use App\Domain\Common\Factory\CountriesFactoryInterface;
use App\Domain\Common\Model\Countries;
use App\Infrastructure\Common\Exception\Form\FormException;
use App\Infrastructure\Common\Factory\Form\CreateCountriesType;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Class CountriesFactory
 *
 * @package App\Infrastructure\Common\Factory
 */
class CountriesFactory extends AbstractFactory implements CountriesFactoryInterface
{
    /**
     * CountriesFactory constructor.
     *
     * @param FormFactoryInterface $factory
     */
    public function __construct(FormFactoryInterface $factory)
    {
        parent::__construct($factory);
    }

    /**
     * Create new ListOfCountries.
     *
     * @param array $data
     * @return Countries
     * @throws FormException
     */
    public function createCountry(array $data): Countries
    {
        $this->formClass = CreateCountriesType::class;

        return $this->execute(self::CREATE, $data);
    }
}
