<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Exception\Form;

use LogicException;

/**
 * Class FormFactoryException
 *
 * @package App\Infrastructure\Common\Exception\Form
 */
class FormFactoryException extends LogicException
{
    /**
     * FormFactoryInterface constructor.
     */
    public function __construct()
    {
        parent::__construct('form.factory.exception.form_class_required', 5005);
    }
}
