<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Doctrine\Types;

use App\Domain\Common\ValueObject\AggregateRootId;
use App\Domain\Common\ValueObject\CountriesId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;
use Ramsey\Uuid\Doctrine\UuidBinaryType;

/**
 * Class CountriesIdType
 *
 * @package App\Infrastructure\Common\Doctrine\Types
 */
class CountriesIdType extends IntegerType
{
    const COUNTRIES_ID = 'countriesId';

    /**
     * @return string
     */
    public function getName()
    {
        return self::COUNTRIES_ID;
    }
}
