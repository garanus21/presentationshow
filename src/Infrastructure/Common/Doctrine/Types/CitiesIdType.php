<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Doctrine\Types;

use App\Domain\Common\ValueObject\AggregateRootId;
use App\Domain\Common\ValueObject\CitiesId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;
use Ramsey\Uuid\Doctrine\UuidBinaryType;

/**
 * Class CitiesIdType
 *
 * @package App\Infrastructure\Common\Doctrine\Types
 */
class CitiesIdType extends IntegerType
{
    const CITIES_ID = 'citiesId';

    /**
     * @return string
     */
    public function getName()
    {
        return self::CITIES_ID;
    }
}
