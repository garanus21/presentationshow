<?php

declare(strict_types=1);

namespace App\Infrastructure\Security\Provider;

use App\Domain\Security\Exception\AccessDeniedException;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Domain\Workspace\Exception\ActiveWorkspaceNotFoundException;
use App\Domain\Workspace\Repository\WorkspaceRepositoryInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class UserProvider
 *
 * @package App\Infrastructure\Security\Provider
 */
class UserProvider implements UserProviderInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $repository;

    /**
     * UserProvider constructor.
     *
     * @param UserRepositoryInterface $repository
     */
    public function __construct(
        UserRepositoryInterface $repository
    ) {
        $this->repository = $repository;
    }

    /**
     * {@inheritDoc}
     */
    public function loadUserByUsername($username)
    {
        $user = $this->repository->findOneByUsername($username);

        if (! $user) {
            throw new UsernameNotFoundException();
        }
        if ($user->getDeletedAt()) {
            throw new AccessDeniedException();
        }

        return $user;
    }

    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user)
    {
        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * {@inheritDoc}
     */
    public function supportsClass($class)
    {
        return true;
    }
}
