<?php

declare(strict_types=1);

namespace App\Infrastructure\Security\Repository;

use App\Domain\Security\Model\AccessToken;
use App\Domain\Security\Repository\AccessTokenRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class SecurityRepository
 *
 * @package App\Infrastructure\Security\Repository
 */
class AccessTokenRepository extends ServiceEntityRepository implements AccessTokenRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccessToken::class);
    }

    /**
     * {@inheritDoc}
     */
    public function removeByToken(string $token): void
    {
        $this->createQueryBuilder('a')
            ->delete()
            ->where('a.token = :token')
            ->setParameter('token', $token)
            ->getQuery()
            ->execute();
    }
}
