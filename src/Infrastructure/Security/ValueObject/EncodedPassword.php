<?php

declare(strict_types=1);

namespace App\Infrastructure\Security\ValueObject;

use App\Domain\Security\Exception\NullPasswordException;
use App\Domain\Security\ValueObject\EncodedPasswordInterface;
use Symfony\Component\Security\Core\Encoder\NativePasswordEncoder;

/**
 * Class EncodedPassword
 *
 * @package App\Infrastructure\Security\ValueObject
 */
class EncodedPassword implements EncodedPasswordInterface
{
    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $plainPassword;

    /**
     * @var NativePasswordEncoder
     */
    private $encoder;

    /**
     * {@inheritDoc}
     */
    public function __construct(?string $plainPassword = null)
    {
        if (null === $plainPassword){
            throw new NullPasswordException();
        }

        $this->encoder = new NativePasswordEncoder();

        $this->setPassword($plainPassword);
    }

    /**
     * @param string $plainPassword
     */
    private function setPassword(string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;

        $this->password = $this->encoder->encodePassword($plainPassword, null);
    }

    /**
     * {@inheritDoc}
     */
    public function matchHash(string $hash): bool
    {
        return password_verify($this->plainPassword, $hash);
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return $this->password;
    }
}
