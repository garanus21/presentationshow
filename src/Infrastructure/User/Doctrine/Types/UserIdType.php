<?php

declare(strict_types=1);

namespace App\Infrastructure\User\Doctrine\Types;

use App\Domain\User\ValueObject\UserId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;
use Exception;
use Ramsey\Uuid\Doctrine\UuidBinaryType;

/**
 * Class UserIdType
 *
 * @package App\Infrastructure\User\Doctrine\Types
 */
class UserIdType extends IntegerType
{
    const USER_ID = 'userId';
    
    /**
     * @return string
     */
    public function getName()
    {
        return self::USER_ID;
    }
}
