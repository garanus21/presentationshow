<?php

declare(strict_types=1);

namespace App\Infrastructure\User\Helper;

use App\Domain\User\Helper\UserMailerInterface;
use App\Domain\User\Model\User;
use App\Infrastructure\Common\Helper\Mailer;

/**
 * Class UserMailer
 *
 * @package App\Infrastructure\User\Helper
 */
class UserMailer extends Mailer implements UserMailerInterface
{
    /**
     * {@inheritDoc}
     */
    public function sendForgotPasswordEmail(User $user): int
    {
        $this->message
            ->setTo($user->getEmail())
            ->setSubject('Aplikacja Plandeki | Zapomniałem hasła')
            ->setBody(
                $this->render('emails/users/forgot_password.html.twig', [
                    'user' => $user,
                ]),
                'text/html'
            );

        return $this->send();
    }

    /**
     * @inheritDoc
     */
    public function sendActivationEmail(User $user, $hash): int
    {
        $this->message
            ->setTo($user->getEmail())
            ->setSubject('Aplikacja Plandeki | Aktywacja konta')
            ->setBody(
                $this->render('emails/users/activate_account.html.twig', [
                    'user' => $user,
                    'hash' => $hash,
                ]),
                'text/html'
            );

        return $this->send();
    }
}
