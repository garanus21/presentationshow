<?php

declare(strict_types=1);

namespace App\Infrastructure\User\Factory\Form;

use App\Domain\User\Model\User;
use App\Infrastructure\Common\Factory\AbstractFactory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class UpdateType
 *
 * @package App\Infrastructure\User\Factory\Form
 */
class UpdateType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('uuid', null, [
                'mapped' => false
            ])
            ->add('first_name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'user.exception.first_name.not_blank',
                    ]),
                ],
                'mapped' => false,
            ])
            ->add('last_name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'user.exception.last_name.not_blank',
                    ]),
                ],
                'mapped' => false,
            ])
            ->add('phone', TextType::class, [
                'mapped' => false,
            ])
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => User::class,
            'method' => AbstractFactory::UPDATE,
        ]);
    }
}
