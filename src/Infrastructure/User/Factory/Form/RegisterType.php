<?php

declare(strict_types=1);

namespace App\Infrastructure\User\Factory\Form;

use App\Domain\User\Model\User;
use App\Infrastructure\Security\ValueObject\EncodedPassword;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class CreateType
 *
 * @package App\Infrastructure\User\Factory\Form
 */
class RegisterType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('uuid', null, [
                'mapped' => false,
            ])
            ->add('email', EmailType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'user.exception.email.not_blank',
                    ]),
                    new NotNull([
                        'message' => 'user.exception.null.not_null',
                    ]),
                    new Email([
                        'message' => 'user.exception.email.not_valid',
                    ]),
                ],
                'mapped' => false,
            ])
            ->add('first_name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'user.exception.first_name.not_blank',
                    ]),
                ],
                'mapped' => false,
            ])
            ->add('last_name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'user.exception.last_name.not_blank',
                    ]),
                ],
                'mapped' => false,
            ])
            ->add('password', PasswordType::class, [
                'mapped' => false
            ])
            ->add('phone', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'user.exception.phone.not_blank',
                    ]),
                ],
                'mapped' => false,
            ])
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => User::class,
            'empty_data' => function (FormInterface $form) {
                return User::register(
                    $form->get('uuid')->getData(),
                    (string) $form->get('email')->getData(),
                    (string) $form->get('first_name')->getData(),
                    (string) $form->get('last_name')->getData(),
                    new EncodedPassword((string) $form->get('password')->getData()),
                    new \DateTime(),
                    null,
                    false,
                    false,
                    false,
                    null,
                    (string)$form->get('phone')->getData()
                );
            },
        ]);
    }
}
