<?php

declare(strict_types=1);

namespace App\Infrastructure\User\Factory\Form;

use App\Domain\User\Model\User;
use App\Infrastructure\Security\ValueObject\EncodedPassword;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ResetPasswordType
 *
 * @package App\Infrastructure\User\Factory\Form
 */
class ResetPasswordType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('password', RepeatedType::class,[
                'empty_data' => function (FormInterface $form) {
                    return new EncodedPassword((string) $form->get('password')->getData());
                },
                'constraints' => [
                    new NotBlank([
                        'message' => 'password.exception.status.not_blank',
                    ]),
                ],
                'first_options'  => [
                    'label' => false,
                ],
                'invalid_message' => 'The password fields must match.',
                'mapped' => false,
                'required' => true,
                'second_options' => [
                    'label' => false,
                ],
                'type' => PasswordType::class,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => User::class,
        ]);
    }
}
