<?php

declare(strict_types=1);

namespace App\Infrastructure\User\Factory;

use App\Domain\User\Factory\UserFactoryInterface;
use App\Domain\User\Model\User;
use App\Infrastructure\Common\Exception\Form\FormException;
use App\Infrastructure\Common\Factory\AbstractFactory;
use App\Infrastructure\User\Factory\Form\RegisterType;
use App\Infrastructure\User\Factory\Form\ResetPasswordType;
use App\Infrastructure\User\Factory\Form\UpdateType;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Class UserFactory
 *
 * @package App\Infrastructure\User\Factory
 */
class UserFactory extends AbstractFactory implements UserFactoryInterface
{
    /**
     * UserFactory constructor.
     *
     * @param FormFactoryInterface $factory
     */
    public function __construct(FormFactoryInterface $factory)
    {
        parent::__construct($factory);
    }

    /**
     * {@inheritDoc}
     *
     * @throws FormException
     */
    public function register(array $data): User
    {
        $this->formClass = RegisterType::class;

        return $this->execute(self::CREATE, $data);
    }

    /**
     * {@inheritDoc}
     *
     * @throws FormException
     */
    public function resetPassword(array $data, $object): User
    {
        $this->formClass = ResetPasswordType::class;

        return $this->execute(self::UPDATE, $data, $object);
    }

    /**
     * {@inheritDoc}
     *
     * @throws FormException
     */
    public function update(array $data, $object): User
    {
        $this->formClass = UpdateType::class;

        return $this->execute(self::UPDATE, $data, $object);
    }
}
