<?php

declare(strict_types=1);

namespace App\Infrastructure\User\Repository;

use App\Domain\User\Exception\UserNotFoundException;
use App\Domain\User\Model\User;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Domain\User\ValueObject\UserId;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class UserRepository
 *
 * @package App\Infrastructure\User\Repository
 */
class UserRepository extends ServiceEntityRepository implements UserRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * {@inheritDoc}
     */
    public function delete(User $user): void
    {
        $this->_em->remove($user);
        $this->_em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return parent::findAll();
    }

    /**
     * Find one by criteria.
     *
     * @param array $criteria
     * @param array|null $orderBy
     * @return User|object|null
     */
    public function findOneBy(array $criteria, array $orderBy = null): ?User
    {
        return parent::findOneBy($criteria, $orderBy);
    }

    /**
     * {@inheritDoc}
     */
    public function findOneByUsername(string $username): ?User
    {
        return $this->findOneBy(['auth.username' => $username]);
    }

    /**
     * {@inheritDoc}
     */
    public function findOneByUuid(int $userId): ?User
    {
        return $this->findOneBy(['uuid' => $userId]);
    }

    /**
     * {@inheritDoc}
     */
    public function getOneByCodeAndNotExpired(string $code): User
    {
        $user = $this->createQueryBuilder('u')
            ->where('u.reminder.code = :code')
            ->andWhere('u.reminder.expiresAt > :now')
            ->setParameters([
                'code' => $code,
                'now' => new DateTime(),
            ])
            ->getQuery()
            ->getOneOrNullResult();

        if (! $user) {
            throw new UserNotFoundException();
        }

        return $user;
    }

    /**
     * {@inheritDoc}
     */
    public function getOneByEmail(string $email): User
    {
        $user = $this->findOneBy(['email' => $email]);

        if (! $user) {
            throw new UserNotFoundException();
        }

        return $user;
    }

    /**
     * {@inheritDoc}
     */
    public function getOneByUuid(int $userId): User
    {
        $user = $this->findOneBy(['uuid' => $userId]);

        if (! $user) {
            throw new UserNotFoundException();
        }

        return $user;
    }

    /**
     * {@inheritDoc}
     */
    public function save(User $user): void
    {
        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * @inheritDoc
     */
    public function findOneByEmail(string $email): ?User
    {
        return $this->createQueryBuilder('u')
            ->where('u.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
