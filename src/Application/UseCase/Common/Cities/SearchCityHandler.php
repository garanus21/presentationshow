<?php

declare(strict_types=1);

namespace App\Application\UseCase\Common\Cities;

use App\Application\UseCase\Common\Cities\Request\SearchCity;
use App\Domain\Common\Repository\CitiesRepositoryInterface;

/**
 * Class SearchCityHandler
 *
 * @package App\Application\UseCase\Common\Cities
 */
class SearchCityHandler
{
    /**
     * Cities repository.
     *
     * @var CitiesRepositoryInterface
     */
    private $citiesRepository;

    /**
     * SearchCityHandler constructor.
     *
     * @param CitiesRepositoryInterface $citiesRepository
     */
    public function __construct(
        CitiesRepositoryInterface $citiesRepository
    ) {
        $this->citiesRepository = $citiesRepository;
    }

    /**
     * Function handle.
     *
     * @param SearchCity $request
     * @return array
     */
    public function handle(SearchCity $request): array
    {
        $cities = $this->citiesRepository->searchCity(
            $request->getQuery()
        );

        return $cities;
    }
}
