<?php

declare(strict_types=1);

namespace App\Application\UseCase\Common\Cities\Request;

/**
 * Class SearchCity
 *
 * @package App\Application\UseCase\Common\Cities\Request
 */
class SearchCity
{
    /**
     * Searched query.
     *
     * @var string
     */
    private $query;

    /**
     * SearchCity constructor.
     *
     * @param string $query
     */
    public function __construct(
        string $query
    ) {
        $this->query = $query;
    }

    /**
     * @return string
     */
    public function getQuery(): string
    {
        return $this->query;
    }
}
