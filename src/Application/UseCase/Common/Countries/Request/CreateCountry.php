<?php

declare(strict_types=1);

namespace App\Application\UseCase\Common\Countries\Request;

use App\Domain\Common\ValueObject\CountriesId;

/**
 * Class CreateCountry
 *
 * @package App\Application\UseCase\Common\Countries\Request
 */
class CreateCountry
{
    /**
     * @var CountriesId
     */
    private $countryId;

    /**
     * @var string
     */
    private $name;

    /**
     * CreateCountry constructor.
     *
     * @param CountriesId $countriesId
     * @param string $name
     */
    public function __construct(
        CountriesId $countriesId,
        string $name
    ) {
        $this->countryId = $countriesId;
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function toForm(): array
    {
        return [
            'uuid' => $this->countryId,
            'name' => $this->name,
        ];
    }
}
