<?php

declare(strict_types=1);

namespace App\Application\UseCase\Common\Countries;

use App\Application\UseCase\Common\Countries\Request\GetCountries;
use App\Domain\Common\Repository\CountriesRepositoryInterface;

/**
 * Class GetCountriesHandler
 *
 * @package App\Application\UseCase\Common\Countries
 */
class GetCountriesHandler
{
    /**
     * @var CountriesRepositoryInterface
     */
    private $countryRepository;

    /**
     * GetCountriesHandler constructor.
     *
     * @param CountriesRepositoryInterface $countryRepository
     */
    public function __construct(CountriesRepositoryInterface $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    /**
     * @param GetCountries $request
     * @return array
     */
    public function handle(GetCountries $request): array
    {
        return $this->countryRepository->findAll();
    }
}
