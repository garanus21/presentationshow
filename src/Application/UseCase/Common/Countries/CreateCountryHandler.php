<?php

declare(strict_types=1);

namespace App\Application\UseCase\Common\Countries;

use App\Application\UseCase\Common\Countries\Request\CreateCountry;
use App\Domain\Common\Factory\CountriesFactoryInterface;
use App\Domain\Common\Model\Countries;
use App\Domain\Common\Repository\CountriesRepositoryInterface;

/**
 * Class CreateCountryHandler
 *
 * @package App\Application\UseCase\Common\Countries
 */
class CreateCountryHandler
{
    /**
     * @var CountriesRepositoryInterface
     */
    private $repository;

    /**
     * @var CountriesFactoryInterface
     */
    private $factory;

    /**
     * CreateCountryHandler constructor.
     *
     * @param CountriesRepositoryInterface $repository
     * @param CountriesFactoryInterface $factory
     */
    public function __construct(
        CountriesRepositoryInterface $repository,
        CountriesFactoryInterface $factory
    ) {
        $this->repository = $repository;
        $this->factory = $factory;
    }

    /**
     * @param CreateCountry $request
     * @return Countries
     */
    public function handle(CreateCountry $request): Countries
    {
        $country = $this->factory->createCountry($request->toForm());
        $this->repository->save($country);

        return $country;
    }
}
