<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\SleepingCabinTypes;

use App\Application\UseCase\Dictionaries\SleepingCabinTypes\Request\GetCabinSleepingCabinType;
use App\Domain\Dictionaries\SleepingCabinTypes\Repository\SleepingCabinTypeRepositoryInterface;

/**
 * Class GetCabinSleepingCabinTypeHandler
 * @package App\Application\UseCase\Dictionaries\SleepingCabinTypes
 */
class GetCabinSleepingCabinTypeHandler
{
    /**
     * Sleeping Cabin Type Repository.
     *
     * @var SleepingCabinTypeRepositoryInterface
     */
    private $sleepingCabinTypeRepository;

    /**
     * GetCabinSleepingCabinTypeHandler constructor.
     * @param SleepingCabinTypeRepositoryInterface $sleepingCabinTypeRepository
     */
    public function __construct(
        SleepingCabinTypeRepositoryInterface $sleepingCabinTypeRepository
    ) {
        $this->sleepingCabinTypeRepository = $sleepingCabinTypeRepository;
    }

    /**
     * Function handle.
     *
     * @param GetCabinSleepingCabinType $request
     * @return array
     */
    public function handle(GetCabinSleepingCabinType $request): array
    {
        return $this->sleepingCabinTypeRepository->findByCabinId($request->getCabinId());
    }
}