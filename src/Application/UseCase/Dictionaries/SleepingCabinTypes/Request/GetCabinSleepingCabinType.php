<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\SleepingCabinTypes\Request;

/**
 * Class GetCabinSleepingCabinType
 * @package App\Application\UseCase\Dictionaries\SleepingCabinTypes\Request
 */
class GetCabinSleepingCabinType
{
    /**
     * Cabin id.
     *
     * @var int
     */
    private $cabinId;

    /**
     * GetCabinSleepingCabinType constructor.
     * @param int $cabinId
     */
    public function __construct(
        int $cabinId
    ) {
        $this->cabinId = $cabinId;
    }

    /**
     * @return int
     */
    public function getCabinId(): int
    {
        return $this->cabinId;
    }
}