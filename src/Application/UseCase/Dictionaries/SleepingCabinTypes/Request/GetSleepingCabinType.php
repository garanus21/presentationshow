<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\SleepingCabinTypes\Request;

/**
 * Class GetSleepingCabinType
 * @package App\Application\UseCase\Dictionaries\SleepingCabinTypes\Request
 */
class GetSleepingCabinType
{
    /**
     * Sleeping Cabin Type id.
     *
     * @var int
     */
    private $sleepingCabinTypeId;

    /**
     * GetSleepingCabinType constructor.
     * @param int $sleepingCabinTypeId
     */
    public function __construct(
        int $sleepingCabinTypeId
    ) {
        $this->sleepingCabinTypeId = $sleepingCabinTypeId;
    }

    /**
     * @return int
     */
    public function getSleepingCabinTypeId(): int
    {
        return $this->sleepingCabinTypeId;
    }
}