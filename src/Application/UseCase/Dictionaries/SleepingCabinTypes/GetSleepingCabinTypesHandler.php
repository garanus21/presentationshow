<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\SleepingCabinTypes;

use App\Application\UseCase\Dictionaries\SleepingCabinTypes\Request\GetSleepingCabinTypes;
use App\Domain\Dictionaries\SleepingCabinTypes\Repository\SleepingCabinTypeRepositoryInterface;

/**
 * Class GetSleepingCabinTypesHandler
 * @package App\Application\UseCase\Dictionaries\SleepingCabinTypes
 */
class GetSleepingCabinTypesHandler
{
    /**
     * Sleeping Cabin Type Repository.
     *
     * @var SleepingCabinTypeRepositoryInterface
     */
    private $sleepingCabinTypeRepository;

    /**
     * GetSleepingCabinTypesHandler constructor.
     * @param SleepingCabinTypeRepositoryInterface $sleepingCabinTypeRepository
     */
    public function __construct(
        SleepingCabinTypeRepositoryInterface $sleepingCabinTypeRepository
    ) {
        $this->sleepingCabinTypeRepository = $sleepingCabinTypeRepository;
    }

    /**
     * Function handle.
     *
     * @param GetSleepingCabinTypes $request
     * @return array
     */
    public function handle(GetSleepingCabinTypes $request): array
    {
        return $this->sleepingCabinTypeRepository->findAll();
    }
}