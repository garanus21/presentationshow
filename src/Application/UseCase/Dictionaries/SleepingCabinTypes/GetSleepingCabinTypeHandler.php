<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\SleepingCabinTypes;

use App\Application\UseCase\Dictionaries\SleepingCabinTypes\Request\GetSleepingCabinType;
use App\Domain\Dictionaries\SleepingCabinTypes\Model\SleepingCabinType;
use App\Domain\Dictionaries\SleepingCabinTypes\Repository\SleepingCabinTypeRepositoryInterface;

/**
 * Class GetSleepingCabinTypeHandler
 * @package App\Application\UseCase\Dictionaries\SleepingCabinTypes
 */
class GetSleepingCabinTypeHandler
{
    /**
     * Sleeping Cabin Types Repository.
     *
     * @var SleepingCabinTypeRepositoryInterface
     */
    private $sleepingCabinTypeRepository;

    /**
     * GetSleepingCabinTypeHandler constructor.
     * @param SleepingCabinTypeRepositoryInterface $sleepingCabinTypeRepository
     */
    public function __construct(
        SleepingCabinTypeRepositoryInterface $sleepingCabinTypeRepository
    ) {
        $this->sleepingCabinTypeRepository = $sleepingCabinTypeRepository;
    }

    /**
     * Function handle.
     *
     * @param GetSleepingCabinType $request
     * @return SleepingCabinType|null
     */
    public function handle(GetSleepingCabinType $request): ?SleepingCabinType
    {
        return $this->sleepingCabinTypeRepository->getOneById($request->getSleepingCabinTypeId());
    }
}