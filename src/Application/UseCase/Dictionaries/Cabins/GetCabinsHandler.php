<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\Cabins;

use App\Application\UseCase\Dictionaries\Cabins\Request\GetCabins;
use App\Domain\Dictionaries\Cabins\Repository\CabinRepositoryInterface;

/**
 * Class GetCabinsHandler
 * @package App\Application\UseCase\Dictionaries\Cabins
 */
class GetCabinsHandler
{
    /**
     * Cabin repository.
     *
     * @var CabinRepositoryInterface
     */
    private $cabinRepository;

    /**
     * GetCabinsHandler constructor.
     * @param CabinRepositoryInterface $cabinRepository
     */
    public function __construct(
        CabinRepositoryInterface $cabinRepository
    ) {
        $this->cabinRepository = $cabinRepository;
    }

    /**
     * Function handle.
     *
     * @param GetCabins $request
     * @return array
     */
    public function handle(GetCabins $request): array
    {
        return $this->cabinRepository->findAll();
    }
}