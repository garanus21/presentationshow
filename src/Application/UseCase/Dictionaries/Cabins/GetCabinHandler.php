<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\Cabins;

use App\Application\UseCase\Dictionaries\Cabins\Request\GetCabin;
use App\Domain\Dictionaries\Cabins\Model\Cabin;
use App\Domain\Dictionaries\Cabins\Repository\CabinRepositoryInterface;

/**
 * Class GetCabinHandler
 * @package App\Application\UseCase\Dictionaries\Cabins
 */
class GetCabinHandler
{
    /**
     * Cabin repository.
     *
     * @var CabinRepositoryInterface
     */
    private $cabinRepository;

    /**
     * GetCabinHandler constructor.
     * @param CabinRepositoryInterface $cabinRepository
     */
    public function __construct(
        CabinRepositoryInterface $cabinRepository
    ) {
        $this->cabinRepository = $cabinRepository;
    }

    /**
     * Function handle.
     *
     * @param GetCabin $request
     * @return Cabin|null
     */
    public function handle(GetCabin $request): ?Cabin
    {
        return $this->cabinRepository->getOneById($request->getCabinId());
    }
}