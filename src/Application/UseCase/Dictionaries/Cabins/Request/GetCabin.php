<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\Cabins\Request;

/**
 * Class GetCabin
 * @package App\Application\UseCase\Dictionaries\Cabins\Request
 */
class GetCabin
{
    /**
     * Cabin id.
     *
     * @var int
     */
    private $cabinId;

    /**
     * GetCabin constructor.
     * @param int $cabinId
     */
    public function __construct(
        int $cabinId
    ) {
        $this->cabinId = $cabinId;
    }

    /**
     * @return int
     */
    public function getCabinId(): int
    {
        return $this->cabinId;
    }
}