<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\SleepingCabinBeds;

use App\Application\UseCase\Dictionaries\SleepingCabinBeds\Request\GetSleepingCabinBeds;
use App\Domain\Dictionaries\SleepingCabinBeds\Repository\SleepingCabinBedRepositoryInterface;

/**
 * Class GetSleepingCabinTypesHandler
 * @package App\Application\UseCase\Dictionaries\SleepingCabinTypes
 */
class GetSleepingCabinBedsHandler
{
    /**
     * Sleeping cabin bed repository.
     *
     * @var SleepingCabinBedRepositoryInterface
     */
    private $sleepingCabinBedRepository;

    /**
     * GetSleepingCabinBedsHandler constructor.
     * @param SleepingCabinBedRepositoryInterface $sleepingCabinBedRepository
     */
    public function __construct(
        SleepingCabinBedRepositoryInterface $sleepingCabinBedRepository
    ) {
        $this->sleepingCabinBedRepository = $sleepingCabinBedRepository;
    }

    /**
     * Function handle.
     *
     * @param GetSleepingCabinBeds $request
     * @return array
     */
    public function handle(GetSleepingCabinBeds $request): array
    {
        return $this->sleepingCabinBedRepository->findAll();
    }
}