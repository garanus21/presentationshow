<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\SleepingCabinBeds\Request;

/**
 * Class GetCabinSleepingCabinBed
 * @package App\Application\UseCase\Dictionaries\SleepingCabinBeds\Request
 */
class GetCabinSleepingCabinBed
{
    /**
     * Cabin id.
     *
     * @var int
     */
    private $cabinId;

    /**
     * GetCabinSleepingCabinBed constructor.
     * @param int $cabinId
     */
    public function __construct(
        int $cabinId
    ) {
        $this->cabinId = $cabinId;
    }

    /**
     * @return int
     */
    public function getCabinId(): int
    {
        return $this->cabinId;
    }
}