<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\SleepingCabinBeds\Request;

/**
 * Class GetSleepingCabinBed
 * @package App\Application\UseCase\Dictionaries\SleepingCabinBeds\Request
 */
class GetSleepingCabinBed
{
    /**
     * Sleeping Cabin Bed id.
     *
     * @var int
     */
    private $sleepingCabinBedId;

    /**
     * GetSleepingCabinBed constructor.
     * @param int $sleepingCabinBedId
     */
    public function __construct(
        int $sleepingCabinBedId
    ) {
        $this->sleepingCabinBedId = $sleepingCabinBedId;
    }

    /**
     * @return int
     */
    public function getSleepingCabinBedId(): int
    {
        return $this->sleepingCabinBedId;
    }
}