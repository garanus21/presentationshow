<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\SleepingCabinBeds;

use App\Application\UseCase\Dictionaries\SleepingCabinBeds\Request\GetSleepingCabinBed;
use App\Domain\Dictionaries\SleepingCabinBeds\Model\SleepingCabinBed;
use App\Domain\Dictionaries\SleepingCabinBeds\Repository\SleepingCabinBedRepositoryInterface;

/**
 * Class GetSleepingCabinTypeHandler
 * @package App\Application\UseCase\Dictionaries\SleepingCabinTypes
 */
class GetSleepingCabinBedHandler
{
    /**
     * Sleeping cabin bed repository.
     *
     * @var SleepingCabinBedRepositoryInterface
     */
    private $sleepingCabinBedRepository;

    /**
     * GetSleepingCabinBedHandler constructor.
     * @param SleepingCabinBedRepositoryInterface $sleepingCabinBedRepository
     */
    public function __construct(
        SleepingCabinBedRepositoryInterface $sleepingCabinBedRepository
    ) {
        $this->sleepingCabinBedRepository = $sleepingCabinBedRepository;
    }

    /**
     * Function handle.
     *
     * @param GetSleepingCabinBed $request
     * @return SleepingCabinBed|null
     */
    public function handle(GetSleepingCabinBed $request): ?SleepingCabinBed
    {
        return $this->sleepingCabinBedRepository->getOneById($request->getSleepingCabinBedId());
    }
}