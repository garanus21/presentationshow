<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\SleepingCabinBeds;

use App\Application\UseCase\Dictionaries\SleepingCabinBeds\Request\GetCabinSleepingCabinBed;
use App\Domain\Dictionaries\SleepingCabinBeds\Repository\SleepingCabinBedRepositoryInterface;

/**
 * Class GetCabinSleepingCabinTypeHandler
 * @package App\Application\UseCase\Dictionaries\SleepingCabinTypes
 */
class GetCabinSleepingCabinBedHandler
{
    /**
     * Sleeping cabin bed repository.
     *
     * @var SleepingCabinBedRepositoryInterface
     */
    private $sleepingCabinBedRepository;

    /**
     * GetCabinSleepingCabinBedHandler constructor.
     * @param SleepingCabinBedRepositoryInterface $sleepingCabinBedRepository
     */
    public function __construct(
        SleepingCabinBedRepositoryInterface $sleepingCabinBedRepository
    ) {
        $this->sleepingCabinBedRepository = $sleepingCabinBedRepository;
    }

    /**
     * Function handle.
     *
     * @param GetCabinSleepingCabinBed $request
     * @return array
     */
    public function handle(GetCabinSleepingCabinBed $request): array
    {
        return $this->sleepingCabinBedRepository->findByCabinId($request->getCabinId());
    }
}