<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\PassengerSeats\Request;

/**
 * Class GetPassengerSeat
 * @package App\Application\UseCase\Dictionaries\PassengerSeats\Request
 */
class GetPassengerSeat
{
    /**
     * Passenger seat id.
     *
     * @var int
     */
    private $passengerSeatId;

    /**
     * GetPassengerSeat constructor.
     * @param int $passengerSeatId
     */
    public function __construct(
        int $passengerSeatId
    ) {
        $this->passengerSeatId = $passengerSeatId;
    }

    /**
     * @return int
     */
    public function getPassengerSeatId(): int
    {
        return $this->passengerSeatId;
    }
}