<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\PassengerSeats\Request;

/**
 * Class GetCabinSeats
 * @package App\Application\UseCase\Dictionaries\PassengerSeats\Request
 */
class GetCabinSeats
{
    /**
     * Cabin id.
     *
     * @var int
     */
    private $cabinId;

    /**
     * GetCabinSeats constructor.
     * @param int $cabinId
     */
    public function __construct(
        int $cabinId
    ) {
        $this->cabinId = $cabinId;
    }

    /**
     * @return int
     */
    public function getCabinId(): int
    {
        return $this->cabinId;
    }
}