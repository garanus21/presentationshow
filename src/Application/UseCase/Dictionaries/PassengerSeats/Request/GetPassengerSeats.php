<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\PassengerSeats\Request;

/**
 * Class GetPassengerSeats
 * @package App\Application\UseCase\Dictionaries\PassengerSeats\Request
 */
class GetPassengerSeats
{
}