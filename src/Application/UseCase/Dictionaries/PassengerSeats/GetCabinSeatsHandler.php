<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\PassengerSeats;

use App\Application\UseCase\Dictionaries\PassengerSeats\Request\GetCabinSeats;
use App\Domain\Dictionaries\PassengerSeats\Repository\PassengerSeatRepositoryInterface;

/**
 * Class GetCabinSeatsHandler
 * @package App\Application\UseCase\Dictionaries\PassengerSeats
 */
class GetCabinSeatsHandler
{
    /**
     * Passenger seats repository.
     *
     * @var PassengerSeatRepositoryInterface
     */
    private $passengerSeatRepository;

    /**
     * GetCabinSeatsHandler constructor.
     * @param PassengerSeatRepositoryInterface $passengerSeatRepository
     */
    public function __construct(
        PassengerSeatRepositoryInterface $passengerSeatRepository
    ) {
        $this->passengerSeatRepository = $passengerSeatRepository;
    }

    /**
     * Function handle.
     *
     * @param GetCabinSeats $request
     * @return array
     */
    public function handle(GetCabinSeats $request): array
    {
        return $this->passengerSeatRepository->findByCabinId($request->getCabinId());
    }
}