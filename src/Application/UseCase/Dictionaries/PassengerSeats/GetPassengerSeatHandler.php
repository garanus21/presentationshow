<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\PassengerSeats;

use App\Application\UseCase\Dictionaries\PassengerSeats\Request\GetPassengerSeat;
use App\Domain\Dictionaries\PassengerSeats\Model\PassengerSeat;
use App\Domain\Dictionaries\PassengerSeats\Repository\PassengerSeatRepositoryInterface;

/**
 * Class GetPassengerSeatHandler
 * @package App\Application\UseCase\Dictionaries\PassengerSeats
 */
class GetPassengerSeatHandler
{
    /**
     * Passenger seat repository.
     *
     * @var PassengerSeatRepositoryInterface
     */
    private $passengerSeatRepository;

    /**
     * GetPassengerSeatHandler constructor.
     * @param PassengerSeatRepositoryInterface $passengerSeatRepository
     */
    public function __construct(
        PassengerSeatRepositoryInterface $passengerSeatRepository
    ) {
        $this->passengerSeatRepository = $passengerSeatRepository;
    }

    /**
     * Function handle.
     *
     * @param GetPassengerSeat $request
     * @return PassengerSeat|null
     */
    public function handle(GetPassengerSeat $request): ?PassengerSeat
    {
        return $this->passengerSeatRepository->getOneById($request->getPassengerSeatId());
    }
}