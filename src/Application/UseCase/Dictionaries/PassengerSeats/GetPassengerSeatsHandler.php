<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\PassengerSeats;

use App\Application\UseCase\Dictionaries\PassengerSeats\Request\GetPassengerSeats;
use App\Domain\Dictionaries\PassengerSeats\Repository\PassengerSeatRepositoryInterface;

/**
 * Class GetPassengerSeatsHandler
 * @package App\Application\UseCase\Dictionaries\PassengerSeats
 */
class GetPassengerSeatsHandler
{
    /**
     * Passenger seat repository.
     *
     * @var PassengerSeatRepositoryInterface
     */
    private $passengerSeatRepository;

    /**
     * GetPassengerSeatsHandler constructor.
     * @param PassengerSeatRepositoryInterface $passengerSeatRepository
     */
    public function __construct(
        PassengerSeatRepositoryInterface $passengerSeatRepository
    ) {
        $this->passengerSeatRepository = $passengerSeatRepository;
    }

    /**
     * Function handle.
     *
     * @param GetPassengerSeats $request
     * @return array
     */
    public function handle(GetPassengerSeats $request): array
    {
        return $this->passengerSeatRepository->findAll();
    }
}