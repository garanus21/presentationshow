<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\AdditionalEquipment;

use App\Application\UseCase\Dictionaries\AdditionalEquipment\Request\GetAdditionalEquipment;
use App\Domain\Dictionaries\AdditionalEquipment\Model\AdditionalEquipment;
use App\Domain\Dictionaries\AdditionalEquipment\Repository\AdditionalEquipmentRepositoryInterface;

/**
 * Class GetAdditionalEquipmentHandler
 * @package App\Application\UseCase\Dictionaries\AdditionalEquipment
 */
class GetAdditionalEquipmentHandler
{
    /**
     * Additional equipment repository.
     *
     * @var AdditionalEquipmentRepositoryInterface
     */
    private $additionalEquipmentRepository;

    /**
     * GetAdditionalEquipmentHandler constructor.
     * @param AdditionalEquipmentRepositoryInterface $additionalEquipmentRepository
     */
    public function __construct(
        AdditionalEquipmentRepositoryInterface $additionalEquipmentRepository
    ) {
        $this->additionalEquipmentRepository = $additionalEquipmentRepository;
    }

    /**
     * Function handle.
     *
     * @param GetAdditionalEquipment $request
     * @return AdditionalEquipment|null
     */
    public function handle(GetAdditionalEquipment $request): ?AdditionalEquipment
    {
        return $this->additionalEquipmentRepository->getOneById($request->getAdditionalEquipmentId());
    }
}