<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\AdditionalEquipment;

use App\Application\UseCase\Dictionaries\AdditionalEquipment\Request\GetCabinAdditionalEquipment;
use App\Domain\Dictionaries\AdditionalEquipment\Repository\AdditionalEquipmentRepositoryInterface;

/**
 * Class GetCabinAdditionalEquipmentHandler
 * @package App\Application\UseCase\Dictionaries\AdditionalEquipment
 */
class GetCabinAdditionalEquipmentHandler
{
    /**
     * Additional equipment repository.
     *
     * @var AdditionalEquipmentRepositoryInterface
     */
    private $additionalEquipmentRepository;

    /**
     * GetCabinAdditionalEquipmentHandler constructor.
     * @param AdditionalEquipmentRepositoryInterface $additionalEquipmentRepository
     */
    public function __construct(
        AdditionalEquipmentRepositoryInterface $additionalEquipmentRepository
    ) {
        $this->additionalEquipmentRepository = $additionalEquipmentRepository;
    }

    /**
     * Function handle.
     *
     * @param GetCabinAdditionalEquipment $request
     * @return array
     */
    public function handle(GetCabinAdditionalEquipment $request): array
    {
        return $this->additionalEquipmentRepository->findByCabinId($request->getCabinId());
    }
}