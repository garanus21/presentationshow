<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\AdditionalEquipment\Request;

/**
 * Class GetAdditionalEquipment
 * @package App\Application\UseCase\Dictionaries\AdditionalEquipment\Request
 */
class GetAdditionalEquipment
{
    /**
     * Additional equipment id.
     *
     * @var int
     */
    private $additionalEquipmentId;

    /**
     * GetAdditionalEquipment constructor.
     * @param int $additionalEquipmentId
     */
    public function __construct(
        int $additionalEquipmentId
    ) {
        $this->additionalEquipmentId = $additionalEquipmentId;
    }

    /**
     * @return int
     */
    public function getAdditionalEquipmentId(): int
    {
        return $this->additionalEquipmentId;
    }
}