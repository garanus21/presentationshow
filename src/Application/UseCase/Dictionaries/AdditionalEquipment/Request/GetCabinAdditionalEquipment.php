<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\AdditionalEquipment\Request;

/**
 * Class GetCabinAdditionalEquipment
 * @package App\Application\UseCase\Dictionaries\AdditionalEquipment\Request
 */
class GetCabinAdditionalEquipment
{
    /**
     * Cabin id.
     *
     * @var int
     */
    private $cabinId;

    /**
     * GetCabinAdditionalEquipment constructor.
     * @param int $cabinId
     */
    public function __construct(
        int $cabinId
    ) {
        $this->cabinId = $cabinId;
    }

    /**
     * @return int
     */
    public function getCabinId(): int
    {
        return $this->cabinId;
    }
}