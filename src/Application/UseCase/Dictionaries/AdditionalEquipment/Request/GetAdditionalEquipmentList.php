<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\AdditionalEquipment\Request;

/**
 * Class GetAdditionalEquipmentList
 * @package App\Application\UseCase\Dictionaries\AdditionalEquipment\Request
 */
class GetAdditionalEquipmentList
{
}