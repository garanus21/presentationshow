<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\AdditionalEquipment;

use App\Application\UseCase\Dictionaries\AdditionalEquipment\Request\GetAdditionalEquipmentList;
use App\Domain\Dictionaries\AdditionalEquipment\Repository\AdditionalEquipmentRepositoryInterface;

/**
 * Class GetAdditionalEquipmentListHandler
 * @package App\Application\UseCase\Dictionaries\AdditionalEquipment
 */
class GetAdditionalEquipmentListHandler
{
    /**
     * Additional equipment repository.
     *
     * @var AdditionalEquipmentRepositoryInterface
     */
    private $additionalEquipmentRepository;

    /**
     * GetAdditionalEquipmentListHandler constructor.
     * @param AdditionalEquipmentRepositoryInterface $additionalEquipmentRepository
     */
    public function __construct(
        AdditionalEquipmentRepositoryInterface $additionalEquipmentRepository
    ) {
        $this->additionalEquipmentRepository = $additionalEquipmentRepository;
    }

    /**
     * Function handle.
     *
     * @param GetAdditionalEquipmentList $request
     * @return array
     */
    public function handle(GetAdditionalEquipmentList $request): array
    {
        return $this->additionalEquipmentRepository->findAll();
    }
}