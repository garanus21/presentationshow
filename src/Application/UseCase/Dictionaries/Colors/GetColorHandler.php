<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\Colors;

use App\Application\UseCase\Dictionaries\Colors\Request\GetColor;
use App\Domain\Dictionaries\Colors\Model\Color;
use App\Domain\Dictionaries\Colors\Repository\ColorRepositoryInterface;

/**
 * Class GetColorHandler
 * @package App\Application\UseCase\Dictionaries\Colors
 */
class GetColorHandler
{
    /**
     * Color repository.
     *
     * @var ColorRepositoryInterface
     */
    private $colorRepository;

    /**
     * GetColorHandler constructor.
     * @param ColorRepositoryInterface $colorRepository
     */
    public function __construct(
        ColorRepositoryInterface $colorRepository
    ) {
        $this->colorRepository = $colorRepository;
    }

    /**
     * Function handle.
     *
     * @param GetColor $request
     * @return Color|null
     */
    public function handle(GetColor $request): ?Color
    {
        return $this->colorRepository->getOneById($request->getColorId());
    }
}