<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\Colors;

use App\Application\UseCase\Dictionaries\Colors\Request\GetColors;
use App\Domain\Dictionaries\Colors\Repository\ColorRepositoryInterface;

/**
 * Class GetColorsHandler
 * @package App\Application\UseCase\Dictionaries\Colors
 */
class GetColorsHandler
{
    /**
     * Color repository.
     *
     * @var ColorRepositoryInterface
     */
    private $colorRepository;

    /**
     * GetColorsHandler constructor.
     * @param ColorRepositoryInterface $colorRepository
     */
    public function __construct(
        ColorRepositoryInterface $colorRepository
    ) {
        $this->colorRepository = $colorRepository;
    }

    /**
     * Function handle.
     *
     * @param GetColors $request
     * @return array
     */
    public function handle(GetColors $request): array
    {
        return $this->colorRepository->findAll();
    }
}