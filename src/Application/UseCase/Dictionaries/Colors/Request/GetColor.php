<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\Colors\Request;

/**
 * Class GetColor
 * @package App\Application\UseCase\Dictionaries\Colors\Request
 */
class GetColor
{
    /**
     * Color id.
     *
     * @var int
     */
    private $colorId;

    /**
     * GetColor constructor.
     * @param int $colorId
     */
    public function __construct(
        int $colorId
    ) {
        $this->colorId = $colorId;
    }

    /**
     * @return int
     */
    public function getColorId(): int
    {
        return $this->colorId;
    }
}