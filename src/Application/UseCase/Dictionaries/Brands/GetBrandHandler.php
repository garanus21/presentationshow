<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\Brands;

use App\Application\UseCase\Dictionaries\Brands\Request\GetBrand;
use App\Domain\Dictionaries\Brands\Model\Brand;
use App\Domain\Dictionaries\Brands\Repository\BrandRepositoryInterface;

/**
 * Class GetBrandHandler
 * @package App\Application\UseCase\Dictionaries\Brands
 */
class GetBrandHandler
{
    /**
     * Brand repository.
     *
     * @var BrandRepositoryInterface
     */
    private $brandRepository;

    /**
     * GetBrandHandler constructor.
     * @param BrandRepositoryInterface $brandRepository
     */
    public function __construct(
        BrandRepositoryInterface $brandRepository
    ) {
        $this->brandRepository = $brandRepository;
    }

    /**
     * @param GetBrand $request
     * @return Brand|null
     */
    public function handle(GetBrand $request): ?Brand
    {
        return $this->brandRepository->getOneById($request->getBrandId());
    }
}