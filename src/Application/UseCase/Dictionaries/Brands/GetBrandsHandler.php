<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\Brands;

use App\Application\UseCase\Dictionaries\Brands\Request\GetBrands;
use App\Domain\Dictionaries\Brands\Repository\BrandRepositoryInterface;

/**
 * Class GetBrandsHandler
 * @package App\Application\UseCase\Dictionaries\Brands
 */
class GetBrandsHandler
{
    /**
     * Brand repository.
     *
     * @var BrandRepositoryInterface
     */
    private $brandRepository;

    /**
     * GetBrandsHandler constructor.
     * @param BrandRepositoryInterface $brandRepository
     */
    public function __construct(
        BrandRepositoryInterface $brandRepository
    ) {
        $this->brandRepository = $brandRepository;
    }

    /**
     * Function handle.
     *
     * @param GetBrands $request
     * @return array
     */
    public function handle(GetBrands $request): array
    {
        return $this->brandRepository->findAll();
    }
}