<?php

declare(strict_types=1);

namespace App\Application\UseCase\Dictionaries\Brands\Request;

/**
 * Class GetBrand
 * @package App\Application\UseCase\Dictionaries\Brands\Request
 */
class GetBrand
{
    /**
     * Brand id.
     *
     * @var int
     */
    private $brandId;

    /**
     * GetBrand constructor.
     * @param int $brandId
     */
    public function __construct(
        int $brandId
    ) {
        $this->brandId = $brandId;
    }

    /**
     * @return int
     */
    public function getBrandId(): int
    {
        return $this->brandId;
    }
}