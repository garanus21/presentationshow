<?php

declare(strict_types=1);

namespace App\Application\UseCase\User;

use App\Application\UseCase\User\Request\UnlockUserAccount;
use App\Domain\Security\Exception\AccessDeniedException;
use App\Domain\User\Model\User;
use App\Domain\User\Repository\UserRepositoryInterface;

/**
 * Class UnlockUserAccountHandler
 *
 * @package App\Application\UseCase\User
 */
class UnlockUserAccountHandler
{
    private $userRepository;

    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    public function handle(UnlockUserAccount $request): User
    {
        $currentUser = $this->userRepository->getOneByUuid($request->getCurrentUserId()->__toInt());
        if (! $currentUser->isSuperAdmin()) {
            throw new AccessDeniedException();
        }
        $userToBlock = $this->userRepository->getOneByUuid($request->getUserId()->__toInt());
        if ($userToBlock->isSuperAdmin()) {
            throw new AccessDeniedException();
        }
        $userToBlock->setUnlock();
        $this->userRepository->save($userToBlock);

        return $userToBlock;
    }
}
