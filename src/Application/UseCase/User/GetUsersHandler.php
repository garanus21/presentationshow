<?php

declare(strict_types=1);

namespace App\Application\UseCase\User;

use App\Application\UseCase\User\Request\GetUsers;
use App\Domain\Security\Exception\AccessDeniedException;
use App\Domain\User\Exception\UserNotFoundException;
use App\Domain\User\Repository\UserRepositoryInterface;
use Exception;

/**
 * Class GetUsersHandler
 *
 * @package App\Application\UseCase\User
 */
class GetUsersHandler
{
    /**
     * @var UserRepositoryInterface
     */
    private $repository;

    /**
     * GetUsersHandler constructor.
     *
     * @param UserRepositoryInterface $repository
     */
    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Function handle.
     *
     * @param GetUsers $request
     * @return array
     * @throws Exception
     */
    public function handle(GetUsers $request): array
    {
        return $this->repository->findAll();
    }
}
