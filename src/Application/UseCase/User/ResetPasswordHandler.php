<?php

declare(strict_types=1);

namespace App\Application\UseCase\User;

use App\Application\UseCase\User\Request\ResetPassword;
use App\Domain\User\Factory\UserFactoryInterface;
use App\Domain\User\Repository\UserRepositoryInterface;

/**
 * Class ResetPasswordHandler
 *
 * @package App\Application\UseCase\User
 */
class ResetPasswordHandler
{
    /**
     * @var UserFactoryInterface
     */
    private $factory;

    /**
     * @var UserRepositoryInterface
     */
    private $repository;

    /**
     * ResetPasswordHandler constructor.
     *
     * @param UserFactoryInterface $factory
     * @param UserRepositoryInterface $repository
     */
    public function __construct(
        UserFactoryInterface $factory,
        UserRepositoryInterface $repository
    ) {
        $this->factory = $factory;
        $this->repository = $repository;
    }

    /**
     * @param ResetPassword $request
     */
    public function handle(ResetPassword $request)
    {
        $user = $this->repository->getOneByCodeAndNotExpired($request->getCode());
        $user = $this->factory->resetPassword($request->toForm(), $user);
        $user->resetPassword($request->getPassword());
        $this->repository->save($user);
    }
}
