<?php

declare(strict_types=1);

namespace App\Application\UseCase\User;

use App\Application\UseCase\User\Request\BlockUserAccount;
use App\Domain\Security\Exception\AccessDeniedException;
use App\Domain\User\Exception\UserNotFoundException;
use App\Domain\User\Repository\UserRepositoryInterface;
use Exception;

/**
 * Class BlockUserAccountHandler
 *
 * @package App\Application\UseCase\User
 */
class BlockUserAccountHandler
{
    /**
     * User repository.
     *
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * BlockUserAccountHandler constructor.
     *
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * Function handle.
     *
     * @param BlockUserAccount $request
     * @throws UserNotFoundException
     * @throws Exception
     */
    public function handle(BlockUserAccount $request)
    {
        $currentUser = $this->userRepository->getOneByUuid($request->getCurrentUserId()->__toInt());
        if (! $currentUser->isSuperAdmin()) {
            throw new AccessDeniedException();
        }
        $userToBlock = $this->userRepository->getOneByUuid($request->getUserId()->__toInt());
        if ($userToBlock->isSuperAdmin()) {
            throw new AccessDeniedException();
        }
        $userToBlock->setBlocked();
        $this->userRepository->save($userToBlock);
    }
}
