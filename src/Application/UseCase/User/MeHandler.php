<?php

declare(strict_types=1);

namespace App\Application\UseCase\User;

use App\Application\UseCase\User\Request\Me;
use App\Domain\User\Exception\UserNotFoundException;
use App\Domain\User\Model\User;
use App\Domain\User\Repository\UserRepositoryInterface;

/**
 * Class MeHandler
 *
 * @package App\Application\UseCase\User
 */
class MeHandler
{
    /**
     * @var UserRepositoryInterface
     */
    private $repository;

    /**
     * GetUserHandler constructor.
     *
     * @param UserRepositoryInterface $repository
     */
    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Me $request
     * @return User
     * @throws UserNotFoundException
     */
    public function handle(Me $request): User
    {
        $user = $this->repository->getOneByUuid($request->userId()->__toInt());

        return $user;
    }
}
