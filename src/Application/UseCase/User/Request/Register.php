<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\Request;

use App\Domain\User\ValueObject\UserId;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Register
 *
 * @package App\Application\UseCase\User\Request
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Register
{
    /**
     * @var int
     */
    private $currentUser;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var string
     *
     */
    private $email;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $plainPassword;

    /**
     * Phone number.
     *
     * @var string
     */
    private $phone;

    /**
     * Register constructor.
     *
     * @param int $currentUser
     * @param UserId $userId
     * @param string $email
     * @param string $firstName
     * @param string $lastName
     * @param string $plainPassword
     * @param string $phone
     */
    public function __construct(
        int $currentUser,
        UserId $userId,
        string $email,
        string $firstName,
        string $lastName,
        string $plainPassword,
        string $phone
    ) {
        $this->currentUser = $currentUser;
        $this->userId = $userId;
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->plainPassword = $plainPassword;
        $this->phone = $phone;
    }

    /**
     * @return array
     */
    public function toForm(): array
    {
        return [
            'uuid' => $this->userId,
            'email' => $this->email,
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'password' => $this->plainPassword,
            'phone' => $this->phone,
        ];
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return int
     */
    public function getCurrentUser(): int
    {
        return $this->currentUser;
    }
}
