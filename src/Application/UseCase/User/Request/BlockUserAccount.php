<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\Request;

use App\Domain\User\ValueObject\UserId;
use Exception;

/**
 * Class BlockUserAccount
 *
 * @package App\Application\UseCase\User\Request
 */
class BlockUserAccount
{
    /**
     * User uuid.
     *
     * @var int
     */
    private $userId;

    /**
     * Current user.
     *
     * @var int
     */
    private $currentUserId;

    /**
     * BlockUserAccount constructor.
     *
     * @param int $userId
     * @param int $currentUserId
     */
    public function __construct(
        int $userId,
        int $currentUserId
    ) {
        $this->userId = $userId;
        $this->currentUserId = $currentUserId;
    }

    /**
     * @return UserId
     * @throws Exception
     */
    public function getUserId(): UserId
    {
        return new UserId($this->userId);
    }

    /**
     * @return UserId
     * @throws Exception
     */
    public function getCurrentUserId(): UserId
    {
        return new UserId($this->currentUserId);
    }
}
