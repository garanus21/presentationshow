<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\Request;

/**
 * Class ActivateAccount
 *
 * @package App\Application\UseCase\User\Request
 */
class ActivateAccount
{
    /**
     * Activation hash.
     *
     * @var string
     */
    private $activationHash;

    /**
     * User e-mail.
     *
     * @var string
     */
    private $email;

    /**
     * ActivateAccount constructor.
     *
     * @param string $activationHash
     * @param string $email
     */
    public function __construct(
        string $activationHash,
        string $email
    ) {
        $this->activationHash = $activationHash;
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getActivationHash(): string
    {
        return $this->activationHash;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
}
