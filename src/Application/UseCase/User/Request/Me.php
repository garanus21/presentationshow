<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\Request;

use App\Domain\User\ValueObject\UserId;
use Exception;

/**
 * Class Me
 *
 * @package App\Application\UseCase\User\Request
 */
class Me
{
    /**
     * @var int
     */
    private $userId;

    /**
     * OwnDetails constructor.
     *
     * @param int $userId
     */
    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return UserId
     * @throws Exception
     */
    public function userId(): UserId
    {
        return new UserId($this->userId);
    }
}
