<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\Request;

/**
 * Class ForgotPassword
 *
 * @package App\Application\UseCase\User\Request
 */
class ForgotPassword
{
    /**
     * @var string
     */
    private $email;

    /**
     * ForgotPassword constructor.
     *
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function email(): string
    {
        return $this->email;
    }
}
