<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\Request;

use App\Domain\User\ValueObject\UserId;
use Exception;

/**
 * Class UnlockUserAccount
 *
 * @package App\Application\UseCase\User\Request
 */
class UnlockUserAccount
{
    /**
     * User uuid.
     *
     * @var int
     */
    private $userId;

    /**
     * Current user uuid.
     *
     * @var int
     */
    private $currentUserId;

    /**
     * UnlockUserAccount constructor.
     *
     * @param int $userId
     * @param int $currentUserId
     */
    public function __construct(
        int $userId,
        int $currentUserId
    ) {
        $this->userId = $userId;
        $this->currentUserId = $currentUserId;
    }

    /**
     * @return UserId
     * @throws Exception
     */
    public function getUserId(): UserId
    {
        return new UserId($this->userId);
    }

    /**
     * @return UserId
     * @throws Exception
     */
    public function getCurrentUserId(): UserId
    {
        return new UserId($this->currentUserId);
    }
}
