<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\Request;

use App\Domain\User\ValueObject\UserId;
use Exception;

/**
 * Class GetUsers
 *
 * @package App\Application\UseCase\User\Request
 */
class GetUsers
{
    /**
     * Current user.
     *
     * @var int
     */
    private $userId;

    /**
     * GetUsers constructor.
     *
     * @param int $userId
     */
    public function __construct(
        int $userId
    ) {
        $this->userId = $userId;
    }

    /**
     * @return UserId
     * @throws Exception
     */
    public function getUserId(): UserId
    {
        return new UserId($this->userId);
    }
}
