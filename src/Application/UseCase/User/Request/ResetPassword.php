<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\Request;

use App\Domain\Security\ValueObject\EncodedPasswordInterface;
use App\Domain\User\Model\User;
use App\Infrastructure\Security\ValueObject\EncodedPassword;

/**
 * Class ResetPassword
 *
 * @package App\Application\UseCase\User\Request
 */
class ResetPassword
{
    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $repeatedPassword;

    /**
     * @var string
     */
    private $code;


    /**
     * ResetPassword constructor.
     *
     * @param string $code
     * @param string $password
     * @param string $repeatedPassword
     */
    public function __construct(
        string $code,
        string $password,
        string $repeatedPassword
    ) {
        $this->code = $code;
        $this->password = $password;
        $this->repeatedPassword = $repeatedPassword;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    public function toForm(): array
    {
        return [
            'password' => [
                'first' => $this->password,
                'second' => $this->repeatedPassword,
            ],
        ];
    }

    public function getPassword(): EncodedPasswordInterface
    {
        return new EncodedPassword((string) $this->password);
    }
}
