<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\Request;

use App\Domain\User\ValueObject\UserId;

/**
 * Class Delete
 *
 * @package App\Application\UseCase\User\Request
 */
class Delete
{
    /**
     * User to delete.
     *
     * @var int
     */
    private $uuid;

    /**
     * Current user.
     *
     * @var int
     */
    private $userId;

    /**
     * Delete constructor.
     *
     * @param int $uuid
     * @param int $userId
     * @throws \Exception
     */
    public function __construct(
        int $uuid,
        int $userId
    ) {
        $this->uuid = new UserId($uuid);
        $this->userId = $userId;
    }

    /**
     * @return UserId
     */
    public function uuid(): UserId
    {
        return $this->uuid;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
}
