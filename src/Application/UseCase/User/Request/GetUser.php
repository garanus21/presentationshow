<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\Request;

use App\Domain\User\ValueObject\UserId;
use Exception;

/**
 * Class GetUser
 *
 * @package App\Application\UseCase\User\Request
 */
class GetUser
{
    /**
     * @var UserId
     */
    private $uuid;

    /**
     * Current user.
     *
     * @var int
     */
    private $userId;

    /**
     * GetUser constructor.
     *
     * @param int $uuid
     * @param int $userId
     * @throws Exception
     */
    public function __construct(
        int $uuid,
        int $userId
    ) {
        $this->uuid = new UserId($uuid);
        $this->userId = $userId;
    }

    /**
     * @return UserId
     */
    public function uuid(): UserId
    {
        return $this->uuid;
    }

    /**
     * @return UserId
     * @throws Exception
     */
    public function getUserId(): UserId
    {
        return new UserId($this->userId);
    }
}
