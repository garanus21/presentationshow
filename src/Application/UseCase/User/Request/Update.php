<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\Request;

use App\Domain\User\ValueObject\UserId;
use Exception;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Update
 *
 * @package App\Application\UseCase\User\Request
 * @Serializer\ExclusionPolicy("all")
 */
class Update
{
    /**
     * @var int
     */
    private $userId;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private  $lastName;

    /**
     * @var string
     */
    private $phone;

    /**
     * Update constructor.
     *
     * @param int $userId
     * @param string $firstName
     * @param string $lastName
     * @param string $phone
     */
    public function __construct(
        int $userId,
        string $firstName,
        string $lastName,
        string $phone
    ) {
        $this->userId = $userId;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->phone = $phone;
    }

    /**
     * @return array
     */
    public function toForm(): array
    {
        return [
            'uuid' => $this->userId,
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'phone' => $this->phone,
        ];
    }

    /**
     * @return UserId
     * @throws Exception
     */
    public function userId(): UserId
    {
        return new UserId($this->userId);
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }
}
