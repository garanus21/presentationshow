<?php

declare(strict_types=1);

namespace App\Application\UseCase\User;

use App\Application\UseCase\User\Request\Register;
use App\Domain\Security\Exception\AccessDeniedException;
use App\Domain\User\Factory\UserFactoryInterface;
use App\Domain\User\Model\User;
use App\Domain\User\Repository\UserRepositoryInterface;
use Exception;

/**
 * Class RegisterUserHandler
 *
 * @package App\Application\UseCase\User
 */
class RegisterUserHandler
{
    /**
     * User factory.
     *
     * @var UserFactoryInterface
     */
    private $factory;

    /**
     * User repository.
     *
     * @var UserRepositoryInterface
     */
    private $repository;

    /**
     * RegisterUserHandler constructor.
     *
     * @param UserFactoryInterface $factory
     * @param UserRepositoryInterface $repository
     */
    public function __construct(
        UserFactoryInterface $factory,
        UserRepositoryInterface $repository
    ) {
        $this->factory = $factory;
        $this->repository = $repository;
    }

    /**
     * Function handle.
     *
     * @param Register $request
     * @return User
     * @throws Exception
     */
    public function handle(Register $request): User
    {
        if (!$request->getCurrentUser()) {
            throw new AccessDeniedException();
        }
        // Get form to register user.
        $user = $this->factory->register($request->toForm());
        $this->repository->save($user);
        return $user;
    }
}
