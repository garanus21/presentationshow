<?php

declare(strict_types=1);

namespace App\Application\UseCase\User;

use App\Application\UseCase\User\Request\Update;
use App\Domain\Positions\Repository\PositionRepositoryInterface;
use App\Domain\Positions\ValueObject\PositionId;
use App\Domain\User\Exception\UserNotFoundException;
use App\Domain\User\Factory\UserFactoryInterface;
use App\Domain\User\Model\User;
use App\Domain\User\Repository\UserRepositoryInterface;

/**
 * Class UpdateHandler
 *
 * @package App\Application\UseCase\User
 */
class UpdateHandler
{
    /**
     * User factory.
     *
     * @var UserFactoryInterface
     */
    private $factory;

    /**
     * User repository.
     *
     * @var UserRepositoryInterface
     */
    private $repository;

    /**
     * UpdateHandler constructor.
     *
     * @param UserFactoryInterface $factory
     * @param UserRepositoryInterface $repository
     */
    public function __construct(
        UserFactoryInterface $factory,
        UserRepositoryInterface $repository
    ) {
        $this->factory = $factory;
        $this->repository = $repository;
    }

    /**
     * Function handle.
     *
     * @param Update $request
     * @return User
     * @throws UserNotFoundException
     * @throws \Exception
     */
    public function handle(Update $request): User
    {
        $user = $this->repository->getOneByUuid($request->userId()->__toInt());
        $user = $this->factory->update($request->toForm(), $user);
        $user->update(
            $request->getFirstName(),
            $request->getLastName(),
            $request->getPhone()
        );
        $this->repository->save($user);

        return $user;
    }
}
