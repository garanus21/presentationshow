<?php

declare(strict_types=1);

namespace App\Application\UseCase\User;

use App\Application\UseCase\User\Request\ActivateAccount;
use App\Domain\User\Exception\UserNotFoundException;
use App\Domain\User\Model\User;
use App\Domain\User\Repository\UserRepositoryInterface;

/**
 * Class ActivateAccountHandler
 *
 * @package App\Application\UseCase\User
 */
class ActivateAccountHandler
{
    /**
     * User repository.
     *
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * ActivateAccountHandler constructor.
     *
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * Function handle.
     *
     * @param ActivateAccount $request
     * @return User
     * @throws UserNotFoundException
     */
    public function handle(ActivateAccount $request): User
    {
        $user = $this->userRepository->getOneByEmail($request->getEmail());
        $userHash = $user->getActivationHash();
        $hash = $request->getActivationHash();
        if ($userHash == $hash) {
            $user->setIsActive(true);
            $user->setActivationHash(null);
            $this->userRepository->save($user);
            return $user;
        } else {
            throw new UserNotFoundException();
        }
    }
}
