<?php

declare(strict_types=1);

namespace App\Application\UseCase\User;

use App\Application\UseCase\User\Request\ForgotPassword;
use App\Domain\User\Exception\UserNotFoundException;
use App\Domain\User\Helper\UserMailerInterface;
use App\Domain\User\Repository\UserRepositoryInterface;

/**
 * Class ForgotPasswordHandler
 *
 * @package App\Application\UseCase\User
 */
class ForgotPasswordHandler
{
    /**
     * @var UserRepositoryInterface
     */
    private $repository;

    /**
     * @var UserMailerInterface
     */
    private $mailer;

    /**
     * ForgotPasswordHandler constructor.
     *
     * @param UserMailerInterface $mailer
     * @param UserRepositoryInterface $repository
     */
    public function __construct(
        UserMailerInterface $mailer,
        UserRepositoryInterface $repository
    ) {
        $this->mailer = $mailer;
        $this->repository = $repository;
    }

    /**
     * @param ForgotPassword $request
     * @throws UserNotFoundException
     */
    public function handle(ForgotPassword $request)
    {
        $user = $this->repository->getOneByEmail($request->email());
        $user->remind();
        $this->repository->save($user);
        $this->mailer->sendForgotPasswordEmail($user);
    }
}
