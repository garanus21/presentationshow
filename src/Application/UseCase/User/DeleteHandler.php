<?php

declare(strict_types=1);

namespace App\Application\UseCase\User;

use App\Application\UseCase\User\Request\Delete;
use App\Domain\Security\Exception\AccessDeniedException;
use App\Domain\User\Exception\UserNotFoundException;
use App\Domain\User\Repository\UserRepositoryInterface;
use Exception;

/**
 * Class DeleteHandler
 *
 * @package App\Application\UseCase\User
 */
class DeleteHandler
{
    /**
     * @var UserRepositoryInterface
     */
    private $repository;

    /**
     * GetUserHandler constructor.
     *
     * @param UserRepositoryInterface $repository
     */
    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Delete $request
     * @throws UserNotFoundException
     * @throws Exception
     */
    public function handle(Delete $request)
    {
        $user = $this->repository->getOneByUuid($request->uuid()->__toInt());
        $user->delete();
        $this->repository->save($user);
    }
}
