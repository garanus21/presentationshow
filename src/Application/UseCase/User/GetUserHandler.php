<?php

declare(strict_types=1);

namespace App\Application\UseCase\User;

use App\Application\UseCase\User\Request\GetUser;
use App\Domain\Security\Exception\AccessDeniedException;
use App\Domain\User\Exception\UserNotFoundException;
use App\Domain\User\Model\User;
use App\Domain\User\Repository\UserRepositoryInterface;
use Exception;

/**
 * Class GetUserHandler
 *
 * @package App\Application\UseCase\User
 */
class GetUserHandler
{
    /**
     * @var UserRepositoryInterface
     */
    private $repository;

    /**
     * GetUserHandler constructor.
     *
     * @param UserRepositoryInterface $repository
     */
    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param GetUser $request
     * @return User
     * @throws UserNotFoundException
     * @throws Exception
     */
    public function handle(GetUser $request): User
    {
        return $this->repository->getOneByUuid($request->uuid()->__toInt());
    }
}