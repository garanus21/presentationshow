<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders;

use App\Application\UseCase\Orders\Request\GetOrderAdditionalEquipments;
use App\Domain\Orders\Repository\OrderEquipmentRepositoryInterface;

/**
 * Class GetOrderAdditionalEquipmentsHandler
 * @package App\Application\UseCase\Orders
 */
class GetOrderAdditionalEquipmentsHandler
{
    /**
     * Order equipment.
     *
     * @var OrderEquipmentRepositoryInterface
     */
    private $orderEquipmentRepository;

    /**
     * GetOrderAdditionalEquipmentsHandler constructor.
     * @param OrderEquipmentRepositoryInterface $orderEquipmentRepository
     */
    public function __construct(
        OrderEquipmentRepositoryInterface $orderEquipmentRepository
    ) {
        $this->orderEquipmentRepository = $orderEquipmentRepository;
    }

    /**
     * Function handle.
     *
     * @param GetOrderAdditionalEquipments $request
     * @return array
     */
    public function handle(GetOrderAdditionalEquipments $request): array
    {
        return $this->orderEquipmentRepository->findByOrder($request->getOrderId());
    }
}