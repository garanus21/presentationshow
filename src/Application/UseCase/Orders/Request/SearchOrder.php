<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders\Request;

/**
 * Class SearchOrder
 * @package App\Application\UseCase\Orders\Request
 */
class SearchOrder
{
    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $orderDate;

    /**
     * @var string
     */
    private $equipmentName;

    /**
     * @var string
     */
    private $productType;

    /**
     * @var string
     */
    private $orderStatus;

    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var string
     */
    private $nip;

    /**
     * @var int|null
     */
    private $createdDateOrder;

    /**
     * @var int|null
     */
    private $clientNameOrder;

    private $orderNumber;
    private $orderCompletionTime;

    /**
     * SearchOrder constructor.
     * @param string $lastName
     * @param string $orderDate
     * @param string $equipmentName
     * @param string $productType
     * @param string $orderStatus
     * @param string $nip
     * @param int|null $createdDateOrder
     * @param int|null $clientNameOrder
     * @param string $orderNumber
     * @param string|null $orderCompletionTime
     * @param int $page
     * @param int $limit
     */
    public function __construct(
        string $lastName,
        string $orderDate,
        string $equipmentName,
        string $productType,
        string $orderStatus,
        string $nip,
        ?int $createdDateOrder,
        ?int $clientNameOrder,
        string $orderNumber,
        ?string $orderCompletionTime,
        int $page,
        int $limit
    ) {
        $this->lastName = $lastName;
        $this->orderDate = $orderDate;
        $this->equipmentName = $equipmentName;
        $this->productType = $productType;
        $this->orderStatus = $orderStatus;
        $this->page = $page;
        $this->limit = $limit;
        $this->nip = $nip;
        $this->createdDateOrder = $createdDateOrder;
        $this->clientNameOrder = $clientNameOrder;
        $this->orderNumber = $orderNumber;
        $this->orderCompletionTime = $orderCompletionTime;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getOrderDate(): string
    {
        return $this->orderDate;
    }

    /**
     * @return string
     */
    public function getEquipmentName(): string
    {
        return $this->equipmentName;
    }

    /**
     * @return string
     */
    public function getProductType(): string
    {
        return $this->productType;
    }

    /**
     * @return string
     */
    public function getOrderStatus(): string
    {
        return $this->orderStatus;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return string
     */
    public function getNip(): string
    {
        return $this->nip;
    }

    /**
     * @return int|null
     */
    public function getCreatedDateOrder(): ?int
    {
        return $this->createdDateOrder;
    }

    /**
     * @return int|null
     */
    public function getClientNameOrder(): ?int
    {
        return $this->clientNameOrder;
    }

    /**
     * @return string
     */
    public function getOrderNumber(): string
    {
        return $this->orderNumber;
    }

    /**
     * @return string|null
     */
    public function getOrderCompletionTime(): ?string
    {
        return $this->orderCompletionTime;
    }
}