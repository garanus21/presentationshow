<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders\Request;

/**
 * Class GetOrder
 * @package App\Application\UseCase\Orders\Request
 */
class GetOrder
{
    /**
     * Order id.
     *
     * @var int
     */
    private $orderId;

    /**
     * GetOrder constructor.
     * @param int $orderId
     */
    public function __construct(
        int $orderId
    ) {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }
}