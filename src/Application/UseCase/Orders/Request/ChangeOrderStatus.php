<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders\Request;

/**
 * Class ChangeOrderStatus
 * @package App\Application\UseCase\Orders\Request
 */
class ChangeOrderStatus
{
    /**
     * Current user.
     *
     * @var int
     */
    private $userId;

    /**
     * Order id.
     *
     * @var int
     */
    private $orderId;

    /**
     * Status.
     *
     * @var int
     */
    private $status;

    /**
     * ChangeOrderStatus constructor.
     * @param int $userId
     * @param int $orderId
     * @param int $status
     */
    public function __construct(
        int $userId,
        int $orderId,
        int $status
    ) {
        $this->userId = $userId;
        $this->orderId = $orderId;
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }
}