<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders\Request;

/**
 * Class GetOrdersPaginated
 * @package App\Application\UseCase\Orders\Request
 */
class GetOrdersPaginated
{
    /**
     * Page.
     *
     * @var int
     */
    private $page;

    /**
     * Limit.
     *
     * @var int
     */
    private $limit;

    /**
     * GetOrdersPaginated constructor.
     * @param int $page
     * @param int $limit
     */
    public function __construct(
        int $page,
        int $limit
    ) {
        $this->page = $page;
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }
}