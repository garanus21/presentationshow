<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders\Request;

/**
 * Class DeleteOrder
 * @package App\Application\UseCase\Orders\Request
 */
class DeleteOrder
{
    /**
     * Order id.
     *
     * @var int
     */
    private $orderId;

    /**
     * DeleteOrder constructor.
     * @param int $orderId
     */
    public function __construct(
        int $orderId
    ) {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }
}