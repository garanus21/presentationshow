<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders\Request;

/**
 * Class GetOrderAdditionalEquipments
 * @package App\Application\UseCase\Orders\Request
 */
class GetOrderAdditionalEquipments
{
    /**
     * Current user.
     *
     * @var int
     */
    private $userId;

    /**
     * Order id.
     *
     * @var int
     */
    private $orderId;

    /**
     * GetOrderAdditionalEquipments constructor.
     * @param int $userId
     * @param int $orderId
     */
    public function __construct(
        int $userId,
        int $orderId
    ) {
        $this->userId = $userId;
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }
}