<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders\Request;

/**
 * Class GetOrderAdditionalEquipment
 * @package App\Application\UseCase\Orders\Request
 */
class GetOrderAdditionalEquipment
{
    /**
     * Current user.
     *
     * @var int
     */
    private $userId;

    /**
     * Order equipment id.
     *
     * @var int
     */
    private $orderEquipmentId;

    /**
     * GetOrderAdditionalEquipment constructor.
     * @param int $userId
     * @param int $orderEquipmentId
     */
    public function __construct(
        int $userId,
        int $orderEquipmentId
    ) {
        $this->userId = $userId;
        $this->orderEquipmentId = $orderEquipmentId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getOrderEquipmentId(): int
    {
        return $this->orderEquipmentId;
    }
}