<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders\Request;

use App\Domain\Orders\ValueObject\OrderEquipmentId;

/**
 * Class AddOrderAdditionalEquipment
 * @package App\Application\UseCase\Orders\Request
 */
class AddOrderAdditionalEquipment
{
    /**
     * Current user.
     *
     * @var int
     */
    private $userId;

    /**
     * Order equipment id.
     *
     * @var OrderEquipmentId
     */
    private $orderEquipmentId;

    /**
     * Order id.
     *
     * @var int
     */
    private $orderId;

    /**
     * Additional equipment id.
     *
     * @var int
     */
    private $additionalEquipmentId;

    /**
     * Comment.
     *
     * @var string|null
     */
    private $comment;

    /**
     * Price.
     *
     * @var string|null
     */
    private $price;

    /**
     * AddOrderAdditionalEquipment constructor.
     * @param int $userId
     * @param OrderEquipmentId $orderEquipmentId
     * @param int $orderId
     * @param int $additionalEquipmentId
     * @param string|null $comment
     * @param string|null $price
     */
    public function __construct(
        int $userId,
        OrderEquipmentId $orderEquipmentId,
        int $orderId,
        int $additionalEquipmentId,
        ?string $comment,
        ?string $price
    ) {
        $this->userId = $userId;
        $this->orderEquipmentId = $orderEquipmentId;
        $this->orderId = $orderId;
        $this->additionalEquipmentId = $additionalEquipmentId;
        $this->comment = $comment;
        $this->price = $price;
    }

    /**
     * Function to From.
     *
     * @return array
     */
    public function toForm(): array
    {
        return [
            'uuid' => $this->orderEquipmentId,
            'order_id' => $this->orderId,
            'additional_equipment_id' => $this->additionalEquipmentId,
            'comment' => $this->comment,
            'price' => $this->price,
        ];
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @return int
     */
    public function getAdditionalEquipmentId(): int
    {
        return $this->additionalEquipmentId;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return string|null
     */
    public function getPrice(): ?string
    {
        return $this->price;
    }
}