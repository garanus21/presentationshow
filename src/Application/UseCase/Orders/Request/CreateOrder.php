<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders\Request;

use App\Domain\Orders\ValueObject\OrderId;

/**
 * Class CreateOrder
 * @package App\Application\UseCase\Orders\Request
 */
class CreateOrder
{
    /**
     * @var int
     */
    private $userId;

    /**
     * @var OrderId
     */
    private $orderId;

    /**
     * @var string
     */
    private $clientName;

    /**
     * @var string|null
     */
    private $clientAddress;

    /**
     * @var string|null
     */
    private $clientNip;

    /**
     * @var string|null
     */
    private $clientRegon;

    /**
     * @var int|null
     */
    private $cabin;

    /**
     * @var int|null
     */
    private $brand;

    /**
     * @var string|null
     */
    private $productionYear;

    /**
     * @var int|null
     */
    private $color;

    /**
     * @var string|null
     */
    private $colorNumber;

    /**
     * @var string|null
     */
    private $vin;

    /**
     * @var string|null
     */
    private $placeAssembly;

    /**
     * @var string|null
     */
    private $orderCompletionTime;

    /**
     * @var string|null
     */
    private $laminated;

    /**
     * @var string|null
     */
    private $outcropped;

    /**
     * @var string|null
     */
    private $upholstered;

    /**
     * @var string|null
     */
    private $varnishing;

    /**
     * @var string|null
     */
    private $readyToAssembly;

    /**
     * @var string|null
     */
    private $assembly;

    /**
     * @var string|null
     */
    private $totalPrice;

    /**
     * @var string|null
     */
    private $comment;

    private $passengerSeat;
    private $sleepCabinBed;
    private $sleepCabinType;
    private $manualOrderNumber;

    /**
     * CreateOrder constructor.
     * @param int $userId
     * @param OrderId $orderId
     * @param string $clientName
     * @param string|null $clientAddress
     * @param string|null $clientNip
     * @param string|null $clientRegon
     * @param int|null $cabin
     * @param int|null $brand
     * @param string|null $productionYear
     * @param int|null $color
     * @param string|null $colorNumber
     * @param string|null $vin
     * @param string|null $placeAssembly
     * @param string|null $orderCompletionTime
     * @param string|null $laminated
     * @param string|null $outcropped
     * @param string|null $upholstered
     * @param string|null $varnishing
     * @param string|null $readyToAssembly
     * @param string|null $assembly
     * @param string|null $totalPrice
     * @param string|null $comment
     * @param int|null $passengerSeat
     * @param int|null $sleepCabinBed
     * @param int|null $sleepCabinType
     * @param string|null $manualOrderNumber
     */
    public function __construct(
        int $userId,
        OrderId $orderId,
        string $clientName,
        ?string $clientAddress,
        ?string $clientNip,
        ?string $clientRegon,
        ?int $cabin,
        ?int $brand,
        ?string $productionYear,
        ?int $color,
        ?string $colorNumber,
        ?string $vin,
        ?string $placeAssembly,
        ?string $orderCompletionTime,
        ?string $laminated,
        ?string $outcropped,
        ?string $upholstered,
        ?string $varnishing,
        ?string $readyToAssembly,
        ?string $assembly,
        ?string $totalPrice,
        ?string $comment,
        ?int $passengerSeat,
        ?int $sleepCabinBed,
        ?int $sleepCabinType,
        ?string $manualOrderNumber
    ) {
        $this->userId = $userId;
        $this->orderId = $orderId;
        $this->clientName = $clientName;
        $this->clientAddress = $clientAddress;
        $this->clientNip = $clientNip;
        $this->clientRegon = $clientRegon;
        $this->cabin = $cabin;
        $this->brand = $brand;
        $this->productionYear = $productionYear;
        $this->color = $color;
        $this->colorNumber = $colorNumber;
        $this->vin = $vin;
        $this->placeAssembly = $placeAssembly;
        $this->orderCompletionTime = $orderCompletionTime;
        $this->laminated = $laminated;
        $this->outcropped = $outcropped;
        $this->upholstered = $upholstered;
        $this->varnishing = $varnishing;
        $this->readyToAssembly = $readyToAssembly;
        $this->assembly = $assembly;
        $this->totalPrice = $totalPrice;
        $this->comment = $comment;
        $this->passengerSeat = $passengerSeat;
        $this->sleepCabinBed = $sleepCabinBed;
        $this->sleepCabinType = $sleepCabinType;
        $this->manualOrderNumber = $manualOrderNumber;
    }

    /**
     * Function to form.
     *
     * @return array
     */
    public function toFrom(): array
    {
        return [
            'uuid' => $this->orderId,
            'client_name' => $this->clientName,
            'client_address' => $this->clientAddress,
            'client_nip' =>  $this->clientNip,
            'client_regon' =>  $this->clientRegon,
            'cabin_id' => $this->cabin,
            'brand_id' => $this->brand,
            'production_year' => $this->productionYear,
            'color_id' => $this->color,
            'color_number' => $this->colorNumber,
            'vin' => $this->vin,
            'place_assembly' => $this->placeAssembly,
            'order_completion_time' => $this->orderCompletionTime,
            'laminated' => $this->laminated,
            'outcropped' => $this->outcropped,
            'upholstered' => $this->upholstered,
            'varnishing' => $this->varnishing,
            'ready_to_assembly' => $this->readyToAssembly,
            'assembly' => $this->assembly,
            'total_price' => $this->totalPrice,
            'comment' => $this->comment,
            'passenger_seats_id' => $this->passengerSeat,
            'sleep_cabin_beds_id' => $this->sleepCabinBed,
            'sleep_cabin_types_id' => $this->sleepCabinType,
            'manual_order_number' => $this->manualOrderNumber
        ];
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return OrderId
     */
    public function getOrderId(): OrderId
    {
        return $this->orderId;
    }
}