<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders\Request;

/**
 * Class UpdateOrder
 * @package App\Application\UseCase\Orders\Request
 */
class UpdateOrder
{
    /**
     * @var int
     */
    private $userId;

    /**
     * @var int
     */
    private $orderId;

    /**
     * @var string
     */
    private $clientName;

    /**
     * @var string|null
     */
    private $clientAddress;

    /**
     * @var string|null
     */
    private $clientNip;

    /**
     * @var string|null
     */
    private $clientRegon;

    private $status;

    /**
     * @var string|null
     */
    private $productionYear;

    /**
     * @var string|null
     */
    private $colorNumber;

    /**
     * @var string|null
     */
    private $vin;

    /**
     * @var string|null
     */
    private $placeAssembly;

    /**
     * @var string|null
     */
    private $orderCompletionTime;

    /**
     * @var string|null
     */
    private $laminated;

    /**
     * @var string|null
     */
    private $outcropped;

    /**
     * @var string|null
     */
    private $upholstered;

    /**
     * @var string|null
     */
    private $varnishing;

    /**
     * @var string|null
     */
    private $readyToAssembly;

    /**
     * @var string|null
     */
    private $assembly;

    /**
     * @var string|null
     */
    private $totalPrice;

    /**
     * @var string|null
     */
    private $comment;

    private $passengerSeat;
    private $sleepCabinBed;
    private $sleepCabinType;
    private $manualOrderNumber;

    /**
     * UpdateOrder constructor.
     * @param int $userId
     * @param int $orderId
     * @param string $clientName
     * @param string|null $clientAddress
     * @param string|null $clientNip
     * @param string|null $clientRegon
     * @param int|null $status
     * @param string|null $productionYear
     * @param string|null $colorNumber
     * @param string|null $vin
     * @param string|null $placeAssembly
     * @param string|null $orderCompletionTime
     * @param string|null $laminated
     * @param string|null $outcropped
     * @param string|null $upholstered
     * @param string|null $varnishing
     * @param string|null $readyToAssembly
     * @param string|null $assembly
     * @param string|null $totalPrice
     * @param string|null $comment
     * @param int|null $passengerSeat
     * @param int|null $sleepCabinBed
     * @param int|null $sleepCabinType
     * @param string|null $manualOrderNumber
     */
    public function __construct(
        int $userId,
        int $orderId,
        string $clientName,
        ?string $clientAddress,
        ?string $clientNip,
        ?string $clientRegon,
        ?int $status,
        ?string $productionYear,
        ?string $colorNumber,
        ?string $vin,
        ?string $placeAssembly,
        ?string $orderCompletionTime,
        ?string $laminated,
        ?string $outcropped,
        ?string $upholstered,
        ?string $varnishing,
        ?string $readyToAssembly,
        ?string $assembly,
        ?string $totalPrice,
        ?string $comment,
        ?int $passengerSeat,
        ?int $sleepCabinBed,
        ?int $sleepCabinType,
        ?string $manualOrderNumber
    ) {
        $this->userId = $userId;
        $this->orderId = $orderId;
        $this->clientName = $clientName;
        $this->clientAddress = $clientAddress;
        $this->clientNip = $clientNip;
        $this->clientRegon = $clientRegon;
        $this->status = $status;
        $this->productionYear = $productionYear;
        $this->colorNumber = $colorNumber;
        $this->vin = $vin;
        $this->placeAssembly = $placeAssembly;
        $this->orderCompletionTime = $orderCompletionTime;
        $this->laminated = $laminated;
        $this->outcropped = $outcropped;
        $this->upholstered = $upholstered;
        $this->varnishing = $varnishing;
        $this->readyToAssembly = $readyToAssembly;
        $this->assembly = $assembly;
        $this->totalPrice = $totalPrice;
        $this->comment = $comment;
        $this->passengerSeat = $passengerSeat;
        $this->sleepCabinBed = $sleepCabinBed;
        $this->sleepCabinType = $sleepCabinType;
        $this->manualOrderNumber = $manualOrderNumber;
    }

    /**
     * Function to form.
     *
     * @return array
     */
    public function toForm(): array
    {
        return [
            'uuid' => $this->orderId,
            'client_name' => $this->clientName,
            'client_address' => $this->clientAddress,
            'client_nip' =>  $this->clientNip,
            'client_regon' =>  $this->clientRegon,
            'status' => $this->status,
            'production_year' => $this->productionYear,
            'color_number' => $this->colorNumber,
            'vin' => $this->vin,
            'place_assembly' => $this->placeAssembly,
            'order_completion_time' => $this->orderCompletionTime,
            'laminated' => $this->laminated,
            'outcropped' => $this->outcropped,
            'upholstered' => $this->upholstered,
            'varnishing' => $this->varnishing,
            'ready_to_assembly' => $this->readyToAssembly,
            'assembly' => $this->assembly,
            'total_price' => $this->totalPrice,
            'comment' => $this->comment,
            'passenger_seats_id' => $this->passengerSeat,
            'sleep_cabin_beds_id' => $this->sleepCabinBed,
            'sleep_cabin_types_id' => $this->sleepCabinType,
            'manual_order_number' => $this->manualOrderNumber
        ];
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @return string
     */
    public function getClientName(): string
    {
        return $this->clientName;
    }

    /**
     * @return string|null
     */
    public function getClientAddress(): ?string
    {
        return $this->clientAddress;
    }

    /**
     * @return string|null
     */
    public function getClientNip(): ?string
    {
        return $this->clientNip;
    }

    /**
     * @return string|null
     */
    public function getClientRegon(): ?string
    {
        return $this->clientRegon;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @return string|null
     */
    public function getProductionYear(): ?string
    {
        return $this->productionYear;
    }

    /**
     * @return string|null
     */
    public function getColorNumber(): ?string
    {
        return $this->colorNumber;
    }

    /**
     * @return string|null
     */
    public function getVin(): ?string
    {
        return $this->vin;
    }

    /**
     * @return string|null
     */
    public function getPlaceAssembly(): ?string
    {
        return $this->placeAssembly;
    }

    /**
     * @return string|null
     */
    public function getOrderCompletionTime(): ?string
    {
        return $this->orderCompletionTime;
    }

    /**
     * @return string|null
     */
    public function getLaminated(): ?string
    {
        return $this->laminated;
    }

    /**
     * @return string|null
     */
    public function getOutcropped(): ?string
    {
        return $this->outcropped;
    }

    /**
     * @return string|null
     */
    public function getUpholstered(): ?string
    {
        return $this->upholstered;
    }

    /**
     * @return string|null
     */
    public function getVarnishing(): ?string
    {
        return $this->varnishing;
    }

    /**
     * @return string|null
     */
    public function getReadyToAssembly(): ?string
    {
        return $this->readyToAssembly;
    }

    /**
     * @return string|null
     */
    public function getAssembly(): ?string
    {
        return $this->assembly;
    }

    /**
     * @return string|null
     */
    public function getTotalPrice(): ?string
    {
        return $this->totalPrice;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return int|null
     */
    public function getPassengerSeat(): ?int
    {
        return $this->passengerSeat;
    }

    /**
     * @return int|null
     */
    public function getSleepCabinBed(): ?int
    {
        return $this->sleepCabinBed;
    }

    /**
     * @return int|null
     */
    public function getSleepCabinType(): ?int
    {
        return $this->sleepCabinType;
    }

    /**
     * @return string|null
     */
    public function getManualOrderNumber(): ?string
    {
        return $this->manualOrderNumber;
    }
}