<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders;

use App\Application\UseCase\Orders\Request\GetOrderAdditionalEquipment;
use App\Domain\Orders\Model\OrderEquipment;
use App\Domain\Orders\Repository\OrderEquipmentRepositoryInterface;

/**
 * Class GetOrderAdditionalEquipmentHandler
 * @package App\Application\UseCase\Orders
 */
class GetOrderAdditionalEquipmentHandler
{
    /**
     * Order equipment.
     *
     * @var OrderEquipmentRepositoryInterface
     */
    private $orderEquipmentRepository;

    /**
     * GetOrderAdditionalEquipmentHandler constructor.
     * @param OrderEquipmentRepositoryInterface $orderEquipmentRepository
     */
    public function __construct(
        OrderEquipmentRepositoryInterface $orderEquipmentRepository
    ) {
        $this->orderEquipmentRepository = $orderEquipmentRepository;
    }

    /**
     * Function handle.
     *
     * @param GetOrderAdditionalEquipment $request
     * @return OrderEquipment|null
     */
    public function handle(GetOrderAdditionalEquipment $request): ?OrderEquipment
    {
        return $this->orderEquipmentRepository->getOneById($request->getOrderEquipmentId());
    }
}