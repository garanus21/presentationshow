<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders;

use App\Application\UseCase\Orders\Request\CreateOrder;
use App\Domain\Orders\Factory\OrderFactoryInterface;
use App\Domain\Orders\Model\Order;
use App\Domain\Orders\Repository\OrderRepositoryInterface;
use App\Domain\Security\Exception\AccessDeniedException;
use App\Domain\User\Repository\UserRepositoryInterface;

/**
 * Class CreateOrderHandler
 * @package App\Application\UseCase\Orders
 */
class CreateOrderHandler
{
    /**
     * User repository.
     *
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * Order repository.
     *
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * Order factory.
     *
     * @var OrderFactoryInterface
     */
    private $orderFactory;

    /**
     * CreateOrderHandler constructor.
     * @param UserRepositoryInterface $userRepository
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderFactoryInterface $orderFactory
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        OrderRepositoryInterface $orderRepository,
        OrderFactoryInterface $orderFactory
    ) {
        $this->userRepository = $userRepository;
        $this->orderRepository = $orderRepository;
        $this->orderFactory = $orderFactory;
    }

    /**
     * Function handle.
     *
     * @param CreateOrder $request
     * @return Order
     * @throws \App\Domain\User\Exception\UserNotFoundException
     */
    public function handle(CreateOrder $request): Order
    {
        $order = $this->orderFactory->create($request->toFrom());
        $order->setStatus(0);
        $this->orderRepository->save($order);
        $year = date("Y");;
        $month = date("m");;
        $orderNumber = $year.'/'.$month.'/'.$order->uuid();
        $order->setOrderNumber($orderNumber);
        $this->orderRepository->save($order);

        return $order;
    }
}