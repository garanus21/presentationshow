<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders;

use App\Application\UseCase\Orders\Request\GetOrdersPaginated;
use App\Domain\Orders\Repository\OrderRepositoryInterface;
use App\Infrastructure\Common\Pagination\PagerTrait;

/**
 * Class GetOrdersPaginatedHandler
 * @package App\Application\UseCase\Orders
 */
class GetOrdersPaginatedHandler
{
    use PagerTrait;

    const PER_PAGE = 50;

    /**
     * Order repository.
     *
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * GetOrdersPaginatedHandler constructor.
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository
    ) {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Function handle.
     *
     * @param GetOrdersPaginated $request
     * @return array
     */
    public function handle(GetOrdersPaginated $request): array
    {
        $params = [];
        $count = $this->orderRepository->findPaginatedCountBy($params);

        return [
            'data' => $this->paginate(
                $params,
                $request->getPage(),
                $request->getLimit()
            ),
            'total' => $count,
        ];
    }

    /**
     * @param array $params
     * @param int $page
     * @param int $limit
     * @return array
     */
    private function paginate(array $params, int $page, int $limit = self::PER_PAGE)
    {
        $page = $this->page($page);
        $limit = $this->limit($limit);
        $offset = $this->offset($page, $limit);

        return $this->orderRepository->findPaginatedOrders(
            $params,
            $limit,
            $offset);
    }
}