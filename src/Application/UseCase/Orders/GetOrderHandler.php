<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders;

use App\Application\UseCase\Orders\Request\GetOrder;
use App\Domain\Orders\Model\Order;
use App\Domain\Orders\Repository\OrderRepositoryInterface;

/**
 * Class GetOrderHandler
 * @package App\Application\UseCase\Orders
 */
class GetOrderHandler
{
    /**
     * Order repository.
     *
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * GetOrderHandler constructor.
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository
    ) {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Function handle.
     *
     * @param GetOrder $request
     * @return Order|null
     */
    public function handle(GetOrder $request): ?Order
    {
        return $this->orderRepository->getOneById($request->getOrderId());
    }
}