<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders;

use App\Application\UseCase\Orders\Request\DeleteOrderAdditionalEquipment;
use App\Domain\Orders\Repository\OrderEquipmentRepositoryInterface;

/**
 * Class DeleteOrderAdditionalEquipmentHandler
 * @package App\Application\UseCase\Orders
 */
class DeleteOrderAdditionalEquipmentHandler
{
    /**
     * Order equipment repository.
     *
     * @var OrderEquipmentRepositoryInterface
     */
    private $orderEquipmentRepository;

    /**
     * DeleteOrderAdditionalEquipmentHandler constructor.
     * @param OrderEquipmentRepositoryInterface $orderEquipmentRepository
     */
    public function __construct(
        OrderEquipmentRepositoryInterface $orderEquipmentRepository
    ) {
        $this->orderEquipmentRepository = $orderEquipmentRepository;
    }

    /**
     * Function handle.
     *
     * @param DeleteOrderAdditionalEquipment $request
     */
    public function handle(DeleteOrderAdditionalEquipment $request)
    {
        $orderEquipment = $this->orderEquipmentRepository->getOneById($request->getOrderEquipmentId());
        $this->orderEquipmentRepository->delete($orderEquipment);
    }
}