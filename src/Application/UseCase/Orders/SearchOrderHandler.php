<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders;

use App\Application\UseCase\Orders\Request\SearchOrder;
use App\Domain\Orders\Repository\OrderRepositoryInterface;
use App\Infrastructure\Common\Pagination\PagerTrait;

/**
 * Class SearchOrderHandler
 * @package App\Application\UseCase\Orders
 */
class SearchOrderHandler
{
    use PagerTrait;

    const PER_PAGE = 500;

    private $orderRepository;

    public function __construct(
        OrderRepositoryInterface $orderRepository
    ) {
        $this->orderRepository = $orderRepository;
    }

    public function handle(SearchOrder $request): array
    {
        $lastName = $request->getLastName();
        $orderDate = $request->getOrderDate();
        $equipmentName = $request->getEquipmentName();
        $productType = $request->getProductType();
        $orderStatus = $request->getOrderStatus();
        $page = $request->getPage();
        $limit = $request->getLimit();
        $nip = $request->getNip();
        $orderByDate = $request->getCreatedDateOrder();
        $orderByClientName = $request->getClientNameOrder();
        $orderNumber = $request->getOrderNumber();
        $orderCompletionTime = $request->getOrderCompletionTime();

        $count = $this->orderRepository->searchPaginatedCountByQuery(
            $lastName,
            $orderDate,
            $equipmentName,
            $productType,
            $orderStatus,
            $nip,
            $orderNumber,
            $orderCompletionTime,
            $orderByDate,
            $orderByClientName
        );
        return [
            'data' => $this->paginate(
                $lastName,
                $orderDate,
                $equipmentName,
                $productType,
                $orderStatus,
                $nip,
                $orderNumber,
                $orderCompletionTime,
                $orderByDate,
                $orderByClientName,
                $page,
                $limit
            ),
            'total' => $count,
        ];
    }

    private function paginate(
        string $lastName,
        string $orderDate,
        string $equipmentName,
        string $productType,
        string $orderStatus,
        string $nip,
        string $orderNumber,
        string $orderCompletionTime,
        int $orderByDate,
        int $orderByClientName,
        int $page,
        int $limit = self::PER_PAGE
    ) {
        $page = $this->page($page);
        $limit = $this->limit($limit);
        $offset = $this->offset($page, $limit);

        return $this->orderRepository->searchPaginatedByQueries(
            $lastName,
            $orderDate,
            $equipmentName,
            $productType,
            $orderStatus,
            $nip,
            $orderNumber,
            $orderCompletionTime,
            $orderByDate,
            $orderByClientName,
            $limit,
            $offset
        );
    }
}