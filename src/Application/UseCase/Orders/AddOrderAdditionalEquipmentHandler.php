<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders;

use App\Application\UseCase\Orders\Request\AddOrderAdditionalEquipment;
use App\Domain\Common\Exception\AlreadyExistException;
use App\Domain\Orders\Factory\OrderEquipmentFactoryInterface;
use App\Domain\Orders\Model\OrderEquipment;
use App\Domain\Orders\Repository\OrderEquipmentRepositoryInterface;

/**
 * Class AddOrderAdditionalEquipmentHandler
 * @package App\Application\UseCase\Orders
 */
class AddOrderAdditionalEquipmentHandler
{
    /**
     * Order equipment repository.
     *
     * @var OrderEquipmentRepositoryInterface
     */
    private $orderEquipmentRepository;

    /**
     * Order equipment factory.
     *
     * @var OrderEquipmentFactoryInterface
     */
    private $orderEquipmentFactory;

    /**
     * AddOrderAdditionalEquipmentHandler constructor.
     * @param OrderEquipmentRepositoryInterface $orderEquipmentRepository
     * @param OrderEquipmentFactoryInterface $orderEquipmentFactory
     */
    public function __construct(
        OrderEquipmentRepositoryInterface $orderEquipmentRepository,
        OrderEquipmentFactoryInterface $orderEquipmentFactory
    ) {
        $this->orderEquipmentRepository = $orderEquipmentRepository;
        $this->orderEquipmentFactory = $orderEquipmentFactory;
    }

    /**
     * Function handle.
     *
     * @param AddOrderAdditionalEquipment $request
     * @return OrderEquipment
     * @throws AlreadyExistException
     */
    public function handle(AddOrderAdditionalEquipment $request): OrderEquipment
    {
        $alreadyExist = $this->orderEquipmentRepository->findByOrderAndEquipment($request->getOrderId(), $request->getAdditionalEquipmentId());
        if ($alreadyExist) {
            throw new AlreadyExistException();
        }
        $orderEquipment = $this->orderEquipmentFactory->create($request->toForm());
        $this->orderEquipmentRepository->save($orderEquipment);

        return $orderEquipment;
    }
}