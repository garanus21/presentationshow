<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders;

use App\Application\UseCase\Orders\Request\DeleteOrder;
use App\Domain\Orders\Repository\OrderRepositoryInterface;

/**
 * Class DeleteOrderHandler
 * @package App\Application\UseCase\Orders
 */
class DeleteOrderHandler
{
    /**
     * Order repository.
     *
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * DeleteOrderHandler constructor.
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository
    ) {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Function handle.
     *
     * @param DeleteOrder $request
     * @return \App\Domain\Orders\Model\Order|null
     * @throws \Exception
     */
    public function handle(DeleteOrder $request)
    {
        $order = $this->orderRepository->getOneById($request->getOrderId());
        $order->setDeletedAt(new \DateTime());
        $this->orderRepository->save($order);

        return $order;
    }
}