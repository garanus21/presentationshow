<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders;

use App\Application\UseCase\Orders\Request\GetOrders;
use App\Domain\Orders\Repository\OrderRepositoryInterface;

/**
 * Class GetOrdersHandler
 * @package App\Application\UseCase\Orders
 */
class GetOrdersHandler
{
    /**
     * Order repository.
     *
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * GetOrdersHandler constructor.
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository
    ) {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Function handle.
     *
     * @param GetOrders $request
     * @return array
     */
    public function handle(GetOrders $request): array
    {
        return $this->orderRepository->findAll();
    }

}