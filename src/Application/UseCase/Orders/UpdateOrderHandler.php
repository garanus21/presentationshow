<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders;

use App\Application\UseCase\Orders\Request\UpdateOrder;
use App\Domain\Dictionaries\PassengerSeats\Repository\PassengerSeatRepositoryInterface;
use App\Domain\Dictionaries\SleepingCabinBeds\Repository\SleepingCabinBedRepositoryInterface;
use App\Domain\Dictionaries\SleepingCabinTypes\Repository\SleepingCabinTypeRepositoryInterface;
use App\Domain\Orders\Factory\OrderFactoryInterface;
use App\Domain\Orders\Model\Order;
use App\Domain\Orders\Repository\OrderRepositoryInterface;
use App\Domain\User\Repository\UserRepositoryInterface;

/**
 * Class UpdateOrderHandler
 * @package App\Application\UseCase\Orders
 */
class UpdateOrderHandler
{
    /**
     * User repository.
     *
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * Order repository.
     *
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * Order factory.
     *
     * @var OrderFactoryInterface
     */
    private $orderFactory;

    private $passengerSeatRepository;
    private $sleepingCabinBedRepository;
    private $sleepingCabinTypeRepository;

    /**
     * UpdateOrderHandler constructor.
     * @param UserRepositoryInterface $userRepository
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderFactoryInterface $orderFactory
     * @param PassengerSeatRepositoryInterface $passengerSeatRepository
     * @param SleepingCabinBedRepositoryInterface $sleepingCabinBedRepository
     * @param SleepingCabinTypeRepositoryInterface $sleepingCabinTypeRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        OrderRepositoryInterface $orderRepository,
        OrderFactoryInterface $orderFactory,
        PassengerSeatRepositoryInterface $passengerSeatRepository,
        SleepingCabinBedRepositoryInterface $sleepingCabinBedRepository,
        SleepingCabinTypeRepositoryInterface $sleepingCabinTypeRepository
    ) {
        $this->userRepository = $userRepository;
        $this->orderRepository = $orderRepository;
        $this->orderFactory = $orderFactory;
        $this->passengerSeatRepository = $passengerSeatRepository;
        $this->sleepingCabinBedRepository = $sleepingCabinBedRepository;
        $this->sleepingCabinTypeRepository = $sleepingCabinTypeRepository;
    }

    /**
     * Function handle.
     *
     * @param UpdateOrder $request
     * @return Order
     * @throws \Exception
     */
    public function handle(UpdateOrder $request): Order
    {
        $order = $this->orderRepository->getOneById($request->getOrderId());
        $this->orderFactory->update($request->toForm(),$order);
        if ($request->getPassengerSeat()) {
            $passengerSeat = $this->passengerSeatRepository->getOneById($request->getPassengerSeat());
        } else {
            $passengerSeat = null;
        }
        if ($request->getSleepCabinBed()) {
            $sleepingCabinBed = $this->sleepingCabinBedRepository->getOneById($request->getSleepCabinBed());
        } else {
            $sleepingCabinBed = null;
        }
        if ($request->getSleepCabinType()) {
            $sleepingCabinType = $this->sleepingCabinTypeRepository->getOneById($request->getSleepCabinType());
        } else {
            $sleepingCabinType = null;
        }

        $order->update(
            $request->getClientName(),
            $request->getClientAddress(),
            $request->getClientNip(),
            $request->getClientRegon(),
            $request->getStatus(),
            $request->getProductionYear(),
            $request->getColorNumber(),
            $request->getVin(),
            $request->getPlaceAssembly(),
            ($request->getOrderCompletionTime() == '') ? null : new \DateTime($request->getOrderCompletionTime()),
            ($request->getLaminated() == '') ? null : new \DateTime($request->getLaminated()),
            ($request->getOutcropped() == '') ? null : new \DateTime($request->getOutcropped()),
            ($request->getUpholstered() == '') ? null : new \DateTime($request->getUpholstered()),
            ($request->getVarnishing() == '') ? null : new \DateTime($request->getVarnishing()),
            ($request->getReadyToAssembly() == '') ? null : new \DateTime($request->getReadyToAssembly()),
            ($request->getAssembly() == '') ? null : new \DateTime($request->getAssembly()),
            ($request->getTotalPrice() == '') ? null : $request->getTotalPrice(),
            ($request->getComment() == '') ? null : $request->getComment(),
            $passengerSeat,
            $sleepingCabinBed,
            $sleepingCabinType,
            ($request->getManualOrderNumber() == '') ? null : $request->getManualOrderNumber()
        );
        $this->orderRepository->save($order);
        return $order;
    }
}