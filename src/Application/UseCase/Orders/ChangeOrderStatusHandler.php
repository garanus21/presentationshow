<?php

declare(strict_types=1);

namespace App\Application\UseCase\Orders;

use App\Application\UseCase\Orders\Request\ChangeOrderStatus;
use App\Domain\Orders\Model\Order;
use App\Domain\Orders\Repository\OrderRepositoryInterface;
use App\Domain\User\Repository\UserRepositoryInterface;

/**
 * Class ChangeOrderStatusHandler
 * @package App\Application\UseCase\Orders
 */
class ChangeOrderStatusHandler
{
    /**
     * User repository.
     *
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * Order repository.
     *
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * ChangeOrderStatusHandler constructor.
     * @param UserRepositoryInterface $userRepository
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        OrderRepositoryInterface $orderRepository
    ) {
        $this->userRepository = $userRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Function handle.
     *
     * @param ChangeOrderStatus $request
     * @return Order
     */
    public function handle(ChangeOrderStatus $request): Order
    {
        $order = $this->orderRepository->getOneById($request->getOrderId());
        $order->setStatus($request->getStatus());
        $this->orderRepository->save($order);

        return $order;
    }
}