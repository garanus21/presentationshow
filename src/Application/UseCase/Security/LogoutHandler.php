<?php

declare(strict_types=1);

namespace App\Application\UseCase\Security;

use App\Application\UseCase\Security\Request\Logout;
use App\Domain\Security\Repository\AccessTokenRepositoryInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class LogoutHandler
 *
 * @package App\Application\UseCase\Security
 */
class LogoutHandler
{
    /**
     * @var AccessTokenRepositoryInterface
     */
    private $repository;

    /**
     * @var TokenStorageInterface
     */
    private $storage;

    /**
     * LogoutHandler constructor.
     *
     * @param AccessTokenRepositoryInterface $repository
     * @param TokenStorageInterface $storage
     */
    public function __construct(
        AccessTokenRepositoryInterface $repository,
        TokenStorageInterface $storage
    ) {
        $this->repository = $repository;
        $this->storage = $storage;
    }

    /**
     * @param Logout $request
     */
    public function handle(Logout $request)
    {
        $this->repository->removeByToken(
            $this->storage->getToken()->getCredentials()
        );
    }
}
