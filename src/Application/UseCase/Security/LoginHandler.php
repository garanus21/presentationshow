<?php

declare(strict_types=1);

namespace App\Application\UseCase\Security;

use App\Application\UseCase\Security\Request\Login;
use App\Domain\Security\Exception\AuthenticationException;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\Security\Model\Auth;
use FOS\OAuthServerBundle\Model\AccessTokenInterface;
use OAuth2\Model\IOAuth2AccessToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class LoginHandler
 *
 * @package App\Application\UseCase\Security
 */
class LoginHandler
{
    /**
     * @var AuthenticationUtils
     */
    private $authenticationUtils;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var EncoderFactoryInterface
     */
    private $encoderFactory;

    /**
     * LoginHandler constructor.
     *
     * @param AuthenticationUtils $authenticationUtils
     * @param UserRepositoryInterface $userRepository
     * @param EncoderFactoryInterface $encoderFactory
     */
    public function __construct(
        AuthenticationUtils $authenticationUtils,
        UserRepositoryInterface $userRepository,
        EncoderFactoryInterface $encoderFactory
    ) {
        $this->authenticationUtils = $authenticationUtils;
        $this->userRepository = $userRepository;
        $this->encoderFactory = $encoderFactory;
    }
}
