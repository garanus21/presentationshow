<?php

declare(strict_types=1);

namespace App\Application\UseCase\Security\Request;

use App\Domain\Security\Model\AccessToken;

/**
 * Class Logout
 *
 * @package App\Application\UseCase\Security\Request
 */
class Logout
{
    /**
     * @var AccessToken
     */
    private $token;

    /**
     * Logout constructor.
     *
     */
    public function __construct()
    {
    }
}
