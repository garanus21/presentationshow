<?php

declare(strict_types=1);

namespace App\UI\Rest\Controller\User;

use App\Application\UseCase\User\Request\ActivateAccount;
use App\Application\UseCase\User\Request\BlockUserAccount;
use App\Application\UseCase\User\Request\Delete;
use App\Application\UseCase\User\Request\ForgotPassword;
use App\Application\UseCase\User\Request\GetUser;
use App\Application\UseCase\User\Request\GetUsers;
use App\Application\UseCase\User\Request\Me;
use App\Application\UseCase\User\Request\Register;
use App\Application\UseCase\User\Request\ResetPassword;
use App\Application\UseCase\User\Request\UnlockUserAccount;
use App\Application\UseCase\User\Request\Update;
use App\Domain\User\Model\User;
use App\Domain\User\ValueObject\UserId;
use App\Infrastructure\Common\Exception\Form\FormException;
use App\UI\Rest\Controller\AbstractBusController;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Swagger\Annotations as SWG;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

/**
 * Class UserController
 *
 * @package App\UI\Rest\Controller\User
 */
class UserController extends AbstractBusController
{
    /**
     * Show all users.
     *
     * @SWG\Get(
     *     summary="Find all Users.",
     *     description="Find all Users.",
     *     tags={"User"},
     *     @SWG\Response(
     *          response="200",
     *          description="User collection response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\User\Model\User::class,
     *                      groups={"user"}
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @return Response
     */
    public function list(): Response
    {
        return $this->handleView(
            $this
                ->view($this->ask(new  GetUsers(
                    $this->getUser()->uuid()
                )))
                ->setContext((new Context())->setGroups(['user']))
        );
    }

    /**
     * Show chosen user details.
     *
     * @SWG\Get(
     *     summary="Find user by id",
     *     description="Find user by id",
     *     tags={"User"},
     *     @SWG\Response(
     *          response="200",
     *          description="User resource response",
     *          @Nelmio\Model(type=App\Domain\User\Model\User::class)
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param int $uuid
     * @return User
     * @throws \Exception
     */
    public function show(int $uuid)
    {
        return $this->ask(new GetUser(
            $uuid,
            $this->getUser()->uuid()
        ));
    }

    /**
     * Delete chosen user.
     *
     * @SWG\Delete(
     *     summary="Delete user",
     *     description="Delete user",
     *     tags={"User"},
     *     @SWG\Response(
     *          response="204",
     *          description="User resource deleted"
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="User Not Found"
     *     )
     * )
     *
     * @param int $uuid
     * @return JsonResponse
     *
     * @Nelmio\Security(name="Bearer")
     * @throws \Exception
     */
    public function delete(int $uuid)
    {
        $this->handle(new Delete(
            $this->getUser()->uuid(),
            $uuid
        ));
        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * User forgot password with sending email.
     *
     * @SWG\Post(
     *     summary="User forgot password",
     *     description="User forgot password reset link",
     *     tags={"User"},
     *     @SWG\Response(
     *          response="200",
     *          description="Forgot password email send"
     *      ),
     *      @SWG\Response(
     *          response="404",
     *          description="User not found",
     *      )
     * )
     *
     * @Rest\RequestParam(name="email", strict=false, description="Email")
     *
     * @param ParamFetcherInterface $fetcher
     * @return FormInterface|JsonResponse
     */
    public function forgotPassword(ParamFetcherInterface $fetcher)
    {
        try {
            $user = $this->handle(new ForgotPassword($fetcher->get('email')));

            return new JsonResponse('user.forgot.password.email.send', JsonResponse::HTTP_OK);
        } catch (FormException $e) {
            return $e->getForm();
        }
    }

    /**
     * User reset password(creating new password).
     *
     * @SWG\Post(
     *     summary="User reset password",
     *     description="User reset password with link",
     *     tags={"User"},
     *     @SWG\Response(
     *          response="200",
     *          description="User password updated"
     *      ),
     *     @SWG\Response(
     *          response="400",
     *          description="Wrong input"
     *      ),
     *      @SWG\Response(
     *          response="404",
     *          description="User or reset code not found",
     *      )
     * )
     * @Rest\RequestParam(name="password", strict=false, description="Password")
     * @Rest\RequestParam(name="repeated_password", strict=false, description="Repeated Password")
     *
     * @param ParamFetcherInterface $fetcher
     * @param string $code
     * @return FormInterface|JsonResponse
     */
    public function resetPassword(ParamFetcherInterface $fetcher, string $code)
    {
        try {
            $user = $this->handle(
                new ResetPassword(
                    $code,
                    $fetcher->get('password'),
                    $fetcher->get('repeated_password')
                )
            );

            return new JsonResponse('user.password.changed', JsonResponse::HTTP_OK);
        } catch (FormException $e) {
            return $e->getForm();
        }
    }

    /**
     * New user register action.
     *
     * @SWG\Post(
     *     summary="Register user",
     *     description="Create user account",
     *     tags={"User"},
     *     @SWG\Response(
     *          response="201",
     *          description="User resource registered"
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *         response="409",
     *         description="Already exist",
     *     )
     * )
     *
     * @Rest\RequestParam(name="email", strict=false, description="Unique email")
     * @Rest\RequestParam(name="first_name", strict=false, description="First name")
     * @Rest\RequestParam(name="last_name", strict=false, description="Last name")
     * @Rest\RequestParam(name="password", strict=false, description="Plain password")
     * @Rest\RequestParam(name="phone", strict=false, description="Phone number.")
     * @Nelmio\Security(name="Bearer")
     * @param ParamFetcherInterface $fetcher
     * @return FormInterface|JsonResponse
     * @throws \Exception
     */
    public function register(ParamFetcherInterface $fetcher)
    {
        try {
            $user = $this->handle(
                new Register(
                    $this->getUser()->uuid(),
                    new UserId(),
                    $fetcher->get('email'),
                    $fetcher->get('first_name'),
                    $fetcher->get('last_name'),
                    $fetcher->get('password'),
                    $fetcher->get('phone')
                )
            );

            return new JsonResponse('user.registered', JsonResponse::HTTP_CREATED);
        } catch (FormException $e) {
            return $e->getForm();
        } catch (UniqueConstraintViolationException $e) {
            throw new ConflictHttpException('user.exception.already_exist');
        }
    }

    /**
     * Currently logged in User resource response.
     *
     * @SWG\Get(
     *     summary="Show own details",
     *     description="Show logged user details",
     *     tags={"Me"},
     *     @SWG\Response(
     *          response="200",
     *          description="Currently logged in User resource response",
     *          @Nelmio\Model(
     *              type=App\Domain\User\Model\User::class,
     *              groups={"user"}
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @return Response
     */
    public function me()
    {
        return $this->handleView(
            $this
                ->view($this->ask(new Me($this->getUser()->uuid())))
                ->setContext((new Context())->setGroups(['user']))
        );
    }

    /**
     * Update user own profile data.
     *
     * @SWG\Patch(
     *     summary="Update user own profile",
     *     description="Update login user information",
     *     tags={"Me"},
     *     @SWG\Response(
     *          response="200",
     *          description="User resource updated"
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @Rest\RequestParam(name="first_name", strict=false, description="First name")
     * @Rest\RequestParam(name="last_name", strict=false, description="Last name")
     * @Rest\RequestParam(name="phone", strict=false, description="Phone")
     *
     * @param ParamFetcherInterface $fetcher
     * @return FormInterface|JsonResponse
     */
    public function update(ParamFetcherInterface $fetcher)
    {
        try {
            $user = $this->handle(
                new Update(
                    $this->getUser()->uuid(),
                    $fetcher->get('first_name'),
                    $fetcher->get('last_name'),
                    $fetcher->get('phone')
                )
            );

            return new JsonResponse('user.updated', JsonResponse::HTTP_OK);
        } catch (FormException $e) {
            return $e->getForm();
        }
    }

//    /**
//     * Block User Account.
//     *
//     * @SWG\Delete(
//     *     summary="Block User Account.",
//     *     description="Block User Account.",
//     *     tags={"User"},
//     *     @SWG\Response(
//     *          response="200",
//     *          description="User Blocked"
//     *     ),
//     *     @SWG\Response(
//     *          response="400",
//     *          description="Invalid input"
//     *     ),
//     *     @SWG\Response(
//     *          response="401",
//     *          description="Unauthorized"
//     *     )
//     * )
//     * @Nelmio\Security(name="Bearer")
//     *
//     * @param ParamFetcherInterface $fetcher
//     * @param int $uuid
//     * @return FormInterface|JsonResponse
//     */
//    public function blockUserAccount(ParamFetcherInterface $fetcher, int $uuid)
//    {
//        try {
//            $user = $this->handle(
//                new BlockUserAccount(
//                    $uuid,
//                    $this->getUser()->uuid()
//                )
//            );
//
//            return new JsonResponse('BLOCKED', JsonResponse::HTTP_OK);
//        } catch (FormException $e) {
//            return $e->getForm();
//        }
//    }

//    /**
//     * Unlock user account.
//     *
//     * @SWG\Patch(
//     *     summary="Unlock user account.",
//     *     description="Unlock user account.",
//     *     tags={"User"},
//     *     @SWG\Response(
//     *          response="200",
//     *          description="User unlocked."
//     *     ),
//     *     @SWG\Response(
//     *          response="400",
//     *          description="Invalid input"
//     *     ),
//     *     @SWG\Response(
//     *          response="401",
//     *          description="Unauthorized"
//     *     )
//     * )
//     * @Nelmio\Security(name="Bearer")
//     *
//     * @param ParamFetcherInterface $fetcher
//     * @param int $uuid
//     * @return FormInterface|JsonResponse
//     */
//    public function unlockUserAccount(ParamFetcherInterface $fetcher, int $uuid)
//    {
//        try {
//            $user = $this->handle(
//                new UnlockUserAccount(
//                    $uuid,
//                    $this->getUser()->uuid()
//                )
//            );
//
//            return new JsonResponse('Unlocked', JsonResponse::HTTP_OK);
//        } catch (FormException $e) {
//            return $e->getForm();
//        }
//    }

//    /**
//     * Activate user account.
//     *
//     * @SWG\Patch(
//     *     summary="Activate user account.",
//     *     description="Activate user account.",
//     *     tags={"User"},
//     *     @SWG\Response(
//     *          response="200",
//     *          description="User activated."
//     *     ),
//     *     @SWG\Response(
//     *          response="400",
//     *          description="Invalid input"
//     *     ),
//     *     @SWG\Response(
//     *          response="401",
//     *          description="Unauthorized"
//     *     )
//     * )
//     * @Nelmio\Security(name="Bearer")
//     *
//     * @Rest\RequestParam(name="email", strict=false, description="User e-mail.")
//     * @Rest\RequestParam(name="hash", strict=false, description="User activation hash.")
//     *
//     * @param ParamFetcherInterface $fetcher
//     * @return FormInterface|JsonResponse
//     */
//    public function activateUserAccount(ParamFetcherInterface $fetcher)
//    {
//        try {
//            $user = $this->handle(
//                new ActivateAccount(
//                    $fetcher->get('hash'),
//                    $fetcher->get('email')
//                )
//            );
//
//            return new JsonResponse('Activated', JsonResponse::HTTP_OK);
//        } catch (FormException $e) {
//            return $e->getForm();
//        }
//    }
}
