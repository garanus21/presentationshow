<?php

declare(strict_types=1);

namespace App\UI\Rest\Controller\Security;

use App\Application\UseCase\Security\Request\Logout;
use App\UI\Rest\Controller\AbstractBusController;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Swagger\Annotations as SWG;

/**
 * Class SecurityController
 *
 * @package App\UI\Rest\Controller\Security
 */
class SecurityController extends AbstractBusController
{
    /**
     * Logout user.
     *
     * @return JsonResponse
     *
     * @SWG\Delete(
     *     summary="Logout user",
     *     description="Logout user",
     *     tags={"Security"},
     *     @SWG\Response(
     *          response="204",
     *          description="Logout"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     )
     * )
     * @Security(name="Bearer")
     */
    public function logoutAction()
    {
        $this->handle(new Logout());

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }
}
