<?php

declare(strict_types=1);

namespace App\UI\Rest\Controller\Orders;

use App\Application\UseCase\Orders\Request\AddOrderAdditionalEquipment;
use App\Application\UseCase\Orders\Request\ChangeOrderStatus;
use App\Application\UseCase\Orders\Request\CreateOrder;
use App\Application\UseCase\Orders\Request\DeleteOrder;
use App\Application\UseCase\Orders\Request\DeleteOrderAdditionalEquipment;
use App\Application\UseCase\Orders\Request\GetOrder;
use App\Application\UseCase\Orders\Request\GetOrderAdditionalEquipment;
use App\Application\UseCase\Orders\Request\GetOrderAdditionalEquipments;
use App\Application\UseCase\Orders\Request\GetOrders;
use App\Application\UseCase\Orders\Request\GetOrdersPaginated;
use App\Application\UseCase\Orders\Request\SearchOrder;
use App\Application\UseCase\Orders\Request\UpdateOrder;
use App\Domain\Orders\Model\Order;
use App\Domain\Orders\ValueObject\OrderEquipmentId;
use App\Domain\Orders\ValueObject\OrderId;
use App\UI\Rest\Controller\AbstractBusController;
use App\Infrastructure\Common\Exception\Form\FormException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class OrderController
 * @package App\UI\Rest\Controller\Orders
 */
class OrderController extends AbstractBusController
{
    /**
     * Create order.
     *
     * @SWG\Post(
     *     summary="Create order.",
     *     description="Create order.",
     *     tags={"Orders"},
     *     @SWG\Response(
     *          response="201",
     *          description="Order created."
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *      @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Acces denied - wrong user"
     *     ),
     *     @SWG\Response(
     *         response="409",
     *         description="Already exist",
     *     )
     * )
     *
     * @Rest\RequestParam(name="client_name", strict=false, description="Nazwa klienta.(string)")
     * @Rest\RequestParam(name="client_address", strict=false, description="Adres klienta.(string)")
     * @Rest\RequestParam(name="client_nip", strict=false, description="NIP klienta.(string)")
     * @Rest\RequestParam(name="client_regon", strict=false, description="REGON klienta.(string)")
     * @Rest\RequestParam(name="cabin_id", strict=false, description="Identyfikator kabiny.(int)")
     * @Rest\RequestParam(name="brand_id", strict=false, description="Identyfikator marki pojazdu.(int)")
     * @Rest\RequestParam(name="production_year", strict=false, description="Rok produkcji.(string)")
     * @Rest\RequestParam(name="color_id", strict=false, description="Identyfikator koloru.(integer)")
     * @Rest\RequestParam(name="color_number", strict=false, description="Numer koloru.(string)")
     * @Rest\RequestParam(name="vin", strict=false, description="VIN pojazdu.(string)")
     * @Rest\RequestParam(name="place_assembly", strict=false, description="Miejsce montażu.(string)")
     * @Rest\RequestParam(name="order_completion_time", strict=false, description="Termin realizacji zamówienia.(string(yyyy-mm-dd))")
     * @Rest\RequestParam(name="laminated", strict=false, description="Wylaminowa.(string(yyyy-mm-dd))")
     * @Rest\RequestParam(name="outcropped", strict=false, description="Wykostkowana.(string(yyyy-mm-dd))")
     * @Rest\RequestParam(name="upholstered", strict=false, description="Wytapicerowana.(string(yyyy-mm-dd))")
     * @Rest\RequestParam(name="varnishing", strict=false, description="Lakierowanie.(string(yyyy-mm-dd))")
     * @Rest\RequestParam(name="ready_to_assembly", strict=false, description="Gotowa do montażu.(string(yyyy-mm-dd))")
     * @Rest\RequestParam(name="assembly", strict=false, description="Montaż.(string(yyyy-mm-dd))")
     * @Rest\RequestParam(name="total_price", strict=false, description="Lączna kwota.(string)")
     * @Rest\RequestParam(name="comment", strict=false, description="Komentarz.(string)")
     * @Rest\RequestParam(name="passenger_seats_id", strict=false, description="Passenger seat id.(int)")
     * @Rest\RequestParam(name="sleep_cabin_beds_id", strict=false, description="Sleeping cabin beds id.(int)")
     * @Rest\RequestParam(name="sleep_cabin_types_id", strict=false, description="Sleeping cabin type.(int)")
     * @Rest\RequestParam(name="manual_order_number", strict=false, description="Manual order number.(string)")
     *
     * @param ParamFetcherInterface $fetcher
     * @return View
     * @throws Exception
     */
    public function createOrder(ParamFetcherInterface $fetcher)
    {
        try {
            $order = $this->handle(
                new CreateOrder(
                    $this->getUser()->uuid(),
                    new OrderId(),
                    $fetcher->get('client_name'),
                    $fetcher->get('client_address'),
                    $fetcher->get('client_nip'),
                    $fetcher->get('client_regon'),
                    $fetcher->get('cabin_id'),
                    ($fetcher->get('brand_id') == "")? null : $fetcher->get('brand_id'),
                    $fetcher->get('production_year'),
                    ($fetcher->get('color_id') == "")? null : $fetcher->get('color_id'),
                    $fetcher->get('color_number'),
                    $fetcher->get('vin'),
                    $fetcher->get('place_assembly'),
                    $fetcher->get('order_completion_time'),
                    $fetcher->get('laminated'),
                    $fetcher->get('outcropped'),
                    $fetcher->get('upholstered'),
                    $fetcher->get('varnishing'),
                    $fetcher->get('ready_to_assembly'),
                    $fetcher->get('assembly'),
                    $fetcher->get('total_price'),
                    $fetcher->get('comment'),
                    ($fetcher->get('passenger_seats_id') == "")? null : $fetcher->get('passenger_seats_id'),
                    ($fetcher->get('sleep_cabin_beds_id') == "")? null : $fetcher->get('sleep_cabin_beds_id'),
                    ($fetcher->get('sleep_cabin_types_id') == "")? null : $fetcher->get('sleep_cabin_types_id'),
                    ($fetcher->get('manual_order_number') == "")? null : $fetcher->get('manual_order_number')
                )
            );

            return $this->routeRedirectView(
                'api_v1_order_details',
                [
                    'orderId' => $order->uuid()
                ]
            )
                ->setData($order);
        } catch (UniqueConstraintViolationException $e) {
            throw new ConflictHttpException('order.exception.already_exist');
        }
    }

    /**
     * Show order details.
     *
     * @SWG\Get(
     *     summary="Show order details.",
     *     description="Show order details.",
     *     tags={"Orders"},
     *     @SWG\Response(
     *          response="200",
     *          description="Order resource response",
     *          @Nelmio\Model(type=App\Domain\Orders\Model\Order::class)
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param int $orderId
     * @return Order
     */
    public function showOrder(int $orderId)
    {
        return $this->ask(new GetOrder(
            $orderId
        ));
    }

    /**
     * List orders.
     *
     * @SWG\Get(
     *     summary="List orders.",
     *     description="List orders.",
     *     tags={"Orders"},
     *     @SWG\Response(
     *          response="200",
     *          description="Order resource response",
     *          @Nelmio\Model(type=App\Domain\Orders\Model\Order::class)
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @return Order
     */
    public function listOrders()
    {
        return $this->ask(new GetOrders());
    }

    /**
     * Update order.
     *
     * @SWG\Patch(
     *     summary="Update order.",
     *     description="Update order.",
     *     tags={"Orders"},
     *     @SWG\Response(
     *          response="200",
     *          description="Order updated."
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *      @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Acces denied - wrong user"
     *     ),
     *     @SWG\Response(
     *         response="409",
     *         description="Already exist",
     *     )
     * )
     *
     * @Rest\RequestParam(name="client_name", strict=false, description="Nazwa klienta.(string)")
     * @Rest\RequestParam(name="client_address", strict=false, description="Adres klienta.(string)")
     * @Rest\RequestParam(name="client_nip", strict=false, description="NIP klienta.(string)")
     * @Rest\RequestParam(name="client_regon", strict=false, description="REGON klienta.(string)")
     * @Rest\RequestParam(name="status", strict=false, description="Status zamówienia.(eg. 0 - new, 1 - in progress, 2 - done, 3 - canceled)(integer)")
     * @Rest\RequestParam(name="production_year", strict=false, description="Rok produkcji.(string)")
     * @Rest\RequestParam(name="color_number", strict=false, description="Numer koloru.(string)")
     * @Rest\RequestParam(name="vin", strict=false, description="VIN pojazdu.(string)")
     * @Rest\RequestParam(name="place_assembly", strict=false, description="Miejsce montażu.(string)")
     * @Rest\RequestParam(name="order_completion_time", strict=false, description="Termin realizacji zamówienia.(string)")
     * @Rest\RequestParam(name="laminated", strict=false, description="Wylaminowa.(string(yyyy-mm-dd))")
     * @Rest\RequestParam(name="outcropped", strict=false, description="Wykostkowana.(string(yyyy-mm-dd))")
     * @Rest\RequestParam(name="upholstered", strict=false, description="Wytapicerowana.(string(yyyy-mm-dd))")
     * @Rest\RequestParam(name="varnishing", strict=false, description="Lakierowanie.(string(yyyy-mm-dd))")
     * @Rest\RequestParam(name="ready_to_assembly", strict=false, description="Gotowa do montażu.(string(yyyy-mm-dd))")
     * @Rest\RequestParam(name="assembly", strict=false, description="Montaż.(string(yyyy-mm-dd))")
     * @Rest\RequestParam(name="total_price", strict=false, description="Lączna kwota.(string)")
     * @Rest\RequestParam(name="comment", strict=false, description="Komentarz.(string)")
     * @Rest\RequestParam(name="passenger_seats_id", strict=false, description="Passenger seat id.(int)")
     * @Rest\RequestParam(name="sleep_cabin_beds_id", strict=false, description="Sleeping cabin beds id.(int)")
     * @Rest\RequestParam(name="sleep_cabin_types_id", strict=false, description="Sleeping cabin type.(int)")
     * @Rest\RequestParam(name="manual_order_number", strict=false, description="Manual order number.(string)")
     *
     * @param ParamFetcherInterface $fetcher
     * @param int $orderId
     * @return View
     * @throws Exception
     */
    public function updateOrder(ParamFetcherInterface $fetcher, int $orderId)
    {
        try {
            $order = $this->handle(
                new UpdateOrder(
                    $this->getUser()->uuid(),
                    $orderId,
                    $fetcher->get('client_name'),
                    $fetcher->get('client_address'),
                    $fetcher->get('client_nip'),
                    $fetcher->get('client_regon'),
                    $fetcher->get('status'),
                    $fetcher->get('production_year'),
                    $fetcher->get('color_number'),
                    $fetcher->get('vin'),
                    $fetcher->get('place_assembly'),
                    $fetcher->get('order_completion_time'),
                    $fetcher->get('laminated'),
                    $fetcher->get('outcropped'),
                    $fetcher->get('upholstered'),
                    $fetcher->get('varnishing'),
                    $fetcher->get('ready_to_assembly'),
                    $fetcher->get('assembly'),
                    $fetcher->get('total_price'),
                    $fetcher->get('comment'),
                    ($fetcher->get('passenger_seats_id') == "")? null : $fetcher->get('passenger_seats_id'),
                    ($fetcher->get('sleep_cabin_beds_id') == "")? null : $fetcher->get('sleep_cabin_beds_id'),
                    ($fetcher->get('sleep_cabin_types_id') == "")? null : $fetcher->get('sleep_cabin_types_id'),
                    ($fetcher->get('manual_order_number') == "")? null : $fetcher->get('manual_order_number')
                )
            );

            return $this->routeRedirectView(
                'api_v1_order_details',
                [
                    'orderId' => $order->uuid()
                ]
            )
                ->setData($order);
        } catch (UniqueConstraintViolationException $e) {
            throw new ConflictHttpException('order.exception.already_exist');
        }
    }

    /**
     * Update order status.
     *
     * @SWG\Patch(
     *     summary="Update order status.",
     *     description="Update order status.",
     *     tags={"Orders"},
     *     @SWG\Response(
     *          response="200",
     *          description="Order status updated."
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *      @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Acces denied - wrong user"
     *     ),
     *     @SWG\Response(
     *         response="409",
     *         description="Already exist",
     *     )
     * )
     *
     * @Rest\RequestParam(name="status", strict=false, description="Status zamówienia.(eg. 0 - new, 1 - in progress, 2 - done, 3 - canceled)(integer)")
     *
     * @param ParamFetcherInterface $fetcher
     * @param int $orderId
     * @return View
     * @throws Exception
     */
    public function changeOrderStatus(ParamFetcherInterface $fetcher, int $orderId)
    {
        try {
            $order = $this->handle(
                new ChangeOrderStatus(
                    $this->getUser()->uuid(),
                    $orderId,
                    $fetcher->get('status')
                )
            );

            return $this->routeRedirectView(
                'api_v1_order_details',
                [
                    'orderId' => $order->uuid()
                ]
            )
                ->setData($order);
        } catch (UniqueConstraintViolationException $e) {
            throw new ConflictHttpException('order.exception.already_exist');
        }
    }

    /**
     * Create order equipment.
     *
     * @SWG\Post(
     *     summary="Create order equipment.",
     *     description="Create order equipment.",
     *     tags={"Orders Equipment"},
     *     @SWG\Response(
     *          response="201",
     *          description="Order equipment created."
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *      @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Acces denied - wrong user"
     *     ),
     *     @SWG\Response(
     *         response="409",
     *         description="Already exist",
     *     )
     * )
     *
     * @Rest\RequestParam(name="order_id", strict=false, description="Id zamówienia.(int)")
     * @Rest\RequestParam(name="additional_equipment_id", strict=false, description="Id wyposażenia dodatkowego.(int)")
     * @Rest\RequestParam(name="comment", strict=false, description="Komentarz - opcjonalne.(string)")
     * @Rest\RequestParam(name="price", strict=false, description="Cena - opcjonalne.(string)")
     *
     * @param ParamFetcherInterface $fetcher
     * @return View
     * @throws Exception
     */
    public function createOrderEquipment(ParamFetcherInterface $fetcher)
    {
        try {
            $orderEquipment = $this->handle(
                new AddOrderAdditionalEquipment(
                    $this->getUser()->uuid(),
                    new OrderEquipmentId(),
                    $fetcher->get('order_id'),
                    $fetcher->get('additional_equipment_id'),
                    $fetcher->get('comment'),
                    $fetcher->get('price')
                )
            );

            return $this->routeRedirectView(
                'api_v1_order_equipment_details',
                [
                    'orderEquipmentId' => $orderEquipment->uuid()
                ]
            )
                ->setData($orderEquipment);
        } catch (UniqueConstraintViolationException $e) {
            throw new ConflictHttpException('order_equipment.exception.already_exist');
        }
    }

    /**
     * Show order equipment details.
     *
     * @SWG\Get(
     *     summary="Show order equipment details.",
     *     description="Show order equipment details.",
     *     tags={"Orders Equipment"},
     *     @SWG\Response(
     *          response="200",
     *          description="Order equipment resource response",
     *          @Nelmio\Model(type=App\Domain\Orders\Model\OrderEquipment::class)
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param int $orderEquipmentId
     * @return Order
     */
    public function showOrderEquipment(int $orderEquipmentId)
    {
        return $this->ask(new GetOrderAdditionalEquipment(
            $this->getUser()->uuid(),
            $orderEquipmentId
        ));
    }

    /**
     * List order equipment.
     *
     * @SWG\Get(
     *     summary="List order equipment. equipment.",
     *     description="List order equipment.",
     *     tags={"Orders Equipment"},
     *     @SWG\Response(
     *          response="200",
     *          description="Order equipments response",
     *          @Nelmio\Model(type=App\Domain\Orders\Model\OrderEquipment::class)
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param int $orderId
     * @return Order
     */
    public function listOrderEquipments(int $orderId)
    {
        return $this->ask(new GetOrderAdditionalEquipments($this->getUser()->uuid(), $orderId));
    }

    /**
     * Delete order equipment.
     *
     * @SWG\Delete(
     *     summary="Show order equipment.",
     *     description="Show order equipment.",
     *     tags={"Orders Equipment"},
     *     @SWG\Response(
     *          response="204",
     *          description="Order equipment deleted response"
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param int $orderEquipmentId
     * @return Order
     */
    public function deleteOrderEquipment(int $orderEquipmentId)
    {
        return $this->ask(new DeleteOrderAdditionalEquipment(
            $this->getUser()->uuid(),
            $orderEquipmentId
        ));
    }

    /**
     * List paginated orders.
     *
     * @SWG\Get(
     *     summary="List paginated orders.",
     *     description="List paginated orders.",
     *     tags={"Orders"},
     *     @SWG\Response(
     *          response="200",
     *          description="Orders list."
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid Input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @Rest\QueryParam(name="page", strict=false, default="1", description="Number of page.")
     * @Rest\QueryParam(name="limit", strict=false, default="50", description="Limit items on page.")
     *
     * @param ParamFetcherInterface $fetcher
     * @return mixed
     */
    public function listOrdersPaginated(ParamFetcherInterface $fetcher)
    {
        $result = $this->ask(new GetOrdersPaginated(
            (int) $fetcher->get('page'),
            (int) $fetcher->get('limit')
        ));

        return $this->view(
            $result['data'],
            null,
            [
                'X-Total-Count' => $result['total'],
            ]
        );
    }

    /**
     * Search orders by parameters paginated.
     *
     * @SWG\Get(
     *     summary="Search orders by parameters paginated.",
     *     description="Search orders by parameters paginated.",
     *     tags={"Orders Search"},
     *     @SWG\Response(
     *          response="200",
     *          description="Orders collection response.",
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @Rest\QueryParam(name="last_name", strict=false, description="Customer last name.(string)")
     * @Rest\QueryParam(name="order_date", strict=false, description="Search by order date.(string(yyyy-mm-dd)eg.2021-06-07)")
     * @Rest\QueryParam(name="equipment_name", strict=false, description="Search by additional equipment name.(string)")
     * @Rest\QueryParam(name="product_type", strict=false, description="Search by product type.(string)")
     * @Rest\QueryParam(name="order_status", strict=false, description="Search by order status name.(string)")
     * @Rest\QueryParam(name="nip", strict=false, description="Search by order nip.(string)")
     * @Rest\QueryParam(name="orderNumber", strict=false, description="Search by manual order number.(string)")
     * @Rest\QueryParam(name="orderCompletionTime", strict=false, description="Search by order completion time.(string)")
     * @Rest\QueryParam(name="created_date_order", strict=false, description="Order order by date.(eg. 0 - not order, 1 - order asc, 2 - order desc)(int)")
     * @Rest\QueryParam(name="client_name_order", strict=false, description="Order order by name.(eg. 0 - not order, 1 - order asc, 2 - order desc)(int)")
     * @Rest\QueryParam(name="page", strict=false, description="Paginate page.(int)")
     * @Rest\QueryParam(name="limit", strict=false, description="Paginate limit.(int)")
     * @param ParamFetcherInterface $fetcher
     * @return mixed
     */
    public function searchOrder(ParamFetcherInterface $fetcher)
    {
        $result = $this->ask(new SearchOrder(
            $fetcher->get('last_name'),
            $fetcher->get('order_date'),
            $fetcher->get('equipment_name'),
            $fetcher->get('product_type'),
            $fetcher->get('order_status'),
            $fetcher->get('nip'),
            (int)$fetcher->get('created_date_order'),
            (int)$fetcher->get('client_name_order'),
            $fetcher->get('orderNumber'),
            $fetcher->get('orderCompletionTime'),
            (int) $fetcher->get('page'),
            (int) $fetcher->get('limit')
        ));

        return $this->handleView(
            $this
                ->view(
                    $result['data'],
                    null,
                    [
                        'X-Total-Count' => $result['total'],
                    ]
                )
        );
    }

    /**
     * Delete order.
     *
     * @SWG\Delete(
     *     summary="Delete order.",
     *     description="Delete order.",
     *     tags={"Orders"},
     *     @SWG\Response(
     *          response="204",
     *          description="Order deleted"
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param int $orderId
     * @return mixed
     */
    public function deleteOrder(int $orderId)
    {
        return $this->ask(new DeleteOrder(
            $orderId
        ));
    }
}