<?php

declare(strict_types=1);

namespace App\UI\Rest\Controller\Common;

use App\Application\UseCase\Common\Cities\Request\SearchCity;
use App\UI\Rest\Controller\AbstractBusController;
use App\Infrastructure\Common\Exception\Form\FormException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Context\Context;
use Nelmio\ApiDocBundle\Annotation\Security;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Swagger\Annotations as SWG;

/**
 * Class CityController
 *
 * @package App\UI\Rest\Controller\Common
 */
class CityController extends AbstractBusController
{
//    /**
//     * Search Cities.
//     *
//     * @SWG\Get(
//     *     summary="Find cities.",
//     *     description="Find cities.",
//     *     tags={"Cities"},
//     *     @SWG\Response(
//     *          response="200",
//     *          description="Cities found",
//     *          @SWG\Schema(
//     *              @SWG\Items(
//     *                  ref=@Nelmio\Model(
//     *                      type=App\Domain\Common\Model\Cities::class
//     *                  )
//     *              )
//     *          )
//     *     ),
//     *      @SWG\Response(
//     *          response="401",
//     *          description="Unauthorized"
//     *     ),
//     *     @SWG\Response(
//     *          response="404",
//     *          description="Not found",
//     *     )
//     * )
//     * @Security(name="Bearer")
//     * @Rest\QueryParam(name="query", strict=false, description="Search by query.")
//     *
//     * @param ParamFetcherInterface $fetcher
//     * @return mixed
//     */
//    public function searchCities(ParamFetcherInterface $fetcher)
//    {
//        return $this->ask(new SearchCity(
//            $fetcher->get('query')
//        ));
//    }
}
