<?php

declare(strict_types=1);

namespace App\UI\Rest\Controller\Common;

use App\Application\UseCase\Common\Countries\Request\CreateCountry;
use App\Application\UseCase\Common\Countries\Request\GetCountries;
use App\Domain\Common\ValueObject\CountriesId;
use App\Infrastructure\Common\Exception\Form\FormException;
use App\UI\Rest\Controller\AbstractBusController;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Context\Context;
use Nelmio\ApiDocBundle\Annotation\Security;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Swagger\Annotations as SWG;

/**
 * Class CountryController
 *
 * @package App\UI\Rest\Controller\Common
 */
class CountryController extends AbstractBusController
{
//    /**
//     * Create new Country.
//     *
//     * @SWG\Post(
//     *     summary="Create new Country.",
//     *     description="Create new Country.",
//     *     tags={"Country"},
//     *     @SWG\Response(
//     *          response="201",
//     *          description="New Country created"
//     *      ),
//     *      @SWG\Response(
//     *          response="401",
//     *          description="Unauthorized"
//     *     ),
//     *      @SWG\Response(
//     *          response="404",
//     *          description="Not found",
//     *      ),
//     *      @SWG\Response(
//     *          response="409",
//     *          description="Already exist",
//     *      )
//     * )
//     * @Security(name="Bearer")
//     *
//     * @Rest\RequestParam(name="name", strict=false, default="", description="Country name.")
//     *
//     * @param ParamFetcherInterface $fetcher
//     * @return FormInterface|JsonResponse
//     * @throws Exception
//     */
//    public function createCountry(ParamFetcherInterface $fetcher)
//    {
//        try {
//            $country = $this->handle(
//                new CreateCountry(
//                    new CountriesId(),
//                    $fetcher->get('name')
//                )
//            );
//            return new JsonResponse('country.created', JsonResponse::HTTP_CREATED);
//        } catch (FormException $e) {
//            return $e->getForm();
//        } catch (UniqueConstraintViolationException $e) {
//            throw new ConflictHttpException('country.exception.already_exist');
//        }
//    }
//
//    /**
//     * Show all Countries.
//     *
//     * @SWG\Get(
//     *     summary="Find all Countries.",
//     *     description="Find all Countries.",
//     *     tags={"Country"},
//     *     @SWG\Response(
//     *          response="200",
//     *          description="Countries found",
//     *          @SWG\Schema(
//     *              @SWG\Items(
//     *                  ref=@Nelmio\Model(
//     *                      type=App\Domain\Common\Model\Countries::class
//     *                  )
//     *              )
//     *          )
//     *     ),
//     *      @SWG\Response(
//     *          response="401",
//     *          description="Unauthorized"
//     *     ),
//     *     @SWG\Response(
//     *          response="404",
//     *          description="Not found",
//     *     )
//     * )
//     * @Security(name="Bearer")
//     *
//     * @return mixed
//     */
//    public function list()
//    {
//        return $this->ask(new GetCountries());
//    }
}
