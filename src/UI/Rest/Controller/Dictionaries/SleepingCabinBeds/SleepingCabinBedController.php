<?php

declare(strict_types=1);

namespace App\UI\Rest\Controller\Dictionaries\SleepingCabinBeds;

use App\Application\UseCase\Dictionaries\SleepingCabinBeds\Request\GetCabinSleepingCabinBed;
use App\Application\UseCase\Dictionaries\SleepingCabinBeds\Request\GetSleepingCabinBed;
use App\Application\UseCase\Dictionaries\SleepingCabinBeds\Request\GetSleepingCabinBeds;
use App\UI\Rest\Controller\AbstractBusController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SleepingCabinBedController
 * @package App\UI\Rest\Controller\Dictionaries\SleepingCabinBeds
 */
class SleepingCabinBedController extends AbstractBusController
{
    /**
     * List all sleeping cabin beds.
     *
     * @SWG\Get(
     *     summary="List all sleeping cabin beds.",
     *     description="List all sleeping cabin beds.",
     *     tags={"Sleeping Cabin Beds"},
     *     @SWG\Response(
     *          response="200",
     *          description="Sleeping cabin bed collection response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\SleepingCabinBeds\Model\SleepingCabinBed::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @return Response
     */
    public function listSleepingCabinBeds()
    {
        return $this->ask(new GetSleepingCabinBeds());
    }

    /**
     * Show sleeping cabin bed.
     *
     * @SWG\Get(
     *     summary="Show sleeping cabin bed.",
     *     description="Show sleeping cabin bed.",
     *     tags={"Sleeping Cabin Beds"},
     *     @SWG\Response(
     *          response="200",
     *          description="Sleeping cabin bed details response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\SleepingCabinBeds\Model\SleepingCabinBed::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param int $sleepingCabinBedId
     * @return Response
     */
    public function showSleepingCabinBed(int $sleepingCabinBedId)
    {
        return $this->ask(new GetSleepingCabinBed($sleepingCabinBedId));
    }

    /**
     * List cabin sleeping cabin beds.
     *
     * @SWG\Get(
     *     summary="List cabin sleeping cabin beds.",
     *     description="List cabin sleeping cabin beds.",
     *     tags={"Sleeping Cabin Beds"},
     *     @SWG\Response(
     *          response="200",
     *          description="Sleeping cabin beds added to cabin collection response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\SleepingCabinBeds\Model\SleepingCabinBed::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param int $cabinId
     * @return Response
     */
    public function listCabinSleepingCabinBeds(int $cabinId)
    {
        return $this->ask(new GetCabinSleepingCabinBed($cabinId));
    }
}