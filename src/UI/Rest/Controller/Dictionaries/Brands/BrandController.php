<?php

declare(strict_types=1);

namespace App\UI\Rest\Controller\Dictionaries\Brands;

use App\Application\UseCase\Dictionaries\Brands\Request\GetBrand;
use App\Application\UseCase\Dictionaries\Brands\Request\GetBrands;
use App\UI\Rest\Controller\AbstractBusController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BrandController
 * @package App\UI\Rest\Controller\Dictionaries\Brands
 */
class BrandController extends AbstractBusController
{
    /**
     * List all brands.
     *
     * @SWG\Get(
     *     summary="List all brands.",
     *     description="List all brands.",
     *     tags={"Brands"},
     *     @SWG\Response(
     *          response="200",
     *          description="Brands collection response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\Brands\Model\Brand::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @return Response
     */
    public function listBrands()
    {
        return $this->ask(new GetBrands());
    }

    /**
     * Show brand.
     *
     * @SWG\Get(
     *     summary="Show brand.",
     *     description="Show brand.",
     *     tags={"Brands"},
     *     @SWG\Response(
     *          response="200",
     *          description="Brand details response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\Brands\Model\Brand::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param int $brandId
     * @return Response
     */
    public function showBrand(int $brandId)
    {
        return $this->ask(new GetBrand($brandId));
    }
}