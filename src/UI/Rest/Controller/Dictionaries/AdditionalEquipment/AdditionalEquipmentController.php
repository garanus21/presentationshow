<?php

declare(strict_types=1);

namespace App\UI\Rest\Controller\Dictionaries\AdditionalEquipment;

use App\Application\UseCase\Dictionaries\AdditionalEquipment\Request\GetAdditionalEquipment;
use App\Application\UseCase\Dictionaries\AdditionalEquipment\Request\GetAdditionalEquipmentList;
use App\Application\UseCase\Dictionaries\AdditionalEquipment\Request\GetCabinAdditionalEquipment;
use App\UI\Rest\Controller\AbstractBusController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AdditionalEquipmentController
 * @package App\UI\Rest\Controller\Dictionaries\AdditionalEquipment
 */
class AdditionalEquipmentController extends AbstractBusController
{
    /**
     * List all additional equipment list.
     *
     * @SWG\Get(
     *     summary="List all additional equipment list.",
     *     description="List all additional equipment list.",
     *     tags={"Additional Equipment"},
     *     @SWG\Response(
     *          response="200",
     *          description="All additional equipment collection response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\AdditionalEquipment\Model\AdditionalEquipment::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @return Response
     */
    public function listAdditionalEquipment()
    {
        return $this->ask(new GetAdditionalEquipmentList());
    }

    /**
     * Show additional equipment.
     *
     * @SWG\Get(
     *     summary="Show additional equipment.",
     *     description="Show additional equipment.",
     *     tags={"Additional Equipment"},
     *     @SWG\Response(
     *          response="200",
     *          description="Additional equipment details response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\AdditionalEquipment\Model\AdditionalEquipment::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param int $additionalEquipmentId
     * @return Response
     */
    public function showAdditionalEquipment(int $additionalEquipmentId)
    {
        return $this->ask(new GetAdditionalEquipment($additionalEquipmentId));
    }

    /**
     * List cabin additional equipment.
     *
     * @SWG\Get(
     *     summary="List cabin additional equipment.",
     *     description="List cabin additional equipment.",
     *     tags={"Additional Equipment"},
     *     @SWG\Response(
     *          response="200",
     *          description="Additional equipment list added to cabin collection response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\AdditionalEquipment\Model\AdditionalEquipment::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param int $cabinId
     * @return Response
     */
    public function listCabinAdditionalEquipment(int $cabinId)
    {
        return $this->ask(new GetCabinAdditionalEquipment($cabinId));
    }
}