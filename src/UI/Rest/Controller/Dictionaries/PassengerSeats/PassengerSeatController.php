<?php

declare(strict_types=1);

namespace App\UI\Rest\Controller\Dictionaries\PassengerSeats;

use App\Application\UseCase\Dictionaries\PassengerSeats\Request\GetCabinSeats;
use App\Application\UseCase\Dictionaries\PassengerSeats\Request\GetPassengerSeat;
use App\Application\UseCase\Dictionaries\PassengerSeats\Request\GetPassengerSeats;
use App\UI\Rest\Controller\AbstractBusController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PassengerSeatController
 * @package App\UI\Rest\Controller\Dictionaries\PassengerSeats
 */
class PassengerSeatController extends AbstractBusController
{
    /**
     * List all passenger seats.
     *
     * @SWG\Get(
     *     summary="List all passenger seats.",
     *     description="List all passenger seats.",
     *     tags={"Passenger Seats"},
     *     @SWG\Response(
     *          response="200",
     *          description="Passenger Seats collection response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\PassengerSeats\Model\PassengerSeat::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @return Response
     */
    public function listPassengerSeats()
    {
        return $this->ask(new GetPassengerSeats());
    }

    /**
     * Show passenger seat.
     *
     * @SWG\Get(
     *     summary="Show passenger seat.",
     *     description="Show passenger seat.",
     *     tags={"Passenger Seats"},
     *     @SWG\Response(
     *          response="200",
     *          description="Passenger Seat details response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\PassengerSeats\Model\PassengerSeat::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param int $passengerSeatId
     * @return Response
     */
    public function showPassengerSeat(int $passengerSeatId)
    {
        return $this->ask(new GetPassengerSeat($passengerSeatId));
    }

    /**
     * List cabin passenger seats.
     *
     * @SWG\Get(
     *     summary="List cabin passenger seats.",
     *     description="List cabin passenger seats.",
     *     tags={"Passenger Seats"},
     *     @SWG\Response(
     *          response="200",
     *          description="Passenger Seats added to cabin collection response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\PassengerSeats\Model\PassengerSeat::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param int $cabinId
     * @return Response
     */
    public function listCabinPassengerSeats(int $cabinId)
    {
        return $this->ask(new GetCabinSeats($cabinId));
    }
}