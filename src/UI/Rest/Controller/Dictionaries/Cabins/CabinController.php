<?php

declare(strict_types=1);

namespace App\UI\Rest\Controller\Dictionaries\Cabins;

use App\Application\UseCase\Dictionaries\Cabins\Request\GetCabin;
use App\Application\UseCase\Dictionaries\Cabins\Request\GetCabins;
use App\UI\Rest\Controller\AbstractBusController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CabinController
 * @package App\UI\Rest\Controller\Dictionaries\Cabins
 */
class CabinController extends AbstractBusController
{
    /**
     * List all cabins.
     *
     * @SWG\Get(
     *     summary="List all cabins.",
     *     description="List all cabins.",
     *     tags={"Cabins"},
     *     @SWG\Response(
     *          response="200",
     *          description="Cabins collection response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\Cabins\Model\Cabin::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @return Response
     */
    public function listCabins()
    {
        return $this->ask(new GetCabins());
    }

    /**
     * Show cabin.
     *
     * @SWG\Get(
     *     summary="Show cabin.",
     *     description="Show cabin.",
     *     tags={"Cabins"},
     *     @SWG\Response(
     *          response="200",
     *          description="Cabin details response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\Cabins\Model\Cabin::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param int $cabinId
     * @return Response
     */
    public function showCabin(int $cabinId)
    {
        return $this->ask(new GetCabin($cabinId));
    }
}