<?php

declare(strict_types=1);

namespace App\UI\Rest\Controller\Dictionaries\Colors;

use App\Application\UseCase\Dictionaries\Colors\Request\GetColor;
use App\Application\UseCase\Dictionaries\Colors\Request\GetColors;
use App\UI\Rest\Controller\AbstractBusController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ColorController
 * @package App\UI\Rest\Controller\Dictionaries\Colors
 */
class ColorController extends AbstractBusController
{
    /**
     * List all colors.
     *
     * @SWG\Get(
     *     summary="List all colors.",
     *     description="List all colors.",
     *     tags={"Colors"},
     *     @SWG\Response(
     *          response="200",
     *          description="Colors collection response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\Colors\Model\Color::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @return Response
     */
    public function listColors()
    {
        return $this->ask(new GetColors());
    }

    /**
     * Show color.
     *
     * @SWG\Get(
     *     summary="Show color.",
     *     description="Show color.",
     *     tags={"Colors"},
     *     @SWG\Response(
     *          response="200",
     *          description="Color details response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\Colors\Model\Color::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param int $colorId
     * @return Response
     */
    public function showColor(int $colorId)
    {
        return $this->ask(new GetColor($colorId));
    }
}