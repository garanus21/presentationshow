<?php

declare(strict_types=1);

namespace App\UI\Rest\Controller\Dictionaries\SleepingCabinTypes;

use App\Application\UseCase\Dictionaries\SleepingCabinTypes\Request\GetCabinSleepingCabinType;
use App\Application\UseCase\Dictionaries\SleepingCabinTypes\Request\GetSleepingCabinType;
use App\Application\UseCase\Dictionaries\SleepingCabinTypes\Request\GetSleepingCabinTypes;
use App\UI\Rest\Controller\AbstractBusController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SleepingCabinTypeController
 * @package App\UI\Rest\Controller\Dictionaries\SleepingCabinTypes
 */
class SleepingCabinTypeController extends AbstractBusController
{
    /**
     * List all sleeping cabin types.
     *
     * @SWG\Get(
     *     summary="List all sleeping cabin types.",
     *     description="List all sleeping cabin types.",
     *     tags={"Sleeping Cabin Types"},
     *     @SWG\Response(
     *          response="200",
     *          description="Sleeping cabin type collection response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\SleepingCabinTypes\Model\SleepingCabinType::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @return Response
     */
    public function listSleepingCabinTypes()
    {
        return $this->ask(new GetSleepingCabinTypes());
    }

    /**
     * Show sleeping cabin type.
     *
     * @SWG\Get(
     *     summary="Show sleeping cabin type.",
     *     description="Show sleeping cabin type.",
     *     tags={"Sleeping Cabin Types"},
     *     @SWG\Response(
     *          response="200",
     *          description="Sleeping cabin type details response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\SleepingCabinTypes\Model\SleepingCabinType::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param int $sleepingCabinTypeId
     * @return Response
     */
    public function showSleepingCabinType(int $sleepingCabinTypeId)
    {
        return $this->ask(new GetSleepingCabinType($sleepingCabinTypeId));
    }

    /**
     * List cabin sleeping cabin types.
     *
     * @SWG\Get(
     *     summary="List cabin sleeping cabin types.",
     *     description="List cabin sleeping cabin types.",
     *     tags={"Sleeping Cabin Types"},
     *     @SWG\Response(
     *          response="200",
     *          description="Passenger Seats added to cabin collection response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(
     *                      type=App\Domain\Dictionaries\SleepingCabinTypes\Model\SleepingCabinType::class
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="403",
     *          description="Access denied - wrong user"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param int $cabinId
     * @return Response
     */
    public function listCabinSleepingCabinTypes(int $cabinId)
    {
        return $this->ask(new GetCabinSleepingCabinType($cabinId));
    }
}