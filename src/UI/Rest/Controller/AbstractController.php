<?php

namespace App\UI\Rest\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\ControllerTrait;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbstractController
 *
 * @package App\UI\Rest\Controller
 */
abstract class AbstractController extends AbstractFOSRestController
{
    use ControllerTrait;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param RequestStack $requestStack
     * @required
     */
    public function setRequestStack(RequestStack $requestStack): void
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->getRequest()->getRequestFormat('json');
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->getRequest()->attributes->get('version') ?: 'v1';
    }

    /**
     * @param $route
     * @param array $parameters
     * @param int $statusCode
     * @param array $headers
     * @return View
     */
    protected function routeRedirectView(
        $route,
        array $parameters = [],
        $statusCode = Response::HTTP_CREATED,
        array $headers = []
    ): View {
        return View::createRouteRedirect(
            $route,
            array_merge(
                [
                    'version' => $this->getVersion(),
                    '_format' => $this->getFormat()
                ],
                $parameters
            ),
            $statusCode,
            $headers
        );
    }

    /**
     * @return Request|null
     */
    protected function getRequest(): ?Request
    {
        return $this->requestStack->getMasterRequest();
    }

    /**
     * Get the ViewHandler.
     *
     * @return ViewHandlerInterface
     */
    protected function getViewHandler()
    {
        if (!$this->viewhandler instanceof ViewHandlerInterface) {
            $this->viewhandler = $this->container->get('fos_rest.view_handler');
        }

        return $this->viewhandler;
    }
}
